source 'https://rubygems.org'

ruby '2.5.3'

gem 'rails', '~>  5.2'

# Nokogiri is a pain, so keep it at the version that works.
gem 'nokogiri',       '= 1.8.2'

gem 'bcrypt-ruby',  '~> 3.1'    # ActiveModel#has_secure_password
gem 'jbuilder',     '~> 2.8'    # JSON templates
gem 'newrelic_rpm', '~> 5.5'    # Performance Monitoring
gem 'pg',           '~> 0.18.2' # Database Server
gem 'prawn',        '~> 0.15.0' # PDF Rendering.
gem 'puma',         '~> 3.12'   # Web Server
gem 'scenic',       '~> 1.4'    # View Migrations
gem 'sidekiq',      '~> 5.2'    # Active Job Queueing Service
gem 'sinatra', :require => false
gem 'warden',       '~> 1.2'    # Rack Authentication Framework
gem 'yard',         '~> 0.9'    # Documentation Server

gem 'yard-activerecord', '~> 0.0.16' # Activerecord plugin for yard.

# Active Storage
gem 'aws-sdk-s3',     '~> 1.24'   # Active Storage uses S3
gem 'image_processing', '~> 1.2'  # Image sizes/previews
gem 'mini_magick',    '~> 4.9'    # Image sizes/previews

# Assets
gem 'bootstrap-sass', '~> 3.3.5'  # Bootstrap in Sass format.
gem 'jquery-rails',   '~> 4.3'    # Jquery for Rails, includes jquery_ujs
gem 'react-rails',    '~> 2.4'    # ReactJS for Ruby on Rails.
gem 'sass-rails',     '~> 5.0'    # Sass CSS compiler.
gem 'uglifier',       '~> 4.1'    # Javascript compressor.

# Testing
group :development, :test do
  gem 'byebug',             '~> 10.0'
  gem 'database_cleaner',   '~> 1.7'
  gem 'factory_girl_rails', '~> 4.5.0'
  gem 'guard-rspec',        '~> 4.7', :require => false
  gem 'jasmine',            '~> 2.5.1'
  gem 'json-schema',        '~> 2.8'
  gem 'rb-fsevent',         '~> 0.9.5'
  gem 'rspec-rails',        '~> 3.8'
end

group :test do
  gem 'capybara',           '~> 2.11.0'
  gem 'launchy',            '~> 2.4.3'
  gem 'selenium-webdriver', '~> 3.7.0' # Javascript driver
  gem 'simplecov',          '~> 0.10.0', :require => false
end

# Heroku
gem 'rails_12factor', group: :production

# Dev Tools.
gem 'web-console',        '~> 3.7', group: :development
