class CategoriesComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
      category: ''
    };
  };

  traitsFor(categories, categoryId) {
    return [].concat.apply([], categories.filter(function(category) {
      return category.category_id == categoryId;
    }).map(function(category) {
      return category.traits;
    }));
  };

  componentDidMount() {

    var getInitialCateogryFromTraitId = function(categories, trait_id) {
      return categories.filter(function(category) {
        return category.traits.find((trait) => trait.id == trait_id) != undefined;
      })[0];
    };

    var subscriptionPromise = this.subscriptionFor(this.props.target);

    fetch(this.props.url, {headers: {Accept: 'application/json'}, credentials: 'same-origin'})
      .then((response) => response.json())
      .then((data) => {
        this.setState({ categories: data });

        if(this.props.value != undefined) {
          var initialCategory = getInitialCateogryFromTraitId(data, this.props.value);

          this.setState({ category: initialCategory.category_id });
        }

        return subscriptionPromise;

      }.bind(this))
      .then((subscription) => {
        this.subscription = subscription;

        this.subscription.updateTraits(this.traitsFor(this.state.categories, this.state.category));
      }.bind(this))
      .catch((error) => {
        console.log(error.message);
      });
  };

  handleChange(event) {
    this.setState({ category: event.target.value });

    if(this.subscription != undefined) {
      this.subscription.updateTraits(this.traitsFor(this.state.categories, event.target.value));
    }
  };

  subscriptionFor(target) {
    return new Promise((resolve, reject) => {

      App.cable.subscriptions.create({ channel: 'CategoriesComponentChannel', target: target }, {
        connected: function() {
          resolve(this);
        },
        updateTraits: function(traits) {
          this.perform('update_traits',{traits: traits});
        }
      });
    });
  };

  render() {
    return (
      <select id={this.props.id} className={this.props.className || ''} onChange={this.handleChange.bind(this)} value={this.state.category}>
        <option></option>
        {this.state.categories.map((category) => (<option key={category.category_id} value={category.category_id}>{category.category}</option>))}
      </select>
    );
  };
}
