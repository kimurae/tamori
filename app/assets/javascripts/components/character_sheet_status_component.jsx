class CharacterSheetStatusComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: '100',
      status:   'Complete'
    };
  };

  componentDidMount() {

    this.subscription = App.cable.subscriptions.create({channel: 'CharacterSheetChannel', character_id: this.props.characterId, id: this.props.id}, {
      disconnected: function() {
        if(this.state.status != 'Complete') {
          this.setState({ status: 'Disconnected' });
        }
      }.bind(this),
      received: function(data) {
        this.setState({ status: data.status, progress: data.progress });
      }.bind(this)
    });

  };

  render() {
    return (
      <span>
        {this.state.status} ({this.state.progress}%)
      </span>
    );
  };
}
