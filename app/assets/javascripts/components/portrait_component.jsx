class PortraitComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = { url: null };
  };

  componentDidMount() {
    this.setState({url: this.props.url});

    this.reader = new FileReader();

    this.reader.addEventListener('load', function() {
      this.setState({ url:   this.reader.result});
    }.bind(this), false);

    document.getElementById(this.props.source).addEventListener('change', function(event) {
      if(event.target.files[0]) {
        this.reader.readAsDataURL(event.target.files[0]);
      }
    }.bind(this), false);
  };

  render() {
    if(this.state.url) {
      return ( <img className="form-control-static portrait-preview" src={this.state.url} alt="No Portrait" style={{border:"1", width:"100%"}} /> );
    }
    else {
      return null;
    }
  };
}
