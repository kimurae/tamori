class TextFieldListComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      values: [],
      newItem: ''
    };
  };

  componentDidMount() {

    if(this.props.value != undefined) {
      this.setState({values: this.props.value});
    }
  };

  handleClick(event) {
    event.preventDefault();
    this.setState({ values: [...this.state.values, this.state.newItem] });
    this.setState({ newItem: '' });
    return false;
  };

  render() {
    return (
      <div className="form-group">
        <label htmlFor={this.props.id}>{this.props.label}</label>
        <ul>
          {this.state.values.map((value) => (<li key={value} className="checkbox"><label><input type="checkbox" name={this.props.name} value={value} defaultChecked="checked"/>{value}</label></li>))}
        </ul>
        <div className="input-group">
          <input id={this.props.id} type="text" className="form-control" onChange={(event) => this.setState({ newItem: event.target.value})} value={this.state.newItem} />
          <span className="input-group-btn">
            <button className="btn btn-default" onClick={this.handleClick.bind(this)}>Add {this.props.label}</button>
          </span>
        </div>
      </div>
    );
  };
}
