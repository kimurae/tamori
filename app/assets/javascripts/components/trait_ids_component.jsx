class TraitIdsComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
      category: '',
      trait: '',
      traitIds: []
    };
  };

  traitsFor(categories, categoryId) {
    return [].concat.apply([], categories.filter(function(category) {
      return category.category_id == categoryId;
    }).map(function(category) {
      return category.traits;
    }));
  };

  componentDidMount() {

    fetch('/api/systems/' + encodeURIComponent(this.props.systemId) + '/traits.json', {headers: {Accept: 'application/json'}, credentials: 'same-origin'})
      .then((response) => response.json())
      .then((data) => {
        this.setState({ categories: data });

        if(this.props.value != undefined) {
          console.log(this.props.value)
          this.setState({ traitIds: this.props.value });
        }
      }.bind(this))
      .catch((error) => {
        console.log(error.message);
      });
  };

  handleChange(event) {
    this.setState({ category: event.target.value });
  };

  handleClick(event) {
    event.preventDefault();
    var traitIds = [...this.state.traitIds, this.state.trait];

    this.setState({ traitIds: traitIds });

    return false;
  };

  render() {

    var traitIdList = function(categories, traitIds) {
      return [].concat.apply([], traitIds.map(function(traitId) {
        return [].concat.apply([], categories.map((category) => category.traits))
          .filter((trait) => trait.id == traitId);
      }));
    };

    return (
      <div className="form-group">
        <label htmlFor={this.props.id}>{this.props.label}</label>
        <ul>
          {traitIdList(this.state.categories, this.state.traitIds).map((trait) => (<li key={trait.id} className="checkbox"><label><input type="checkbox" name={this.props.name} value={trait.id} defaultChecked="checked"/> {trait.name}</label></li>))}
        </ul>
        <div className="row">
          <div className="col-xs-12 col-sm-4">
            <select id={this.props.id + '-category'} className="form-control" onChange={this.handleChange.bind(this)} value={this.state.category}>
              <option></option>
              {this.state.categories.map((category) => (<option key={category.category_id} value={category.category_id}>{category.category}</option>))}
            </select>
          </div>
          <div className="col-xs-12 col-sm-4">
            <select id={this.props.id} name={this.props.name} className="form-control" onChange={(event) => this.setState({ trait: event.target.value })} value={this.state.trait}>
              <option></option>
              {this.traitsFor(this.state.categories, this.state.category).map((trait) => (<option key={trait.id} value={trait.id}>{trait.name}</option>))}
            </select>
          </div>
          <div className="col-xs-12 col-sm-4">
            <button className="btn btn-default" onClick={this.handleClick.bind(this)}>Add</button>
          </div>
        </div>
      </div>
    );
  };
}
