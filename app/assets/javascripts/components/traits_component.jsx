class TraitsComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      traits: [],
      trait: ''
    };
  };

  componentDidMount() {

    if(this.props.value != undefined) {
      this.setState({ trait: this.props.value });
    }

    this.subscription = App.cable.subscriptions.create({ channel: 'TraitsComponentChannel', id: this.props.id }, {
      received: function(traits) {
        this.setState({ traits: traits });
      }.bind(this)
    });
  };

  render() {
    return (
      <select id={this.props.id} name={this.props.name} className={this.props.className || ''} onChange={(event) => this.setState({ trait: event.target.value })} value={this.state.trait}>
        <option></option>
        {this.state.traits.map((trait) => (<option key={trait.id} value={trait.id}>{trait.name}</option>))}
      </select>
    );
  };
}
