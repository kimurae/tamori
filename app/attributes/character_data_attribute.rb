# A JSONB object that is structred as a hash of arrays, with a few exceptions
class CharacterDataAttribute < ActiveRecord::Type::Json

  def deserialize(value)
    super(value).tap do |hash|
      hash.default_proc = proc do |h,k|
        h[k] = default_value_for(k)
      end
    end
  end

  def type
    :jsonb
  end

  private

  def default_value_for(key)
    case key.to_s
    when %w(experience level name) then Hash.new { |h,k| h[k] = String.new }
    when 'player'                  then String.new
    else
      Array.new
    end
  end
end
