class SymbolAttribute < ActiveRecord::Type::String

  def type
    :symbol
  end

  def cast(value)
    super.to_sym
  end
end
