class AnyCalculator < Calculator::Base

  def self.calculate(values, argument = 'true')
    values.any? { |i| i.to_s =~ Regexp.new(argument.to_s) }
  end

  def self.to_s(values = [], argument = 0)
    if values.present?
      "Any of #{values_to_s(values).join(',')} match /#{argument}/."
    else
      name
    end
  end

end
