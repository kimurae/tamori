module Calculator
  class Base

    def self.to_s(values = [], argument = nil)
      if values.present?
        name.gsub(/Calculator/,'') + '(' + values_to_s(values).join(',') + ')'
      else
        name
      end
    end

    def self.values_to_s(values)
      values.map { |v| '#{' + v + '}' }
    end

    private_class_method :values_to_s

  end
end
