class ConcatCalculator < Calculator::Base

  def self.calculate(values, argument = ' ')
    values.join(argument.to_s)
  end

  def self.to_s(values = [], argument = ' ')
    if values.present?
      values_to_s(values).join(argument.to_s)
    else
      name
    end
  end
end
