class L5r::FullDefenseCalculator < Calculator::Base

  # This assumes an argument order, sadly.
  def self.calculate(values, argument = nil)
    (armor_tn, ss_def, s_def, reflexes) = values
    "#{armor_tn} + 1/2 #{ss_def.to_i + s_def.to_i + reflexes.to_i}k#{reflexes}"
  end

end
