module L5r
  class InsightRankCalculator < Calculator::Base

    def self.calculate(values, argument = nil)
      insight = values.compact.reduce(:+) || 0

      [1, (( insight - 100).to_f / 25.0 ).floor ].max
    end
  end
end
