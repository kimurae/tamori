module L5r
  class SumDiceCalculator < Calculator::Base

    # Sum Dice bonuses for roll and keep which come in the format of
    # <rolled dice>k<kept dice>. So this just adds both rolled and kept
    # Assuming that nomenclature.
    def self.calculate(values, argument = nil)
      (values + ['0k0', argument]).select do |i|
        i.to_s =~ /\d+k\d+/
      end.map do |i|
        i.split('k').map(&:to_i)
      end.transpose.map do |i|
        i.reduce(:+).to_s
      end.join('k')
    end
  end
end
