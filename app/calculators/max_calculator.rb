class MaxCalculator < Calculator::Base

  def self.calculate(values, argument = nil)
    values.map do |i|
      i.to_i unless i.kind_of?(Numeric)
    end.max || 0
  end

end
