class MinCalculator < Calculator::Base

  def self.calculate(values, argument = nil)
    values.compact.min || 0
  end

end
