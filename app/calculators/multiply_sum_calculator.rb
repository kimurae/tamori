class MultiplySumCalculator < Calculator::Base

  def self.calculate(values, argument = 1)
    ( values.compact.reduce(:+) || 0 ) * argument.to_i
  end

  def self.to_s(values = [], argument = 1)
    if values.present?
      "#{argument}( #{values_to_s(values).join(' + ')} )"
    else
      name
    end
  end

end
