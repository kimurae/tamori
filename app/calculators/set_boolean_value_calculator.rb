class SetBooleanValueCalculator < Calculator::Base

  def self.calculate(values, argument = 'false')
    argument == 'true'
  end

  def self.to_s(values = [], argument = 'false')
    name unless argument.present?
    argument.to_s
  end
end
