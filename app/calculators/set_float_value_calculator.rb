class SetFloatValueCalculator < Calculator::Base

  def self.calculate(values, argument = 0)
    argument.to_f
  end

  def self.to_s(values = [], argument = 0)
    argument.to_f.to_s
  end
end
