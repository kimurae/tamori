class SetIntegerValueCalculator < Calculator::Base

  def self.calculate(values, argument = 0)
    argument.to_i
  end

  def self.to_s(values = [], argument = 0)
    name unless argument.present?
    argument.to_s
  end
end
