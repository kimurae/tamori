class SetValueCalculator < Calculator::Base

  def self.calculate(values, argument = 0)
    argument.to_s
  end

  def self.to_s(values = [], argument = 0)
    if argument.present?
      argument.to_s
    else
      name
    end
  end
end
