class SumCalculator < Calculator::Base

  def self.calculate(values, argument = 0)
    (values + [argument.to_i]).compact.reduce(:+) || 0
  end

  def self.to_s(values = [], argument = 0)
    if values.present?
      (values_to_s(values) + [argument.to_s]).join(' + ')
    else
      name
    end
  end

end
