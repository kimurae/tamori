module Wod
  class BloodPoolCalculator < Calculator::Base

    def self.calculate(values, argument = nil)
      generation = values.compact.reduce(:+) || 0

      case(generation)
        when 4 then 50
        when 5 then 40
        when 6 then 30
        when 7 then 20
        when 8 then 15
        when 9 then 14
        when 10 then 13
        when 11 then 12
        when 12 then 11
      else
        10
      end
    end
  end
end
