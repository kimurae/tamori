module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = AuthenticatedConstraint.current_user(request)
    end
  end
end
