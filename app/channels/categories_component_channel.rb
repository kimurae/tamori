class CategoriesComponentChannel < ApplicationCable::Channel

  def update_traits(data)
    ActionCable.server.broadcast("trait-component-#{params['target']}", data.fetch('traits'))
  end
end
