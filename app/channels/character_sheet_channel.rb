class CharacterSheetChannel < ApplicationCable::Channel

  def subscribed
    stream_from "character-sheet-#{params[:id]}"
  end
end

