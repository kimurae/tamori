class TraitsComponentChannel < ApplicationCable::Channel

  def subscribed
    stream_from "trait-component-#{params[:id]}"
  end
end
