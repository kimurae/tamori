# Compiles the Character Data for the PDF renderer.
# Eventually this will be refactored into an attribute class.
class CharacterDataCompiler

  # @param build_ids [Array<Integer>] range of build ids to use for the query to fetch the character data.
  # @param build_traits [ActiveRelation] the trait data to merge with the character data.
  # @param character_id [Integer] the character id used for the query to fetch the character data.
  # @param user_items [ActiveRelation] the user defined trait data to merge with the character data.
  # @return [Hash<Array>] compiled character, trait, and user_item data grouped by category.
  def self.compile(*args)
    new(*args).send(:generate)
  end

  private_class_method :new

  private

  attr_reader :build_traits, :user_items

  delegate :name, :user_name, :system_id, :to => :character, :prefix => true

  def initialize(build_ids:, build_traits:, character_id:, user_items:)
    @build_ids    = build_ids
    @build_traits = build_traits
    @character_id = character_id
    @user_items   = user_items
  end

  def addable?(*args)
    args.all? do |value|
      value.kind_of?(Numeric) && value.respond_to?(:+)
    end
  end

  def character
    @character ||= Character.find(@character_id)
  end

  def generate
    merge_components.tap do |h|

      experience_traits.each do |component|
        h[component.id][:active] = true
        h[component.id][:value] ||= experience_cost_for(component.name.to_sym)
      end

      # dereived_traits - trait_ids that are inputs
      # some function to perform on those inputs - I think these can be std agg and special snowflake cases.
      build_calculations.each do |component|
        h[component.trait_id][component.output_key] = if addable?(h[component.trait_id][component.output_key], component.value_from(h))
          (h[component.trait_id][component.output_key] || 0) + component.value_from(h)
        else
          component.value_from(h)
        end

        # TODO, rework this to have a better solution instead of this hack.
        if update_summary?(component.output_key, h[component.trait_id][:value])
          h[component.trait_id][:summary] = if h[component.trait_id][:value].present? && h[component.trait_id][:value].to_s != '0'
            "#{h[component.trait_id][:label]} #{h[component.trait_id][:value]}"
          else
            String.new
          end
        end
      end

    end.values.select do |h|
      h[:category_name]
    end.group_by do |h|
      h[:category_name].to_sym
    end.tap do |h|
      h[:player]    = character_user_name
      h[:name]      = { :id => 'character-name', :name => 'Name', :value => character_name, :active => true }
      h[:level]     = h.fetch(:level, []).first || {}
      h[:experience] = h.fetch(:experience, []).detect { |xp| xp[:name] == 'experience' }
    end
  end

  def build_calculations
    @build_calculations ||= BuildCalculation.for_system(character_system_id).to_a
  end

  def build_components
    @build_components ||= BuildComponent.compiled(@build_ids)
  end

  def experience_cost_for(trait_name_sym)
    build_components.select do |component|
      component.respond_to?(trait_name_sym)
    end.map(&trait_name_sym).map(&:to_i).reduce(:+)
  end

  def experience_traits
    @experience_traits ||= Trait.for_experience(character_system_id).to_a
  end

  def merge_components
    [
      build_traits,
      build_components
    ].flatten.map do |component|
      component.to_component
    end.group_by do |component|
      component[:id]
    end.values.map do |component_list|
      component_list.inject(&:merge)
    end.inject(Hash.new) do |components, component|
      components.merge( component[:id] => component )
    end
  end

  def update_summary?(output_key, value)
    output_key.to_s == 'value' &&
    [String, Numeric].any? { |k| value.kind_of?(k) }
  end
end
