# Compiles character cost related data for the cost report.
class CostReportCompiler

  # @return [Array<Hash>] compiled cost data for a pdf cost report.
  def self.compile(*args)
    new(*args).send(:data)
  end

  private

  attr_reader :data

  def initialize(build_ids, traits)
    @data = generate(build_ids, traits)
  end

  def generate(build_ids, build_traits)
    build_traits.group_by(&:category_label).flat_map do |category_label, traits|
      CostReport.compiled(build_ids, traits.map(&:id)).map do |entry|
        {
          :label         => entry.label.to_s,
          :xp            => entry.xp.to_i,
          :fp            => entry.fp.to_i,
          :dots          => entry.dots.to_i
        }
      end.tap do |entries|
        entries << {
          :label         => "Total #{category_label.pluralize}",
          :subtotal      => true,
          :xp            => entries.map { |e| e[:xp] }.inject(:+).to_i,
          :fp            => entries.map { |e| e[:fp] }.inject(:+).to_i,
          :dots          => entries.map { |e| e[:dots] }.inject(:+).to_i
        }
      end
    end.tap do |entries|
      entries << {
        :label         => 'Total',
        :total         => true,
        :xp            => entries.select { |e| e[:subtotal] == true }.map { |e| e[:xp] }.compact.inject(:+).to_i,
        :fp            => entries.select { |e| e[:subtotal] == true }.map { |e| e[:fp] }.compact.inject(:+).to_i,
        :dots          => entries.select { |e| e[:subtotal] == true }.map { |e| e[:dots] }.compact.inject(:+).to_i
      }
    end
  end

end
