class AdminAuthenticatedConstraint < AuthenticatedConstraint

  def matches?(request)
    super && current_user(request).admin?
  end
end
