module Api
  module Systems
    class TraitsController < ApplicationController

      helper_method :traits, :current_trait

      def index; end

      def show; end

      private

      def system
        System.find system_id
      end

      def system_id
        params.require(:system_id)
      end

      def current_trait
        @trait ||= traits.find params.require(:id)
      end

      def traits
        Trait.for_system(system_id)
      end
    end
  end
end
