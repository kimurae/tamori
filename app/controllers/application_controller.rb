class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from ActiveRecord::RecordInvalid,  :with => :record_invalid
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  after_action :set_csrf_cookie_for_ng

  helper_method :current_user_admin?, :current_user_name

  def self.serves_model(model_name)

    define_method :model_klass do
      model_name.to_s.camelize.constantize
    end

    define_method :model_scope do
      model_klass.all
    end

    define_method :"#{model_name}_id" do
      params.require(:id)
    end

    define_method :"#{model_name.to_s.pluralize}" do
      model_scope
    end

    define_method :"current_#{model_name}" do
      instance_variable_set(:"@#{model_name}",
        instance_variable_get(:"@#{model_name}") || fetch_current_model(send(:"#{model_name}_id")))
    end

    define_method :fetch_current_model do |model_id|
      model_scope.find(model_id)
    end

    helper_method :"current_#{model_name}", :"#{model_name.to_s.pluralize}"
  end

  protected

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end

  private

  delegate :admin?, :name, to: :current_user, allow_nil: true, prefix: true

  def current_user
    @current_user ||= AuthenticatedConstraint.current_user(request)
  end

  def record_invalid(err)
    render :json => err.record.errors, :status => :unprocessable_entity
  end

  def record_not_found(err)
    render :json => err.message, :status => :unprocessable_entity
  end

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
end
