module Characters
  class ApplicationController < ::ApplicationController

    layout 'character'

    private

    def fetch_character
      @character ||= Character.editable_by(current_user).find params.require(:character_id)
    end
  end
end
