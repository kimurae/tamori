module Characters
  class BuildAspectsController < BuildComponentsController

    private

    def build_component_params
      params.require(:build_aspect).permit(:active, :comment, :cost, :cost_type, :trait_id, notes: [])
    end

    def new_build_component(*args)
      build.build_aspects.build(*args)
    end
  end
end
