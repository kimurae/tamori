module Characters
  class BuildAttributesController < BuildComponentsController

    private

    def build_component_params
      params.require(:build_attribute).permit(:comment, :cost, :cost_type, :trait_id, :value, emphasis: [])
    end

    def new_build_component(*args)
      build.build_attributes.build(*args)
    end
  end
end
