module Characters
  class BuildComponentsController < Characters::ApplicationController

    def new
      render locals: { build: build, character: character, build_component: new_build_component }
    end

    def create
      build_component = new_build_component(build_component_params)

      if build_component.save
        redirect_to edit_character_build_path(params[:character_id], params[:build_id])
      else
        render action: :new, locals: { build: build, character: character, build_component: new_build_component }
      end
    end

    def edit
      render locals: { build: build, character: character, build_component: fetch_build_component }
    end

    def update
      build_component = fetch_build_component

      if build_component.update(build_component_params)
        redirect_to edit_character_build_path(params[:character_id], params[:build_id])
      else
        render action: :edit, locals: { build: build, character: character, build_component: fetch_build_component }
      end
    end

    def destroy
      fetch_build_component.destroy
      redirect_to edit_character_build_path(params[:character_id], params[:build_id])
    end

    private

    def build
      @build ||= character.builds.find params.require(:build_id)
    end

    def build_component_params
      raise NotImplementedError
    end

    def character
      @character ||= fetch_character
    end

    def fetch_build_component
      BuildComponent.
        for_build(params.require(:build_id)).
        find(params.require(:id))
    end

    def new_build_component(*args)
      raise NotImplementedError
    end
  end
end
