module Characters
  class BuildEssaysController < BuildComponentsController

    private

    def build_component_params
      params.require(:build_essay).permit(:comment, :cost, :cost_type, :trait_id, :value)
    end

    def new_build_component(*args)
      build.build_essays.build(*args)
    end
  end
end
