module Characters
  class BuildItemsController < Characters::ApplicationController

    helper_method :user_items

    def new
      render locals: { build: build, character: character, build_item: new_build_item }
    end

    def create
      build_item = new_build_item(build_item_params)

      if build_item.save
        redirect_to edit_character_build_path(params[:character_id], params[:build_id])
      else
        render action: :new, locals: { build: build, character: character, build_item: new_build_item }
      end
    end

    def edit
      render locals: { build: build, character: character, build_item: fetch_build_item }
    end

    def update
      build_item = fetch_build_item

      if build_item.update(build_item_params)
        redirect_to edit_character_build_path(params[:character_id], params[:build_id])
      else
        render action: :edit, locals: { build: build, character: character, build_item: fetch_build_item }
      end
    end

    def destroy
      fetch_build_item.destroy
      redirect_to edit_character_build_path(params[:character_id], params[:build_id])
    end

    private

    def build
      @build ||= character.builds.find params.require(:build_id)
    end

    def build_item_params
      params.require(:build_item).permit(:comment, :cost, :cost_type, :location, :qty, :trait_id, notes: [])
    end

    def character
      @character ||= fetch_character
    end

    def fetch_build_item
      build.build_items.find params.require(:id)
    end

    def new_build_item(*args)
      build.build_items.build(*args)
    end

    def user_items
      current_user.user_items.order(:name)
    end
  end
end
