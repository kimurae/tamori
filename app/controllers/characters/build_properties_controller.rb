module Characters
  class BuildPropertiesController < BuildComponentsController

    private

    def build_component_params
      params.require(:build_property).permit(:comment, :cost, :cost_type, :trait_id, :value)
    end

    def new_build_component(*args)
      build.build_properties.build(*args)
    end
  end
end
