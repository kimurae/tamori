module Characters
  class BuildRanksController < BuildComponentsController

    private

    def build_component_params
      params.require(:build_rank).permit(:comment, :cost, :cost_type, :trait_id, :value)
    end

    def new_build_component(*args)
      build.build_ranks.build(*args)
    end
  end
end
