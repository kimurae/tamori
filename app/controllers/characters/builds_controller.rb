module Characters
  class BuildsController < Characters::ApplicationController

    def index
      render locals: { builds: fetch_builds_for_character, character: fetch_character }
    end

    def create
      build = generate_build

      if build.save!
        redirect_to action: :index
      else
        raise 'I have no idea why this broke'
      end
    end

    def edit
      build = fetch_build
      render locals: { build: build, character: fetch_character }
    end

    def update
      build = fetch_build

      if build.update(model_params)
        redirect_to action: :index
      else
        render action: :edit, locals: { build: build, character: fetch_character }
      end
    end

    def destroy
      fetch_build.destroy

      redirect_to action: :index
    end

    private

    delegate :builds, :system_id, to: :fetch_character

    def fetch_builds_for_character
      builds.order(built_on: :desc).order(created_at: :desc)
    end

    def fetch_build
      builds.find(params[:id])
    end

    def generate_build
      Build.new(characters: [fetch_character], name: 'New Build', built_on: Date.today, system_id: system_id, user: current_user)
    end

    def model_params
      params.require(:build).permit(:built_on, :name)
    end
  end
end
