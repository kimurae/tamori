module Characters
  class GameSessionsController < Characters::ApplicationController

    def index
      character = fetch_character

      render locals: { game_sessions: fetch_game_sessions_for(character), character: character }
    end

    def create
      game_session = generate_game_session

      if game_session.save!
        redirect_to action: :index
      else
        raise 'I have no idea why this broke'
      end
    end

    def edit
      game_session = fetch_game_session
      render locals: { game_session: game_session, character: fetch_character }
    end

    def update
      game_session = fetch_game_session

      if game_session.update(model_params)
        redirect_to action: :index
      else
        render action: :edit, locals: { game_session: game_session, character: fetch_character }
      end
    end

    def destroy
      fetch_game_session.destroy

      redirect_to action: :index
    end

    private

    delegate :system_id, to: :fetch_character

    def fetch_game_sessions_for(character)
      character.
        game_sessions.
        includes(:experience_awarded_attribute).
        order(built_on: :desc).
        order(created_at: :desc)
    end

    def fetch_game_session
      fetch_character.game_sessions.find(params[:id])
    end

    def generate_game_session
      GameSession.new(characters: [fetch_character], name: 'New Game Session', built_on: Date.today, system_id: system_id, user: current_user)
    end

    def model_params
      params.require(:game_session).permit(:built_on, :name, :experience_awarded)
    end
  end
end
