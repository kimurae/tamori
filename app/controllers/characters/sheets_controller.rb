module Characters
  class SheetsController < Characters::ApplicationController

    def index
      character = fetch_character
      render locals: { character: character, character_sheets: character.character_sheets }
    end

    def show
      character_sheet = CharacterSheet.for_public.find params.require(:id)
      redirect_to rails_blob_path(character_sheet.sheet, disposition: 'attachment') 
    end

    def create
      character_sheet = fetch_character.character_sheets_create!
      CharacterCompileJob.perform_later(character_sheet.id, character_sheet.character_build_ids)
      redirect_to action: :index, character_id: params[:character_id]
    end

    def update
      character_sheet = fetch_sheet
      CharacterCompileJob.perform_later(character_sheet.id, character_sheet.character_build_ids)
      redirect_to action: :index, character_id: params[:character_id]
    end

    def destroy
      fetch_sheet.destroy
      redirect_to action: :index, character_id: params[:character_id]
    end

    private

    def fetch_sheet
      fetch_character.character_sheets.find(params.require(:id))
    end
  end
end
