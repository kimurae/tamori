class CharactersController < ApplicationController

  helper_method :portrait_url

  def index
    render locals: { characters: character_scope.all }
  end

  def show
    render locals: { character: fetch_character }, layout: 'character'
  end

  def new
    render locals: { character: current_user.characters.build, systems: System.all }
  end

  def create
    character = current_user.characters.build(character_params)

    if character.save
      redirect_to action: :index
    else
      render action: :new, locals: { character: character, systems: System.all }
    end
  end

  def edit
    render locals: { character: fetch_character }, layout: 'character'
  end

  def update
    character = fetch_character

    if character.update(character_params)
      redirect_to action: :index
    else
      render action: :edit, locals: { character: fetch_character }, layout: 'character'
    end
  end

  private

  def character_params
    params.require(:character).permit(:name, :portrait, :system_id)
  end

  def character_scope
    Character.editable_by(current_user)
  end

  def fetch_character
    character_scope.find params.require(:id)
  end

  def portrait_url(character)
    rails_blob_path(character.portrait) if character.portrait.attached?
  end
    
end
