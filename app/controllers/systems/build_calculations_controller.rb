module Systems
  class BuildCalculationsController < ApplicationController

    layout 'system'

    helper_method :calculation_options, :trait_key_options

    def index
      system = fetch_system

      render locals: { build_calculations: BuildCalculation.for_system(system.id).by_trait, system: system }
    end

    def new
      render locals: { build_calculation: new_build_calculation, system: fetch_system }
    end

    def create
      build_calculation = new_build_calculation(build_calculation_params)

      if build_calculation.save
        redirect_to action: :index, system_id: build_calculation.system.id
      else
        render action: :new, locals: { build_calculation: build_calculation, system: build_calculation.system }
      end
    end

    def edit
      render locals: { build_calculation: fetch_build_calculation, system: fetch_build_calculation.system}
    end

    def update
      build_calculation = fetch_build_calculation

      if build_calculation.update(build_calculation_params)
        redirect_to action: :index, system_id: build_calculation.system_id
      else
        render action: :edit, locals: { build_calculation: build_calculation, system: build_calculation.system }
      end
    end

    private

    def build_calculation_params
      params.require(:build_calculation).permit(
        :argument,
        :calculator,
        :input_key,
        :output_key,
        :priority,
        :trait_id,
        trait_ids: []
      )
    end

    def calculation_options
      [
        ['AnyCalculator',               'AnyCalculator'],
        ['ConcatCalculator',            'ConcatCalculator'],
        ['MaxCalculator',               'MaxCalculator'],
        ['MinCalculator',               'MinCalculator'],
        ['SetBooleanValueCalculator',   'SetBooleanValueCalculator'],
        ['SetFloatValueCalculator',     'SetFloatValueCalculator'],
        ['SetIntegerValueCalculator',   'SetIntegerValueCalculator'],
        ['SumCalculator',               'SumCalculator'],
        ['MultiplySumCaclulator',       'MultiplySumCalculator'],
        ['FullDefenseCalculator (L5R)', 'L5r::FullDefenseCalculator'],
        ['InsightRankCalculator (L5R)', 'L5r::InsightRankCalculator'],
        ['SumDiceCalculator (L5R)',     'L5r::SumDiceCalculator'],
        ['BloodPoolCalculator (Wod)',   'Wod::BloodPoolCalculator'],
      ]
    end

    def fetch_build_calculation
      BuildCalculation.find params.require(:id)
    end

    def fetch_system
      System.find params.require(:system_id)
    end

    def new_build_calculation(*args)
      fetch_system.build_calculations.build(*args)
    end

    def trait_key_options
      %w(active attack damage level summary value virtue_dots_spent).map do |i|
        [i.titleize, i]
      end
    end

  end
end
