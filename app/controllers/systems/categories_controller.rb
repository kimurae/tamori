module Systems
  class CategoriesController < ApplicationController
    helper_method :category_name_list
    layout        'system'

    def index
      system = fetch_system
      render locals: { categories: system.categories.order(:label), system: system }
    end

    def new
      category = new_category
      render locals: { category: category, system: category.system }
    end

    def create
      category = new_category(category_params)

      if category.save
        redirect_to action: :index, system_id: category.system_id
      else
        render action: :new, locals: { category: category, system: category.system }
      end
    end

    def edit
      category = fetch_category
      render locals: { category: category, system: category.system }
    end

    def update
      category = fetch_category

      if category.update(category_params)
        redirect_to action: :index, system_id: category.system_id
      else
        render action: :edit, locals: { category: category, system: category.system }
      end
    end

    private

    def category_params
      params.require(:category).permit(:label, :name)
    end

    def fetch_category
      Category.find params.require(:id)
    end

    def fetch_system
      System.find params.require(:system_id)
    end

    def new_category(*args)
      fetch_system.categories.build(*args)
    end

    def category_name_list
      %w(
        air_attributes
        armor_tn
        armor
        aspects
        attributes
        backgrounds
        bearings
        calculations
        combat_modifiers
        details
        description
        earth_attributes
        experience
        factions
        fire_attributes
        insight
        items
        knowledge
        level
        mental_attributes
        name
        paths
        pools
        renown
        rings
        physical_attributes
        prelude
        relationships
        schools
        school_skills
        skills
        social_attributes
        spells
        summary_lists
        talents
        techniques
        virtues
        water_attributes
        weapons
        willpower
        wounds
      )
    end
  end
end
