module Systems
  class TraitsController < ApplicationController
    layout 'system'

    def index
      render locals: { categories: categories.includes(:traits), system: system }
    end

    def new
      render locals: { categories: categories, trait: system.traits.build(detail: Detail.new), system: system }
    end

    def create

      trait = system.traits.build(trait_params)

      if trait.save
        redirect_to action: :index, system_id: params[:system_id]
      else
        render action: :new, locals: { categories: categories, trait: trait, system: system }
      end
    end

    def edit
      trait = Trait.find(params[:id])
      trait.detail ||= Detail.new

      render locals: { categories: categories, trait: trait, system: system }
    end

    def update
      trait = Trait.find(params[:id])

      if trait.update(trait_params)
        redirect_to action: :index, system_id: params[:system_id]
      else
        render action: :edit, locals: { categories: categories, trait: trait, system: system }
      end
    end

    private

    def categories
      system.categories.order(:label)
    end
 
    def system
      @system ||= System.find params.require(:system_id)
    end

    def trait_params
      params.require(:trait).permit(
        :category_id,
        :label,
        :manual_order,
        :name,
        detail_attributes: [
          :area,
          :book,
          :cost,
          :damage,
          :description,
          :demands,
          :difficulty,
          :duration,
          :level,
          :list,
          :page,
          :range,
          :reduction,
          :roll,
          :tn_bonus,
          keywords: [],
          raises: []
        ]
      )
    end
  end
end
