class SystemsController < ApplicationController

  def index
    render locals: { systems: System.order(:name) }
  end

  def show
    render locals: { system: System.find(params[:id]) }, layout: 'system'
  end

  def new
    render locals: { system: System.new }
  end

  def create
    system = System.new(system_params)

    if system.save
      redirect_to action: :index
    else
      render action: :new, locals: { system: system }
    end
  end

  private

  def system_params
    params.require(:system).permit(:name, :renderer)
  end
end
