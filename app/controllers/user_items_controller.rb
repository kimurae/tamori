class UserItemsController < ApplicationController

  def index
    render locals: { user_items: user_items_by_category }
  end

  def new
    render locals: { categories: fetch_categories, user_item: current_user.user_items.build }
  end

  def create
    user_item = current_user.user_items.build(user_item_params)

    if user_item.save
      redirect_to action: :index
    else
      render action: :new, locals: { categories: fetch_categories, user_item: user_item }
    end
  end

  def edit
    render locals: { categories: fetch_categories, user_item: fetch_user_item }
  end

  def update
    user_item = fetch_user_item

    if user_item.update(user_item_params)
      redirect_to action: :index
    else
      render action: :edit, locals: { categories: fetch_categories, user_item: user_item }
    end
  end

  def destroy
    fetch_user_item.destroy
    redirect_to action: :index
  end

  private

  def fetch_categories
    Category.order(:label).includes(:system).all
  end

  def fetch_user_item
    current_user.user_items.find params[:id]
  end

  def user_items_by_category
    current_user.user_items.to_a.sort_by(&:category_label).group_by(&:category_label)
  end

  def user_item_params
    params.require(:user_item).permit(%i(
      category_id
      book
      description
      devotion
      influence
      level
      list
      mentor
      name
      page
    ))
  end
end
