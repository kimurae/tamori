module CollapsePanelHelper

  def collapse_panel(id:, title:)
    content_tag(:div, class: 'panel-group', id: "#{id}-panel-group", role: 'tablist') do
      content_tag(:div, class: 'panel panel-default') do
        content_tag(:div, class: 'panel-heading', role: 'tab', id: "#{id}-panel-heading") do
          content_tag(:h4, class: 'panel-title') do
            link_to(title, "##{id}-collapse", aria: { controls: "#{id}-collapse" }, data: { parent: "##{id}-panel-group", toggle: 'collapse' })
          end
        end +
        content_tag(:div, arialabeledby: "#{id}-panel-heading", class: 'panel-collapse collapse', id: "#{id}-collapse", role: 'tabpanel') do
          content_tag(:div, class: 'panel-body') do
            yield
          end
        end
      end
    end
  end
end
