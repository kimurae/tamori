class TamoriFormBuilder < ActionView::Helpers::FormBuilder

  def submit_button
    submit(class: 'btn btn-default')
  end

  def text_field(attribute, label_override: nil)
    wrap_form_element(attribute, label_override) do
      super(attribute, class: 'form-control')
    end
  end

  # This is a method that is very specific, but found in a number of forms for trait selection.
  def select_traits_by_category(attribute)
    content_id    = @template.dom_id(object, attribute)
    content_value = object.send(attribute)
    api_path      = @template.api_system_traits_path(object.system_id)

    @template.content_tag(:div, class: 'form-group') do
      @template.content_tag(:label, 'Category', for: "#{attribute}-category".parameterize) +
      @template.react_component('CategoriesComponent', { id: "#{attribute}-category".parameterize, className: 'form-control', target: content_id, value: content_value, url: api_path })
    end +
    @template.content_tag(:div, class: 'form-group') do
      @template.content_tag(:label, 'Trait', for: content_id) +
      @template.react_component('TraitsComponent', { id: content_id, className: 'form-control', name: "#{object_name}[#{attribute}]", value: content_value })
    end
  end

  private

  def wrap_form_element(attribute, label_override)
    @template.content_tag(:div, class: 'form-group') do
      label(label_override || attribute) + yield
    end
  end
end
