# Background Job to:
# * Compile the character data for a range of builds, then
# * Render a PDF character sheet.
# * Upload the document to AWS.
class CharacterCompileJob < ActiveJob::Base

  # @param sheet_id [Integer] the character sheet id.
  # @param build_ids [Array<Integer>] ids corresponding to the range of character builds.
  def perform(sheet_id, build_ids)

    @build_ids   = build_ids

    notify 'Initializing', 25, sheet_id

    sheet = CharacterSheet.find(sheet_id)

    @character_id = sheet.character_id

    notify 'Compiling', 25, sheet_id
    data = compile
    notify 'Compiled', 50, sheet_id

    notify 'Rendering', 50, sheet_id
    pdf  = StringIO.new(render_engine.new(data.with_indifferent_access).render)
    notify 'Rendered', 75, sheet_id

    notify 'Saving', 75, sheet_id
    sheet.sheet.attach(io: pdf, filename: "#{character.name}.pdf")
    sheet.update!(data: data)
    character.update!(data: data )

    notify 'Complete', 0, sheet_id
  end

  private

  attr_reader :character_id

  delegate :name, :user_name, :user_items, :system_id, :to => :character, :prefix => true

  def character
    @character ||= Character.find(character_id)
  end

  def compile
    character_data.tap do |h|
      h[:cost_report] = cost_report
    end
  end

  def build_traits
    @build_traits ||= Trait.for_system(character_system_id)
  end

  def character_data
    CharacterDataCompiler.compile(build_ids: @build_ids, build_traits: build_traits, character_id: character_id, user_items: character_user_items)
  end

  def cost_report
    CostReportCompiler.compile(@build_ids, build_traits)
  end

  def notify(message, percentage, sheet_id)
    ActionCable.server.broadcast "character-sheet-#{sheet_id}", :progress => percentage, :status => message
  end

  def render_engine
    character.system_render_engine
  end
end
