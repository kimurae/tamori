# A set of changes made to a character. This can represent:
# * A step in character creation.
# * A game session.
# * Spending experience points.
# Depending on the convience of the user.
class Build < ActiveRecord::Base
  belongs_to :system
  belongs_to :user

  has_and_belongs_to_many :characters, inverse_of: :builds

  has_many :build_components, dependent: :destroy

  validates :name, :presence => true

  default_scope -> {
    includes(:build_aspects, :build_attributes, :build_items, :build_properties)
  }

  scope :for_character, ->(character_id) { where(id: BuildsCharacter.where(character_id: character_id)) }

  delegate :create!, :to => :build_aspects,    :prefix => true, :allow_nil => true
  delegate :create!, :to => :build_attributes, :prefix => true, :allow_nil => true
  delegate :create!, :to => :build_essays,     :prefix => true, :allow_nil => true
  delegate :create!, :to => :build_items,      :prefix => true, :allow_nil => true
  delegate :create!, :to => :build_properties, :prefix => true, :allow_nil => true
  delegate :create!, :to => :build_ranks,      :prefix => true, :allow_nil => true

  delegate :strftime, :to => :built_on, :prefix => true, :allow_nil => true

  # @deprecated This is no longer used anywhere AFAIK. So its slated for removal.
  def components
    [
      build_attributes.to_a,
      build_properties.to_a
    ].reduce(:+).map do |component|
      component.becomes(Component)
    end
  end

  private

  has_many :build_aspects,    :dependent => :destroy
  has_many :build_attributes, :dependent => :destroy
  has_many :build_essays,     :dependent => :destroy
  has_many :build_items,      :dependent => :destroy
  has_many :build_properties, :dependent => :destroy
  has_many :build_ranks,      :dependent => :destroy

  # @private
  class Component

    def initialize
      @errors = ActiveModel::Errors.new(self)
    end

    attr_reader :errors

    def category
      read_attribute :category
    end

    def cost
      [
        read_attribute('cost'),
        read_attribute('cost_type').try(:upcase)
      ].compact.join(' ')
    end

    def id
      read_attribute :trait_id
    end

    def name
      read_attribute :name
    end

    def value
      read_attribute :value
    end

    private

    def read_attribute(attr)
      @attributes[attr.to_s].try(:value)
    end
  end
end
