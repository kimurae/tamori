# A BuildComponent where value is treated as a on/off.
# Aspects usually represent spells, techniques, or other forms of character
# data that relies mostly on the static information set in the Trait itself.
# * When aggregated it returns the last value.
class BuildAspect < BuildComponent
  attribute       :value,      :boolean
  attribute       :value_type, :string, default: 'boolean'

  alias_attribute :active, :value

  private

  def summary
    label.to_s
  end
end
