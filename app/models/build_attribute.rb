# A BuildComponent where value is treated as a numeric value.
# Attributes are numeric data such as ranks in a skill or a character's level.
# * When aggregated it returns the sum of the values.
class BuildAttribute < BuildComponent
  alias_attribute :emphasis,    :notes

  attribute       :value,       :integer
  attribute       :value_type,  :string, default: 'integer'

  # A serialized hash of its attributes.
  # @see CharacterDataCompiler
  # @return [Hash]
  def to_component
    super.merge({ :emphasis => notes, :notes => [] })
  end
end
