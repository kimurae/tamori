# An autmoatic calculation configured by the user for a game system.
# * Calculations are executed in order by priority.
# * They modify the existing data payload.
# * @see CharacterDataCompiler
class BuildCalculation < ActiveRecord::Base
  belongs_to :system
  belongs_to :trait

  enum :calculator => {
    'SumCalculator'               => 0,
    'MinCalculator'               => 1,
    'MultiplySumCalculator'       => 2,
    'L5r::InsightRankCalculator'  => 3,
    'ConcatCalculator'            => 4,
    'L5r::FullDefenseCalculator'  => 5,
    'SetIntegerValueCalculator'   => 6,
    'SetFloatValueCalculator'     => 7,
    'SetValueCalculator'          => 8,
    'Wod::BloodPoolCalculator'    => 9,
    'L5r::SumDiceCalculator'      => 10,
    'SetBooleanValueCalculator'   => 11,
    'AnyCalculator'               => 12,
    'MaxCalculator'               => 13
  }

  attribute :input_key,   SymbolAttribute.new, default: 'value'
  attribute :output_key,  SymbolAttribute.new, default: 'value'

  validates :calculator, :presence => true
  validates :input_key,  :presence => true
  validates :output_key, :presence => true
  validates :system_id,  :presence => true
  validates :trait_id,   :presence => true

  scope :by_trait, -> {
    includes(:trait).
    references(:traits).
    merge(reflect_on_association(:trait).klass.by_label)
  }

  scope :for_system, -> (system_id) { where(:system_id => system_id).order(:priority) }

  delegate :label, to: :trait, prefix: true, allow_nil: true

  # @return [String] A pseudo mathematical expression of the calculation.
  def to_s
    [
      "#{trait_label}[#{output_key}] = ",
      calculator.constantize.to_s(labels_for_calculator, argument)
    ].sum
  end

  # @param [Hash] character data grouped by trait_id.
  # @return [String,Numeric] the caclulated result.
  def value_from(trait_hash)
    calculator_calculate(
      values_from(trait_hash),
      argument
    )
  end

  private

  def calculator_calculate(*args)
    calculator.constantize.calculate(*args)
  end

  def labels_for_calculator
    Trait.where(id: trait_ids).pluck(:label).map { |i| "#{i}[#{input_key}]" }
  end

  def values_from(trait_hash)
    trait_ids.map do |t_id|
      trait_hash.with_indifferent_access.fetch(t_id, Hash.new)
    end.select do |t|
      t[self.filter_key].to_s =~ Regexp.new(self.filter_argument)
    end.map do |t|
      t.fetch(self.input_key, 0)
    end
  end
end
