# A specific change made for a character to a specific trait.
class BuildComponent < ActiveRecord::Base

  belongs_to :build
  belongs_to :trait, polymorphic: true

  # An aggregate of components grouped by trait.
  scope :compiled, CompiledComponentsQuery

  scope :for_build, -> (build_id) { where(build_id: build_id) }

  # An aggregate of component costs grouped by cost type.
  scope :summarized, -> {
   select([ :cost_type, Arel::Nodes::NamedFunction.new('SUM',[arel_table[:cost]], 'cost') ]).
   group(:cost_type)
  }

  scope :not_experience, -> {
    where.not(type: 'ExperienceAwardedAttribute')
  }

  delegate :system_id, to: :build

  # A serialized hash of its attributes.
  # @see CharacterDataCompiler
  # @return [Hash]
  def to_component
    {
      id: trait_id,
      active: true,
      summary: summary
    }.merge(serializable_hash(except: %i(id location trait_id trait_type)).symbolize_keys)
  end

  private

  delegate :label, to: :trait, allow_nil: true

  def summary
    if value.present? && value.to_s != '0'
      "#{label} #{value}"
    else
      String.new
    end
  end
end
