# A BuildComponent where value is treated as a text box.
# This is pretty much the same as Property except the UI will
# display the value as a text area rather than a single line of
# text. This is for traits like a character's description or prelude.
# * When aggregated, it returns the last value.
class BuildEssay < BuildComponent; end
