# A BuildComponent where value is treated as a quantity of a user defined trait.
# (@see UserItem)
# * aggreggates will return a sum of the values.
# * the trait properties use UserItem instead of Trait.
class BuildItem < BuildComponent
  attribute :trait_type, :string, default: 'UserItem'
  attribute :value,      :integer
  attribute :value_type, :string, default: 'integer'

  alias_attribute :qty, :value

  delegate :category_id, :category_name, :category_label, :label, :name, :book, :description, :page, :properties, to: :trait, allow_nil: true

  # A serialized hash of its attributes.
  # @see CharacterDataCompiler
  # @return [Hash]
  def to_component
    trait_to_component.merge(
    {
      id: trait_id,
      active: true,
      qty: value
    }).merge(serializable_hash(except: %i(id trait_id trait_type value)).symbolize_keys)
  end

  private

  delegate :to_component, to: :trait, allow_nil: true, prefix: true
end
