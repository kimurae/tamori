# A BuildComponent where value is treated as text.
# This is used for traits like class, or race, clan, etc.
# * When aggregated, it returns the last value.
class BuildProperty < BuildComponent; end
