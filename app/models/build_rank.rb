# A BuildComponent where value is treated as a decimal value.
# This is used for systems that have ranks and dots like L5R's glory and honor.
# * When aggregated, it returns the sum of the values.
class BuildRank < BuildComponent
  attribute :value,       :decimal
  attribute :value_type,  :string, default: 'numeric(3,2)'
end
