class BuildTemplate < ApplicationRecord
  belongs_to :owner, polymorphic: true

  has_many :build_components, as: :bloc, dependent: :destroy
end
