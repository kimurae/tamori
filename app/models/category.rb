class Category < ActiveRecord::Base
  belongs_to :system
  has_many   :traits, dependent: :destroy

  scope :for_system, -> (system_id) { where(:system_id => system_id).order(:label) }

  delegate :name, to: :system, prefix: true

  def label_with_system
    "#{system_name}: #{label}"
  end
end
