# A User created character for a roleplaying game.
class Character < ActiveRecord::Base
  belongs_to :game
  belongs_to :system
  belongs_to :user

  has_many :summaries,      dependent: :destroy
  has_many :user_items,     through:   :user

  has_many :character_sheets, :dependent => :destroy, :inverse_of => :character

  has_and_belongs_to_many :builds,         dependent: :destroy, inverse_of: :characters
  has_and_belongs_to_many :game_sessions,  dependent: :destroy, inverse_of: :characters, association_foreign_key: :build_id

  has_one_attached  :portrait

  enum status: {
    active: 0,
    archived: 1
  }

  scope :editable_by, ->(user) { where(user: user).active }

  delegate :create!,  :to => :builds, :prefix => true, :allow_nil => true
  delegate :create!,  :to => :logs,   :prefix => true, :allow_nil => true
  delegate :name,     :to => :system, :prefix => true, :allow_nil => true
  delegate :name,     :to => :user,   :prefix => true, :allow_nil => true

  delegate :create!,  :to => :character_sheets, :prefix => true, :allow_nil => true

  delegate :render_engine, :to => :system, :prefix => true, :allow_nil => true

  attribute :data, CharacterDataAttribute.new, default: Hash.new { |h,k| h[k] = Array.new }

  store_accessor :data, :summary_lists

  # @return [String] the character's description.
  def description
    data['description'].map { |i| i['value'] }.reduce(&:+).to_s
  end

  def experience_awarded_label
    Trait.experience_awarded(self.system_id).first.label
  end
end
