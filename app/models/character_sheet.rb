# An instance of the character sheet PDF generated and uploaded to AWS.
class CharacterSheet < ActiveRecord::Base
  belongs_to :character

  has_one_attached :sheet, dependent: :destroy

  scope :for_character, -> (character_id) { where(character_id: character_id) }
  scope :for_public, ->() { all }

  delegate :build_ids, to: :character, prefix: true
end
