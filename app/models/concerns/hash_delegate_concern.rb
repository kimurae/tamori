module HashDelegateConcern

  def hash_delegate(*keys, to:)
    keys.each do |k|
      define_method(k) do
        send(to)[k]
      end
    end
  end
end
