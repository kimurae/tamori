# An aggregate summary of character spending (experience, freebies, gold, etc)
class CostReport < ActiveRecord::Base
  self.table_name = 'cost_report_vw'

  # A compiled aggregate of the view that sums each cost by type.
  scope :compiled, -> (build_ids, trait_ids) {
    select(:label).
    select(Arel::Nodes::NamedFunction.new('SUM', [arel_table['xp']], 'xp')).
    select(Arel::Nodes::NamedFunction.new('SUM', [arel_table['fp']], 'fp')).
    select(Arel::Nodes::NamedFunction.new('SUM', [arel_table['dots']], 'dots')).
    where(:build_id => build_ids, :trait_id => trait_ids).
    group(:label)
  }

end
