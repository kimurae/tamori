# Optional details for a character trait.
class Detail < ActiveRecord::Base
  belongs_to :trait, :inverse_of => :detail

  store_accessor :properties, *%i(
    area
    cost
    damage
    demands
    difficulty
    duration
    level
    list
    range
    reduction
    roll
    tn_bonus
  )
end
