# A BuildComponent where value is specifically the experience awarded from a game session.
# * When aggregated it returns the sum of the values.
class ExperienceAwardedAttribute < BuildComponent
  attribute :trait_type,  :string, default: 'Trait'
  attribute :value,       :integer
  attribute :value_type,  :string, default: 'integer'
end
