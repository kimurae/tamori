class GameSession < Build

  def self.total_experience_awarded
    all.to_a.reduce(0) do |total, i|
      total + i.experience_awarded.to_i
    end
  end

  def experience_awarded
    experience_awarded_attribute_value
  end

  def experience_awarded=(new_value)
    experience_awarded_attribute ||= build_experience_awarded_attribute

    experience_awarded_attribute.comment  = "+#{new_value} #{experience_awarded_label}"
    experience_awarded_attribute.trait    = experience_awarded_trait
    experience_awarded_attribute.value    = new_value
  end

  private

  has_one :experience_awarded_attribute, autosave: true, foreign_key:  :build_id

  delegate :label, to: :experience_awarded_trait, prefix: :experience_awarded
  delegate :value, to: :experience_awarded_attribute, allow_nil: true, prefix: true

  def experience_awarded_trait
    Trait.experience_awarded(system_id).first
  end
end
