class Log < ActiveRecord::Base
  belongs_to  :character

  validates   :character,     :presence => true
  validates   :description,   :presence => true
  validates   :session_on,    :presence => true

  scope :for_character, ->(character_id) { where(:character_id => character_id) }

end
