# A Game System used to run a roleplaying game.
class System < ActiveRecord::Base
  has_many :build_calculations, :dependent => :destroy
  has_many :categories,         :dependent => :destroy
  has_many :traits,             :dependent => :destroy

  validates :name, :presence => true, :uniqueness => true

  delegate :create!, to: :build_calculations,  prefix: true
  delegate :create!, to: :categories,          prefix: true
  delegate :create!, to: :traits,              prefix: true

  enum :renderer => {
    'L5rRenderer' => 0,
    'WodRenderer' => 1,
    'GenericRenderer' => 2
  }

  # @return [Class] the class of the PDF renderer.
  def render_engine
    renderer.constantize
  end

end
