# A Character's trait. This represents the static data used by the system, not
# the actual data for a character.
class Trait < ActiveRecord::Base
  belongs_to :category, touch: true
  belongs_to :system

  has_many :build_aspects,    dependent: :restrict_with_exception, as: :trait
  has_many :build_attributes, dependent: :restrict_with_exception, as: :trait
  has_many :build_essays,     dependent: :restrict_with_exception, as: :trait
  has_many :build_properties, dependent: :restrict_with_exception, as: :trait

  has_one :detail

  scope :by_label, -> { order(:label) }

  scope :for_system, ->(system_id) {
    includes(:category).
    where(:system_id => system_id).
    order('categories.label').
    default_order
  }

  scope :for_experience, ->(system_id) {
    includes(:category).
    for_system(system_id).
    merge(Category.where(:label => 'Experience'))
  }

  scope :experience_awarded, ->(system_id) {
    for_experience(system_id).
    where(name: 'xp_awarded')
  }

  scope :default_order, -> {
    order(:manual_order).
    order(:label)
  }

  validates :category_id,  :presence => true
  validates :label,     :presence => true
  validates :name,      :presence => true
  validates :system_id, :presence => true

  accepts_nested_attributes_for :detail

  delegate :book, :page, :description, :keywords, :properties, :raises, :to => :detail, :allow_nil => true, :prefix => true

  delegate :name, :label, :to => :category, :prefix => true, :allow_nil => true

  # @return [Hash] a hash of the trait data, that can be merged with the character data.
  # @see CharacterDataCompiler
  def to_component
    {
      :id       => id,
      :active   => false,
      :category => category_label,
      :category_name => category_name,
      :label    => label,
      :name     => name,
      :book     => detail_book,
      :page     => detail_page,
      :description => detail_description,
      :keywords    => detail_keywords
    }.merge(detail_properties || {}).symbolize_keys
  end
end
