# The user logged into the site.
class User < ActiveRecord::Base
  has_secure_password

  has_many :characters

  has_many :user_items

  validates :email, :uniqueness => :true

  delegate :exists?, :find, :to => :characters, :prefix => true, :allow_nil => true

  delegate :create!, :to => :user_items, :prefix => true, :allow_nil => true
end
