# User defined traits, eventually this and Trait will be merged into one table
# and use STI and polymorphic associations with User and System.
class UserItem < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  has_many   :build_items, dependent: :restrict_with_exception, as: :trait

  delegate :name, :label, to: :category, prefix: true, allow_nil: true

  store_accessor  :properties, :devotion, :influence, :level, :list, :mentor

  alias_attribute :label, :name

  # @return [Hash] user defined trait data that can be merged with character data.
  # @see CharacterDataCompiler
  def to_component
    {
      id:         id,
      active:     false,
      category:   category_label,
      Devotion:   devotion,
      Influence:  influence,
      Mentor:     mentor
    }.merge(
      serializable_hash(:except => [:id, :properties, :category_id], :methods => [:category_name, :label, :level, :list])
    ).symbolize_keys
  end

end
