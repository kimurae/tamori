class CompiledComponentsQuery
  extend HashDelegateConcern

  class << self
    delegate :call, to: :new
  end

  private_class_method :new

  def call(build_ids)
    select([
      :type,
      :trait_id,
      :trait_type,
      last(col: location, as: 'location'),
      compile(col: value, type: value_type, as: 'value'),
      array_agg_mult(col: notes, as: 'notes')
    ]).
    select(xp_costs_for(cost_types_for(build_ids))).
    where(build_id: build_ids).
    group(:trait_id).
    group(:trait_type).
    group(:type).
    having(%q{type NOT IN ('BuildAspect', 'BuildItem') OR (type = 'BuildAspect' AND LAST(value) = 't') OR (type = 'BuildItem' AND SUM(CASE WHEN type = 'BuildItem' THEN value::integer ELSE 0 END) > 0)}).
    order(:trait_id).
    includes(:trait).
    readonly
  end

  private

  include SqlFunctionsConcern

  attr_reader :relation

  private *delegate(:arel_table, :select, to: :relation)

  private *hash_delegate(:location, :notes, :value, :value_type, to: :arel_table)

  def initialize
    @relation = BuildComponent.all
  end

  def cost_types_for(build_ids)
    select(:cost_type).
      distinct.
      where(build_id: build_ids).
      pluck(:cost_type).compact
  end

  def xp_costs_for(cost_types = [])
    cost_types.map do |i|
      sum(as: "#{i}_cost".parameterize(separator: '_')) do
        Arel::Nodes::Case.new.when(arel_table[:cost_type].eq(i)).then(arel_table[:cost]).else(0)
      end
    end
  end
end
