# AREL implementations for the SQL functions used in the app.
module SqlFunctionsConcern

  private

  def array_agg_mult(col:, as:)
    Arel::Nodes::NamedFunction.new('array_agg_mult', [col], as)
  end

  def cast(col:, as:)
    Arel::Nodes::NamedFunction.new('CAST', [col.as(as)])
  end

  def compile(col:, type:, as:)
    Arel::Nodes::NamedFunction.new('COMPILE', [col, type], as)
  end

  def last(col:, as:)
    Arel::Nodes::NamedFunction.new('LAST', [col], as)
  end

  def sum(as: nil, &block)
    Arel::Nodes::NamedFunction.new('SUM', [block.call], as)
  end
end
