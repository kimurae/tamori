module CircledTraitConcern

  private

  def circled_trait(value, loc, radius = 10)
    fill_color 'ffffff'
    transparent(0.6) do
      fill_circle   loc, radius
    end

    stroke_circle loc, radius

    fill_color '000000'

    bounding_box [loc.first - radius, loc.last + radius], :width => (radius * 2), :height => (radius * 2) do
      text value.to_s, :align => :center, :valign => :center
    end

  end
end
