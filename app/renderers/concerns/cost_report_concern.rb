module CostReportConcern

  private

  def draw_cost_report(data, columns = %w(xp))

    return unless data.fetch(:cost_report, []).size > 0

    h1('Purchase Report')

    ledger_start = bounds.width - columns.size * 60

    columns.inject(ledger_start) do |x, col|
      bounding_box [x, cursor], :width => 60, :height => 16 do
        text col.to_s.upcase, :style => :bold, :align => :right
      end

      move_up 16
      x + 60
    end

    move_down 16

    data[:cost_report].each_with_index do |entry, i|

      next if entry.slice(*columns).values.all? { |i| i == 0 }

       if( entry[:total] || ( i.even? && !entry[:subtotal] ))
         fill_color 'dddddd'
         fill_rectangle [0, cursor], bounds.width, 16
         fill_color '000000'
       end

      if entry[:subtotal]
        stroke_line [0, cursor], [bounds.width, cursor]
      end

      style = if(entry[:total] || entry[:subtotal])
        :bold
      else
        :normal
      end

      columns.inject(ledger_start) do |x, col|
        text_box entry[col].to_s, :at => [x, cursor], :width => 60, :height => 16, :style => style, :align => :right
        x + 60
      end

      text entry[:label].to_s, :style => style

      if(entry[:subtotal])
        move_down 8
      end
    end
  end
end
