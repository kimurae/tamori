module DescriptionBoxConcern

  private

  def description_box(loc, data, width, show_category = false)

    content = Array.new.tap do |content|
      content << "<b>#{data[:category]}</b>" if show_category
      content << "<b>#{data[:list]}</b>"     if data[:list].present?

      if data[:keywords].present?
        content << "[ <b>#{data.fetch(:keywords,[]).join('</b>, <b>')}</b> ]"
      end

      content << "/ #{data[:level]}" if data[:level].present?

      property_content = data.slice(:Devotion, :Influence, :Mentor).map do |property, value|
        "#{property} / #{value}"
      end.compact.join(' — ')

      content  << "— #{property_content}" if property_content.present?

      content << "— #{data[:book]} #{data[:page]}" if data[:book].present?
    end.compact.join(' ')


    text content.to_s, :inline_format => true
    text data[:description].to_s
    data.fetch(:notes, []).each do |note|
      text "• #{note}"
    end
    if data[:raises].present?
      text 'Raises', :style => :bold
      data[:raises].each do |raise|
        text "• #{raise}"
      end
    end
  end
end
