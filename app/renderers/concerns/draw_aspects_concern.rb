module DrawAspectsConcern
  include DescriptionBoxConcern

  private

  def draw_aspects(data)
    aspect_data = data.fetch(:aspects, []).select do |i|
      i[:active]
    end.sort do |a,b|
      a[:label] <=> b[:label]
    end

    return unless aspect_data.size > 0

    h1('Aspects')

    aspect_data.each do |advantage|
      move_down 10

      group do
        h2([0, cursor], advantage[:label], bounds.width)

        move_down 2

        description_box [0, cursor], advantage, bounds.width, true
      end
    end
  end
end
