module DrawEssaysConcern

  private

  def draw_essay(essay)

    return unless essay.present?
    group do
      h3([0, cursor], essay[:label].to_s, bounds.width)
      move_down 2
      text essay[:value].to_s
    end
  end
end
