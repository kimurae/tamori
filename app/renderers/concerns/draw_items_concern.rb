module DrawItemsConcern

  private

  def draw_items(data)
    return unless data.fetch(:items, []).size > 0

    data[:items].group_by do |item|
      item.fetch(:category, 'Item').pluralize
    end.sort.each do |category, items|

      h1(category)

      items.each do |item|
        fill_color '000000'
        group do
          height = [ 12 * (item[:qty].to_f / 10 ).ceil, 20].max

          item_heading = item[:label].to_s + if( item[:location])
            " (#{item[:location]})"
          else
            ''
          end

          text item_heading, :style => :bold, :size => 10

          text "— #{item[:book]} #{item[:page]}", :size => 10 if item[:book]

          text item[:description], :size => 10 if item[:description]

          item.fetch(:notes, []).each do |note|
            text "• #{note}", :size => 10
          end

          if(item.fetch(:qty, 0).to_i > 1)
            wound_boxes([0, cursor], item[:qty].to_i)
            move_down height
            fill_color '000000'
          end

        end
      end
    end
  end
end
