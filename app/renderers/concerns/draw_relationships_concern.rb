module DrawRelationshipsConcern
  include DescriptionBoxConcern

  private

  def draw_relationships(data, title = 'Relationships')

    return unless data.fetch(:relationships,[]).size > 0

    h1(title)

    data.fetch(:relationships,[]).each do |person|
      move_down 10

      group do
        h2([0, cursor], person[:label], bounds.width)

        move_down 2

        description_box([0, cursor], person, bounds.width, true)
      end
    end
  end

end
