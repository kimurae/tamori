module DrawSkillTableConcern
  include WodDotsConcern

  private

  def draw_skill_table(title: 'Skills', skills: [], max: 5)
    h1(title)

    skills.each_with_index do |skill,i|
      content = if skill.fetch(:emphasis,[]).any?
        "#{skill[:label]} (#{skill[:emphasis].join(',')})"
      else
        skill[:label].to_s
      end

      wod_dots [160, cursor + 3], skill[:value].to_i, max
      text content
    end
  end
end
