module DrawSpellsConcern
  include DescriptionBoxConcern

  private

  def draw_spells(data, title = 'Spells', order_by_list = false)
    return unless data.fetch(:spells, []).size > 0

    h1(title)

    sort_keys = %i(label)

    sort_func = if order_by_list
      lambda { |x| [x[:category].to_s, x[:list].to_s, x[:level].to_i, x[:label].to_s] }
    else
      lambda { |x| [x[:category].to_s, x[:label].to_s] }
    end

    spell_data = data.fetch(:spells,[]).select { |s| s[:active] }.sort_by(&sort_func).each do |spell|
      move_down 10

      group do
        h2([2, cursor], spell[:label].to_s, bounds.width - 2)

        move_down 2

        description_box [2, cursor], spell, bounds.width - 2, true

        stat_width = (spell_stats.map(&:length).max + 2) * 5

        spell_stats.each do |stat|
          next unless spell[stat].present? && spell[stat].to_s != '0'

          formatted_text_box [{ :text => "#{stat.titleize}:", :styles => [:bold] }], :at => [0, cursor], :width => stat_width, :height => 16, :align => :left
          text_box spell[stat].to_s, :at => [stat_width + 10, cursor], :height => 16, :align => :left

          # This moves the cursor appropos, and does not hose the calculations grouping does.
          text ' '
        end
      end
    end
  end

  def spell_stats
    %w(roll area cost damage difficulty duration range)
  end
end
