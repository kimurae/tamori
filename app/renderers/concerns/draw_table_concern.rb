module DrawTableConcern
  extend ActiveSupport::Concern

  private

  def draw_table(loc, rows, columns, header_row = false, header_col = true)
    (x, y) = loc

    width  = columns.reduce(:+)
    height = 20 * rows

    if header_row || header_col
      fill_color 'aaaaaa'
      fill_rectangle [x,y], width, height
    end

    fill_color 'ffffff'

    transparent(0.6) do
      if header_row && header_col
        fill_rectangle [x + columns.first, y], width - columns.first, 20
      elsif header_row
        fill_rectangle [x,y], width, 20
      end

      if header_col
        rows.times do |i|
          fill_rectangle [x, y - (20 * i)], columns.first, 20
        end
      end
    end

    if header_col && ! header_row
      fill_rectangle [x + columns.first,y], width - columns.first, height
    elsif header_col && header_row
      fill_rectangle [x + columns.first,y - 20], width - columns.first, height - 20
    elsif header_row && !header_col
      fill_rectangle [x,y - 20], width, height - 20
    end

    stroke_color '777777'
    stroke_rectangle [x,y], width, height

    columns.inject do |w, i|
      stroke_line [x + w, y], [x + w, y - height]
      w += i
    end

    rows.times do |i|
      stroke_line [x, y - (i * 20)], [x + width, y - (i * 20)]
    end

  end
end
