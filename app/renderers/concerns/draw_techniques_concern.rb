module DrawTechniquesConcern
  include DescriptionBoxConcern

  private

  def draw_techniques(data)
    return unless data[:techniques].present?

    h1('Techniques')

    move_down 2

    data.fetch(:techniques,[]).select { |t| t[:active] }.each do |technique|
      move_down 10
      group do
        h2([2, cursor], "#{technique[:label]}", bounds.width - 2)
        move_down 2
        description_box [2, cursor], technique, bounds.width - 2
      end
    end
 end
end
