module WodDotsConcern

  private

  def wod_dots(loc, value, max_dots = 5)
    (x, y) = loc
    fill_color '000000'
    stroke_color '000000'
    [max_dots, value].max.times do |i|
      x_offset = i * 12
      if i < value
        fill_circle   [x + x_offset ,y - 10], 5
      end
      stroke_circle [x + x_offset ,y - 10], 5
    end
  end
end
