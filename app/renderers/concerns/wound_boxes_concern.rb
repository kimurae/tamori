module WoundBoxesConcern

  private

  def wound_boxes(loc, wounds, boxes_per_row = 10)
    (x, y) = loc
    fill_color 'ffffff'
    stroke_color '777777'
    wounds.times do |i|
      x_offset = ( i % boxes_per_row ) * 10
      y_offset = ( i / boxes_per_row ) * 10
      fill_rectangle   [x + x_offset ,y - y_offset], 8,8
      stroke_rectangle [x + x_offset ,y - y_offset], 8,8
    end
  end
end
