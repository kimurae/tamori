# Renders a generic character sheet.
class GenericRenderer < Prawn::Document

  def save_as(*args)
    render_file(*args)
  end

  def initialize(data)

    super(margin: Array.new(3,4), page_layout: :landscape)

    data.default = Array.new

    setup_fonts

    draw_name data
    move_cursor_to document_start

    if data[:experience].present?
      detail label: data[:experience][:label].to_s,  content: data[:experience][:value].to_s, loc: [bounds.width - 260, document_start], width: 260, alignment: :right
    end

    move_down 20

    bounding_box [0, document_start - 20], width: bounds.width / 3 - 2 do
      draw_details details: [{ label: 'Player', value: data[:player]}]
      draw_details details: data[:details]
      draw_details details: data[:renown]
    end

    bounding_box [bounds.width / 3, document_start - 20], width: bounds.width / 3 - 2 do
      data[:attributes].group_by do |i|
        i[:category]
      end.each do |k,v|
        draw_attributes attributes: v
      end

      data[:skills].group_by do |i|
        i[:category]
      end.each do |k,v|
        draw_skill_table skills: v
      end

      draw_wounds      data
    end

    bounding_box [2 * (bounds.width / 3), document_start - 20], :width => bounds.width / 3 - 2 do
      data[:aspects].select do |i|
        i[:active]
      end.group_by do |i|
        i[:category]
      end.each do |(k, v)|
        h1(k)
        v.sort_by do |i|
          i[:label]
        end.each do |i|
          text i[:label]
        end
      end
    end

    if data.has_key?(:items)
      start_new_page
      draw_name data
      move_cursor_to document_start
      draw_items data
    end

    if data.has_key?(:aspects)
      start_new_page layout: :portrait
      draw_name data
      move_cursor_to document_start
      draw_aspects data
    end

    if data.has_key?(:relationships)
      start_new_page

      draw_name data

      move_cursor_to document_start

      draw_relationships data, 'Allies and Enemies'
    end

    start_new_page
    draw_name data
    move_cursor_to document_start
    draw_cost_report data, %w(xp)
  end

  private

  include CostReportConcern
  include DrawAspectsConcern
  include CircledTraitConcern
  include DrawItemsConcern
  include DrawRelationshipsConcern
  include DrawSkillTableConcern
  include DrawTableConcern
  include DrawSpellsConcern
  include DrawTechniquesConcern
  include WoundBoxesConcern

  # Possible Parent class stuff here.

  def setup_fonts
    font_families.update("Bradley" => {
      :normal => "#{Rails.root}/vendor/fonts/bradley.ttf"
    })

    font_families.update("Optima" => {
      :normal => "#{Rails.root}/vendor/fonts/Optima-Regular.ttf",
      :bold   => "#{Rails.root}/vendor/fonts/Optima-Bold.ttf",
      :italic => "#{Rails.root}/vendor/fonts/Optima-Italic.ttf",
      :bold_italic => "#{Rails.root}/vendor/fonts/Optima-BoldItalic.ttf",
      :extra_black => "#{Rails.root}/vendor/fonts/Optima-ExtraBlack.ttf"
    })

    font 'Optima'
    font_size 12
  end

  def h1(content, loc: [0, cursor], width: bounds.width)
    fill_color '000000'
    font('Bradley', :size => 16) do
      bounding_box loc, :width => width, :height => 20 do
        text content.to_s, :align => :center, :valign => :center
      end
    end
  end

  def h2(_loc, content, _width, header_size = 14)
    font('Optima', :size => header_size) do
      text content.to_s
    end
  end

  # L5R Specific

  def document_start
    bounds.top - 22
  end

  def draw_attributes(attributes: [], max: 5)
    h1(attributes.first[:category]) if attributes.present?

    attributes.each do |attribute|
      wod_dots [160, cursor + 3], attribute[:value].to_i, max
      text attribute[:label]
    end
  end

  def draw_details(details: [], label: true)
    h1(details.first[:category]) if label && details.present?

    details.each do |i|
      detail    label: i[:label], content: i[:value], loc: [0, cursor], width: bounds.width
      move_down 20
    end
  end

  def draw_name(data)
    font('Bradley', :size => 18) do
      text data.fetch(:name, {})[:value].to_s
    end
    stroke { line [0,bounds.top - 20], [bounds.width, bounds.top - 20] }
  end

  def draw_wounds(data)
    h1('Wounds')

    data.fetch(:wounds,[]).inject(cursor - 23) do |wound_y, wound|
      height = [ 12 * (wound[:value].to_f / 10 ).ceil, 20].max

      wound_boxes [122,cursor - 3], wound[:value].to_i

      fill_color '000000'
      font 'Optima'
      font_size 12

      bounding_box [2,cursor], :height => height, :width => 118 do
        text wound[:label].to_s, :style => :bold, :valign => :top, :align => :right
      end

      wound_y - height
    end
  end

  def detail(label:, content:, loc:, width: 80, alignment: :left)
    return unless label.present?
    formatted_text_box( [
      { :text => "#{label}: ", :styles => [:bold] },
      { :text => content.to_s }
    ], :at => loc, :width => width, :height => 20, :align => alignment)
  end
end
