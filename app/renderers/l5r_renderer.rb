# Renders a character sheet for Legend of the Five Rings (AEG).
class L5rRenderer < Prawn::Document

  def save_as(*args)
    render_file(*args)
  end

  def initialize(data)

    super(margin: Array.new(3,4))

    setup_fonts

    draw_name data

    # Was 720 Add 64 to every Y coordinate.
    detail 'Player',                    data[:player],                               [0, document_start], 138
    if data[:level].present?
      detail data[:level][:label],        data[:level][:value],                        [180, document_start],120
    end

    if data[:experience].present?
      detail data[:experience][:label].to_s,  data[:experience][:value].to_s, [280, document_start], 260, :right
    end

    schools = data.fetch(:schools,[]).select { |d| d['active'] }.map do |school|
      "#{school.fetch(:label,'')} / #{school.fetch(:value,0)}"
    end.join(' ')

    offset = data.fetch(:factions, []).inject(0) do |memo, i|
      detail i.fetch(:label, ''), i.fetch(:value,''), [memo, document_start - 14]
      memo + 80
    end

    draw_renown x: document_start - 28, data: data

    detail 'School(s)',schools,     [180,document_start - 14], 360

    bounding_box [10, document_start - 58], :width => 258 do
      draw_attributes  data
      draw_combat      data
      draw_armor_table data
      draw_equipment_table key: :armor, data: data, with: %i(tn_bonus reduction)
      draw_equipment_table key: :weapons, data: data, with: %i(attack damage)
      draw_wounds      data
    end

    bounding_box [270, document_start - 50], :width => 158 do
      draw_school_skills [270, document_start - 50 ], data
      draw_skills        [270, document_start - 238], data
    end

    start_new_page

    draw_name data

    move_cursor_to document_start

    draw_aspects data

    start_new_page

    draw_name data

    move_cursor_to document_start

    draw_items data

    start_new_page

    draw_name data

    move_cursor_to document_start

    draw_techniques data
    draw_spells data, 'Kata/Kiho/Spells'

    if data.has_key?(:relationships)
      start_new_page

      draw_name data

      move_cursor_to document_start

      draw_relationships data, 'Allies and Enemies'
    end

    start_new_page

    draw_name data

    move_cursor_to document_start

    draw_cost_report data, %w(xp)
  end

  private

  include CostReportConcern
  include DrawAspectsConcern
  include CircledTraitConcern
  include DrawItemsConcern
  include DrawRelationshipsConcern
  include DrawSkillTableConcern
  include DrawTableConcern
  include DrawSpellsConcern
  include DrawTechniquesConcern
  include WoundBoxesConcern

  # Possible Parent class stuff here.

  def setup_fonts
    font_families.update("Bradley" => {
      :normal => "#{Rails.root}/vendor/fonts/bradley.ttf"
    })

    font_families.update("Optima" => {
      :normal => "#{Rails.root}/vendor/fonts/Optima-Regular.ttf",
      :bold   => "#{Rails.root}/vendor/fonts/Optima-Bold.ttf",
      :italic => "#{Rails.root}/vendor/fonts/Optima-Italic.ttf",
      :bold_italic => "#{Rails.root}/vendor/fonts/Optima-BoldItalic.ttf",
      :extra_black => "#{Rails.root}/vendor/fonts/Optima-ExtraBlack.ttf"
    })

    font 'Optima'
    font_size 12
  end

  def h1(content, loc: [0, cursor], width: bounds.width)
    fill_color '000000'
    font('Bradley', :size => 16) do
      bounding_box loc, :width => width, :height => 20 do
        text content.to_s, :align => :center, :valign => :center
      end
    end
  end

  def h2(_loc, content, _width, header_size = 14)
    font('Optima', :size => header_size) do
      text content.to_s
    end
  end

  # L5R Specific

  def document_start
    bounds.top - 22
  end

  def draw_armor_table(data)
    table_data = data.fetch(:armor_tn,[]).inject({}) do |memo, i|
      memo.merge(i.fetch(:name) => i.fetch(:value, 0))
    end

    h1('Armor TN')

    bounding_box [2, cursor], :height => 20, :width => 48 do
      text 'Attack', :style => :bold, :valign => :bottom, :size => 12, :font => 'Optima'
    end

    move_up 20

    bounding_box [42, cursor], :height => 20, :width => 48 do
      text 'Full Atk', :style => :bold, :valign => :bottom, :size => 12, :font => 'Optima'
    end

    move_up 20

    bounding_box [92, cursor], :height => 20, :width => 48 do
      text 'Defense', :style => :bold, :valign => :bottom, :size => 12, :font => 'Optima'
    end

    move_up 20

    bounding_box [142, cursor], :height => 20, :width => 104 do
      text 'Full Defense', :style => :bold, :align => :center, :valign => :bottom, :size => 12, :font => 'Optima'
    end

    stroke { line [2,cursor], [bounds.width - 2, cursor] }

    bounding_box [2, cursor], :height => 20, :width => 48 do
      text table_data['armor_tn_attack'].to_s, :valign => :bottom, :size => 12, :align => :center
    end

    move_up 20

    bounding_box [42, cursor], :height => 20, :width => 48 do
      text table_data['armor_tn_full_attack'].to_s, :valign => :bottom, :size => 12, :align => :center
    end

    move_up 20

    bounding_box [92, cursor], :height => 20, :width => 48 do
      text table_data['armor_tn_defense'].to_s, :valign => :bottom, :size => 12, :align => :center
    end

    move_up 20

    bounding_box [142, cursor], :height => 20, :width => 104 do
      text table_data['armor_tn_full_defense'].to_s, :valign => :bottom, :size => 12, :align => :center
    end
  end

  def draw_attributes(data)
    ring_data = data.fetch(:rings,[]).inject({}) do |memo, i|
      memo.merge( i[:name] => i[:value].to_s )
    end

    ring label: 'Air',   value: ring_data['air_ring'].to_i,   traits: data.fetch(:air_attributes, [])
    ring label: 'Earth', value: ring_data['earth_ring'].to_i, traits: data.fetch(:earth_attributes, [])
    ring label: 'Fire',  value: ring_data['fire_ring'].to_i,  traits: data.fetch(:fire_attributes, [])
    ring label: 'Water', value: ring_data['water_ring'].to_i, traits: data.fetch(:water_attributes, [])
    ring label: 'Void',  value: ring_data['void_ring'].to_i,  traits: Array.new, border: false
  end

  def draw_combat(data)

    h1('Combat Modifiers', width: 230)

    data.fetch(:combat_modifiers,[]).each_with_index do |modifier, i|

      bounding_box [2, cursor], :height => 16, :width => 68 do
        text modifier[:label].to_s, :valign => :bottom, :style => :bold
      end

      move_up 16

      bounding_box [45, cursor], :height => 16, :width => 90 do
        text modifier[:value].to_s, :valign => :bottom, :align => :center
      end
    end
  end

  def draw_equipment_table(data:, key:, with: [])
    equipment_data = data.fetch(key, []).select { |i| i.fetch(:active) == true }

    return unless equipment_data.any?

    h1(key.to_s.titleize.pluralize, width: 230)

    bounding_box [0, cursor], :height => 20, :width => 148 do
      text 'Name', :style => :bold, :align => :center, :valign => :bottom, :size => 12, :font => 'Optima'
    end

    with.each_with_index do |col, idx|

      move_up 20

      bounding_box [150 + (60 * idx), cursor], :height => 20, :width => 58 do
        text col.to_s.titleize, :style => :bold, :align => :center, :valign => :bottom, :size => 12, :font => 'Optima'
      end
    end

    stroke { line [0,cursor], [bounds.width - 2, cursor] }

    equipment_data.each do |row|
      bounding_box [0, cursor], :height => 20, :width => 148 do
        text row['label'], :align => :center, :valign => :bottom, :size => 12, :font => 'Optima'
      end

      with.each_with_index do |col, idx|
        move_up 20

        bounding_box [150 + (60 * idx), cursor], :height => 20, :width => 58 do
          text row[col.to_s], :align => :center, :valign => :bottom, :size => 12, :font => 'Optima'
        end
      end
    end
  end

  def draw_name(data)

    font('Bradley', :size => 18) do
      text data.fetch(:name, {})[:value].to_s
    end
    stroke { line [0,bounds.top - 20], [bounds.width, bounds.top - 20] }
  end

  def draw_school_skills(loc, data )
    draw_skill_table(title: 'School Skills', skills: data.fetch(:school_skills,[]).select { |s| s[:active] }, max: 10)
  end

  def draw_skills(loc, data)
    draw_skill_table(skills: data.fetch(:skills,[]).select { |s| s[:active] }, max: 10)
  end

  def draw_renown(x:, data:)
    data.fetch(:renown, []).inject(0) do |memo, i|
      detail i.fetch(:label, ''), i.fetch(:value, ''), [memo, x]

      if (80..179).include? memo
        180
      else
        memo + 80
      end
    end
  end

  def draw_wounds(data)
    h1('Wounds', width: 230)

    data.fetch(:wounds,[]).inject(cursor - 23) do |wound_y, wound|
      height = [ 12 * (wound[:value].to_f / 10 ).ceil, 20].max

      wound_boxes [122,cursor - 3], wound[:value].to_i

      fill_color '000000'
      font 'Optima'
      font_size 12

      bounding_box [2,cursor], :height => height, :width => 118 do
        text wound[:label].to_s, :style => :bold, :valign => :top, :align => :right
      end

      wound_y - height
    end
  end

  def detail(label, content, loc, width = 80, alignment = :left)
    return unless label.present?
    formatted_text_box( [
      { :text => "#{label}: ", :styles => [:bold] },
      { :text => content.to_s }
    ], :at => loc, :width => width, :height => 20, :align => alignment)
  end

  def ring(label:, value:, traits:, border: true)
    move_down 10
    wod_dots [80, cursor + 0.4], value.to_i, 10
    font('Bradley', :size => 16) { text label }
    stroke { line [0,cursor], [195, cursor] } if border

    traits.each do |trait|
      wod_dots [80, cursor + 3], trait[:value].to_i, 10
      text trait[:label]
    end
  end
end
