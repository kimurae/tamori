# Renders a character sheet that's used for a Storyteller System.
class WodRenderer < Prawn::Document

  def save_as(*args)
    render_file(*args)
  end

  def initialize(data)

    super()

    setup_fonts

    draw_name data

    move_cursor_to 698

    if data[:prelude].present? || data[:description].present?
      draw_essay(data[:prelude].first)
      draw_essay(data[:description].first)

      start_new_page

      draw_name data

      move_cursor_to 698
    end

    detail 'Player',                    data[:player],                                  [0, 698], 138
    detail 'Nature',                    data.fetch(:aspects,[{}]).detect( -> { Hash.new }) { |i| i[:category] == 'Nature'   && i[:active] }[:label].to_s, [0, 682], 138
    detail 'Demeanor',                  data.fetch(:aspects,[{}]).detect( -> { Hash.new }) { |i| i[:category] == 'Demeanor' && i[:active] }[:label].to_s, [0, 666], 138

    if data[:experience].present?
      detail data[:experience][:label].to_s,  data[:experience][:value].to_s, [340, 698], 200, :right, 140
    end

    if data[:level].present?
      detail 'Rank', data[:level][:value], [340, 682], 200, :right
    end

    offset = data.fetch(:factions, []).select { |i| i[:active] }.inject(698) do |memo, i|
      detail i[:label], i[:value], [140, memo], 200, :left, 70
      memo - 16
    end

    move_cursor_to offset

    h2([0, cursor], 'Attributes', bounds.width, 14, :center)

    float do
      bounding_box [10, cursor], :width => 170, :height => cursor do
        draw_attributes('Physical', :physical_attributes, data)
      end
    end

    float do
      bounding_box [180, cursor], :width => 170, :height => cursor do
        draw_attributes('Social', :social_attributes, data)
      end
    end

    bounding_box [355, cursor], :width => 170 do
      draw_attributes('Mental', :mental_attributes, data)
    end

    move_down 2

    h2([0, cursor], 'Abilities', bounds.width, 14, :center)

    float do
      bounding_box [10, cursor], :width => 170, :height => cursor do
        draw_active_attributes('Talents', :talents, data)
      end
    end

    float do
      bounding_box [180, cursor], :width => 170, :height => cursor do
        draw_active_attributes('Skills', :skills, data)
      end
    end

    bounding_box [355, cursor], :width => 170 do
      draw_active_attributes('Knowledge', :knowledge, data)
    end

    move_down 2

    h2([0, cursor], 'Advantages', bounds.width, 14, :center)

    float do
      bounding_box [10, cursor], :width => 170, :height => cursor do
        if data[:spells].present?
          draw_disciplines(data)
        end
      end
    end

    float do
      bounding_box [180, cursor], :width => 170, :height => cursor do
        draw_active_attributes('Backgrounds', :backgrounds, data)
      end
    end

    float do
      bounding_box [355, cursor], :width => 170, :height => cursor do
        draw_active_attributes('Virtues', :virtues, data)
        move_down 10
      end
    end

    move_down 140

    h2([0, cursor], ' ', bounds.width)

    float do
      bounding_box [10, cursor], width: 130, height: cursor do
        %w(Merit Flaw).map do |category|
          {
            category: category,
            aspects:  data.fetch(:aspects,[]).select do |i|
                        i[:active] && i[:category] == category
                      end.sort_by do |i|
                        i[:label]
                      end
          }
        end.select do |i|
          i[:aspects].present?
        end.each do |i|
          h1(i[:category].pluralize, font_size: 16)
          i[:aspects].map do |aspect|
            aspect[:label] + if(aspect[:notes].present?)
              "(#{aspect[:notes].first})"
            else
              String.new
            end
          end.each do |aspect|
            text(aspect)
          end
        end
      end
    end

    float do
      bounding_box [135, cursor], width: 130, height: cursor do
        [data.fetch(:aspects,[]).select do |i|
          i[:active] && i[:category] == 'Language'
        end.map do |i|
          i[:label]
        end.sort].select do |i|
          i.present?
        end.each do |i|
          h1('Languages', font_size: 14)
          i.each do |language|
           text(language)
          end
        end

        [data.values_at(:physical_attributes, :mental_attributes, :social_attributes, :talents, :skills, :knowledge).compact.flat_map do |i|
          i.flat_map do |j|
            j.fetch(:emphasis, Array.new).map do |emphasis|
              "#{j[:label]} (#{emphasis})"
            end
          end
        end.select(&:present?)].each do |i|
          h1('Specializations', font_size: 14)
          i.each do |specialization|
            text(specialization)
          end
        end
      end
    end

    float do
      bounding_box [275, cursor], :width => 170, :height => cursor do
        data.fetch(:paths, []).select do |path|
          path[:active]
        end.each do |path|
          h1(path[:label], font_size: 14)
          wod_dots([30, cursor], path.fetch(:value, 0).to_i, 10)
          move_down 15
        end

        data.fetch(:bearings, []).each do |bearing|
          text("#{bearing[:label]}: #{bearing[:value]}", :align => :center) if bearing[:value].present?
        end

        data.fetch(:willpower, []).each do |willpower|
          h1('Willpower', font_size: 14)
          wod_dots([30, cursor], willpower.fetch(:value,0).to_i, 10)
          move_down 20
          wound_boxes([30, cursor], 10, 10)
          move_down 15
        end

        data.fetch(:pools, []).select { |i| i[:active] }.each do |pool|
          h1(pool[:label], font_size: 14)
          move_down 2
          wound_boxes([10, cursor], pool.fetch(:value, 0).to_i, 15)
        end
      end
    end

    float do
      bounding_box [390, cursor], :width => 135, :height => cursor do
        draw_wounds         [10,cursor], data
      end
    end

    start_new_page
    draw_name       data
    move_cursor_to  698
    draw_aspects    data

    if data[:items].present?
      start_new_page
      draw_name       data
      move_cursor_to  698
      draw_items data
    end

    if data[:spells].present?
      start_new_page
      draw_name data
      move_cursor_to 698
      draw_spells data, 'Disciplines', sort_by_list = true
    end

    if data[:relationships].present?
      start_new_page
      draw_name data
      move_cursor_to 698
      draw_relationships data, 'Allies, Contacts, and Retainers'
    end

    start_new_page
    draw_name data
    move_cursor_to 698
    draw_cost_report data, %w(xp fp)

  end

  private

  include CostReportConcern
  include DrawAspectsConcern
  include DrawEssaysConcern
  include DrawItemsConcern
  include DrawRelationshipsConcern
  include DrawSpellsConcern
  include WodDotsConcern
  include WoundBoxesConcern

  # Possible Parent class stuff here.

  def setup_fonts
    font_families.update("Bradley" => {
      :normal => "#{Rails.root}/vendor/fonts/bradley.ttf"
    })

    font_families.update("Optima" => {
      :normal => "#{Rails.root}/vendor/fonts/Optima-Regular.ttf",
      :bold   => "#{Rails.root}/vendor/fonts/Optima-Bold.ttf",
      :italic => "#{Rails.root}/vendor/fonts/Optima-Italic.ttf",
      :bold_italic => "#{Rails.root}/vendor/fonts/Optima-BoldItalic.ttf",
      :extra_black => "#{Rails.root}/vendor/fonts/Optima-ExtraBlack.ttf"
    })

    font 'Optima'
    font_size 12
  end

  def h1(content, loc: [0,cursor], width: bounds.width, font_size: 16)
    fill_color '000000'
    font('Optima', :size => font_size) do
      bounding_box loc, :width => width, :height => 20 do
        text content.to_s, :align => :center, :valign => :center, :style => :bold
      end
    end
  end

  def h2(loc, content, width, _size = 14, _align = :left)
    ( x, y ) = loc

    fill_color 'dddddd'
    fill_rectangle [0, cursor], width, 20

    fill_color '000000'
    font('Optima', :size => _size) do
      bounding_box [x + 2, cursor], :width => width - 2, :height => 20 do
        text content.to_s, :valign => :bottom, :align => _align
      end
    end
  end

  def h3(loc, content, width, _size = 12)
    fill_color '000000'
    font('Optima', :size => _size) do
      bounding_box loc, :width => width, :height => 20 do
        text content.to_s, :align => :center, :valign => :center, :style => :bold
      end
    end
  end

  def draw_attribute(loc, label, value)
    wod_dots([110, cursor + 3], value)
    text label.to_s
  end

  def draw_attributes(label, category, data)
    h3([0, cursor], label, bounds.width, 12)
    data.fetch(category,[]).each do |attribute|
      draw_attribute([0,cursor], attribute[:label].to_s, attribute.fetch(:value,0).to_i)
    end
  end

  def draw_active_attributes(label, category, data)
    if data.fetch(category, []).any? { |a| a[:active] }
      h3([0, cursor], label, bounds.width)
      data.fetch(category,[]).select { |a| a[:active] }.each do |attribute|
        draw_attribute([0,cursor], attribute[:label].to_s, attribute.fetch(:value,0).to_i)
      end
    end
  end

  def draw_disciplines(data)
    h3([0,cursor], 'Disciplines', bounds.width)
    data.fetch(:spells, []).select { |i| i[:active] }.group_by { |i| i[:list] || i[:label] }.each do |k,v|
      draw_attribute([0, cursor], k, v.flat_map { |i| [i.fetch(:value, 0).to_i, i.fetch(:level, 0).to_i] }.max)
    end
  end

  def draw_name(data)

    font('Optima', :size => 18) do
      text data.fetch(:name, {})[:value].to_s
    end
    stroke { line [0,700], [bounds.width, 700] }
  end

  def draw_wounds(loc, data)
    (x, y) = loc

    h1('Health', loc: loc, width: 230)

    data.fetch(:wounds,[]).inject(y - 23) do |wound_y, wound|
      height = [ 12 * (wound[:value].to_f / 10 ).ceil, 20].max
      fill_color '000000'
      font 'Optima'
      font_size 12

      bounding_box [x + 2,wound_y], :height => height, :width => 118 do
        text wound[:label].to_s, :style => :bold, :valign => :top, :align => :right
      end
      wound_boxes [x + 122,wound_y], wound[:value].to_i

      wound_y - height
    end
  end

  def detail(label, content, loc, width = 80, alignment = :left, label_width = 65)
    (x, y) = loc
    return unless label.present?
    bounding_box [x,y], :width => label_width, :height => 16 do
      text "#{label}: ", :style => :bold
    end
    bounding_box [x + label_width, y], :width => width - label_width, :height => 16 do
      text_box content.to_s, :align => alignment, :overflow => :shrink_to_fit, :height => bounds.height, :width => bounds.width
    end
  end

end
