class DocumentationService

  def self.libraries
    @libraries ||= new.yield_self do |srv| 
      srv.send(:_generate!)
      srv.send(:_libraries)
    end
  end

  private

  private_class_method :new

  def _gem_specs
    Bundler::LockfileParser.new(_lockfile).specs
  end

  def _generate!
    Dir.chdir(Rails.root) do
      YARD::CLI::Yardoc.run('-n --plugin activerecord')
    end
  end

  
  def _libraries
    _gem_specs.inject({ 'tamori' => [_project_library] }) do |memo, spec|
      memo.tap do |library_hash|
        library_hash[spec.name] ||= Array.new
        library_hash[spec.name] |= [_spec_library_for(spec)]
      end
    end
  end

  def _lockfile
    File.read(File.join(Rails.root, 'Gemfile.lock'))
  end

  def _project_library
    YARD::Server::LibraryVersion.new('tamori', nil, _yardoc_file)
  end

  def _spec_library_for(spec)
    YARD::Server::LibraryVersion.new(spec.name, spec.version.to_s, nil, :gem)
  end

  def _yardoc_file
    File.join(Rails.root, YARD::Registry::DEFAULT_YARDOC_FILE)
  end
end
