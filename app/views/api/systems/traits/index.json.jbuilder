json.array! traits.to_a.group_by(&:category_label) do |(category,category_traits)|
  json.category category
  json.category_id category_traits.first.category_id
  json.traits category_traits do |trait|
    json.id           trait.id
    json.manual_order trait.manual_order
    json.name         trait.label
  end
end
