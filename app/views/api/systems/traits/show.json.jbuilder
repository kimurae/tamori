json.id           current_trait.id
json.category     current_trait.category_label
json.category_id  current_trait.category_id
json.label        current_trait.label
json.manual_order current_trait.manual_order
json.name         current_trait.name

json.detail_attributes do
  json.book         current_trait.detail_book
  json.page         current_trait.detail_page
  json.description  current_trait.detail_description
  json.keywords     current_trait.detail_keywords
  json.properties   current_trait.detail_properties
  json.raises       current_trait.detail_raises
end
