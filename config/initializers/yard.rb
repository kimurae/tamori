# Override the scopes definition
module YARD::Handlers::Ruby::ActiveRecord::Scopes
  class ScopeHandler < YARD::Handlers::Ruby::MethodHandler
    def process
      object = register YARD::CodeObjects::MethodObject.new(namespace, method_name, :class)

      object.docstring = return_description unless object.docstring.present?

      object.docstring.add_tag get_tag(:return, '', class_name)
      object.docstring.add_tag get_tag(:see,"ActiveRecord::Scoping", nil,
        'http://api.rubyonrails.org/classes/ActiveRecord/Scoping/Named/ClassMethods.html')
    end
  end
end

