require 'sidekiq/web'

Rails.application.routes.draw do
  scope :format => true, :constraints => { :format => /json/ } do
    resources :summaries,  :only => [:show]
  end

  mount ActionCable.server => '/cable'

  scope :constraints => AuthenticatedConstraint.new do

    resources :characters, except: [:destroy] do
      scope module: 'characters' do
        resources :builds,  except: [:new] do
          resources :build_aspects,     except: %i(index show)
          resources :build_attributes,  except: %i(index show)
          resources :build_essays,      except: %i(index show)
          resources :build_items,       except: %i(index show)
          resources :build_properties,  except: %i(index show)
          resources :build_ranks,       except: %i(index show)
        end
        resources :game_sessions, except: %i(new)
        resources :sheets,        except: %i(new edit)
      end
    end

    namespace :api do
      resources :systems, only: %i(index) do
        scope module: 'systems' do
          resources :traits,      only: %i(show index)
        end
      end
    end

    resources :user_items, except: %i(show)

    get '/logout', :to => 'sessions#destroy'

    root :to => 'characters#index'
  end

  scope :constraints => AdminAuthenticatedConstraint.new do

    mount DocServer.new => '/docs'    if Rails.application.config.yard_server               == :enabled
    mount Sidekiq::Web  => '/sidekiq' if Rails.application.config.active_job.queue_adapter  == :sidekiq

    resources :systems, only: %i(index show new create), shallow: true do
      scope module: 'systems' do
        resources :build_calculations, except: %i(destroy show)
        resources :categories,         except: %i(destroy show)
      end
    end

    resources :systems, only: %i(index show new create) do
      scope module: 'systems' do
        resources :traits, except: %i(destroy show)
      end
    end

    get '/admin', :to => 'systems#index'
  end

  post '/login', :to => 'sessions#create'

  scope :constraints => lambda { |r| r.env['warden'].user.nil? } do
    get '/', :to => 'sessions#new'
  end

  match '(*)', :via => [:get, :post, :put, :delete, :patch],
               :to  => proc { [404, {}, ['']] }, :as => :not_found
end
