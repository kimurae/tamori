-- Create a function that takes the value and value_type, then decide what to do with it based on its type.
CREATE OR REPLACE FUNCTION public.compile_value(memo text, value text, value_type text)
  RETURNS text LANGUAGE SQL IMMUTABLE STRICT AS $$
  SELECT  CASE $3
          WHEN 'integer' THEN CAST ( ($1::integer + $2::integer) AS text)
          WHEN 'numeric(3,2)' THEN CAST ( ($1::numeric(3,2) + $2::numeric(3,2)) AS text)
          ELSE
            $1
          END;
$$;

CREATE AGGREGATE public.COMPILE(text, text) (
  sfunc = public.compile_value,
  stype = text 
);
