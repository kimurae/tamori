class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.integer :game_id
      t.string  :system
      t.timestamps
    end

    add_index :characters, :game_id
  end
end
