class CreateBuilds < ActiveRecord::Migration
  def change
    create_table :builds do |t|
      t.integer :character_id
      t.timestamps
    end

    add_index :builds, :character_id
  end
end
