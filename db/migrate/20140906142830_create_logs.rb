class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.integer :character_id
      t.timestamps
    end

    add_index :logs, :character_id
  end
end
