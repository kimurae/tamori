class CreateSummaries < ActiveRecord::Migration
  def change
    create_table :summaries do |t|
      t.integer :character_id

      t.timestamps
    end

    add_index :summaries, :character_id
  end
end
