class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest

      t.timestamps
    end

    add_column :characters, :user_id, :integer
    add_index  :characters, :user_id
    add_index  :characters, [:user_id, :id]

  end
end
