class AddXpToLog < ActiveRecord::Migration
  def change
    add_column :logs, :description, :string
    add_column :logs, :xp,          :integer
    add_column :logs, :session_on,  :date
    add_column :logs, :notes,       :text
  end
end
