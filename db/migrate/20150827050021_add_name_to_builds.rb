class AddNameToBuilds < ActiveRecord::Migration
  def change
    add_column :builds, :name, :string
  end
end
