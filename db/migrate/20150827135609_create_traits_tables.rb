class CreateTraitsTables < ActiveRecord::Migration
  def change

    enable_extension 'uuid-ossp'

    create_table :build_attributes do |t|
      t.integer :build_id
      t.string  :category
      t.integer :cost
      t.string  :cost_type
      t.string  :name
      t.uuid    :trait_id
      t.integer :value
      t.timestamps
    end

    create_table :build_properties do |t|
      t.integer :build_id
      t.string  :category
      t.integer :cost
      t.string  :cost_type
      t.string  :name
      t.uuid    :trait_id
      t.string  :value
      t.timestamps
    end

    create_table :traits, :id => false do |t|
      t.primary_key :id, :uuid, :default => 'uuid_generate_v1()'
      t.string :name
      t.timestamps
    end
  end
end
