class CreateRankedAttribute < ActiveRecord::Migration
  def change
    create_table :build_ranked_attributes do |t|
      t.integer :build_id
      t.string  :category
      t.integer :cost
      t.string  :cost_type
      t.string  :name
      t.uuid    :trait_id
      t.decimal :value, :precision => 1
    end
  end
end
