class AddNameAndCategoryToTraits < ActiveRecord::Migration
  def change
    add_column :traits, :label, :string
    add_column :traits, :category, :string
  end
end
