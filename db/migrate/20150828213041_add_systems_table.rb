class AddSystemsTable < ActiveRecord::Migration
  def change
    create_table :systems, id: :uuid, default: "uuid_generate_v1()", force: :cascade do |t|
      t.string :name
      t.timestamps
    end

    add_column :traits, :system_id, :uuid
  end

end
