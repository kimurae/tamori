class CreateDetails < ActiveRecord::Migration
  def change
    enable_extension 'hstore'
    create_table :details do |t|
      t.uuid   :trait_id
      t.string :book
      t.string :page
      t.text :description
      t.string :keywords, :array => true, :default => []
      t.hstore :properties, :default => {}
      t.timestamps
    end
  end
end
