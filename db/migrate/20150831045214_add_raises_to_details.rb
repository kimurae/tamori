class AddRaisesToDetails < ActiveRecord::Migration
  def change
    add_column :details, :raises, :string, :array => true, :default => []
  end
end
