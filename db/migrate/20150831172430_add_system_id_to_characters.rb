class AddSystemIdToCharacters < ActiveRecord::Migration
  def change
    add_column    :characters, :system_id, :uuid
    add_column    :characters, :name, :string
    remove_column :characters, :system
  end
end
