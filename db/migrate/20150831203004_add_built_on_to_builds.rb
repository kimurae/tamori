class AddBuiltOnToBuilds < ActiveRecord::Migration
  def change
    add_column :builds, :built_on, :date
  end
end
