class AddCommentToBuildAttributes < ActiveRecord::Migration
  def change
    add_column    :build_attributes, :comment, :string
    remove_column :build_attributes, :name,    :string
    add_column    :build_properties, :comment, :string
    remove_column :build_properties, :name,    :string
  end
end
