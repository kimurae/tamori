class CreateAspects < ActiveRecord::Migration
  def change
    create_table :build_aspects do |t|
      t.integer :build_id
      t.uuid    :trait_id
      t.boolean :active
      t.string  :comment
      t.integer :cost
      t.string  :cost_type
      t.string  :notes, :array => true, :default => []
      t.timestamps
    end
  end
end
