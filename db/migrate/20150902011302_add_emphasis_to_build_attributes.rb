class AddEmphasisToBuildAttributes < ActiveRecord::Migration
  def change
    add_column :build_attributes, :emphasis, :string, :array => true, :default => []
  end
end
