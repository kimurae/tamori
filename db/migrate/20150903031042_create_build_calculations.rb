class CreateBuildCalculations < ActiveRecord::Migration
  def change
    create_table :build_calculations do |t|
      t.uuid :system_id
      t.uuid :trait_id
      t.integer :argument
      t.integer :calculator
      t.integer :priority
      t.uuid :trait_ids, :array => true, :default => []
      t.timestamps
    end
  end
end
