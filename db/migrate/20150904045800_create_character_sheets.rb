class CreateCharacterSheets < ActiveRecord::Migration
  def change
    
    create_table :character_sheets do |t|
      t.integer :character_id
      t.jsonb :data
    end
  end
end
