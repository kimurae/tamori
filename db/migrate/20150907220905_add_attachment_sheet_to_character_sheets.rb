class AddAttachmentSheetToCharacterSheets < ActiveRecord::Migration
  def self.up
    change_table :character_sheets do |t|
      t.attachment :sheet
    end
  end

  def self.down
    remove_attachment :character_sheets, :sheet
  end
end
