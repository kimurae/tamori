class CreateItems < ActiveRecord::Migration
  def change
    create_table :build_items do |t|
      t.integer :build_id
      t.string :comment
      t.integer :cost
      t.string :cost_type
      t.string :location
      t.string :name
      t.string :notes, :array => true, :default => []
      t.integer :qty
      t.timestamps
    end
  end
end
