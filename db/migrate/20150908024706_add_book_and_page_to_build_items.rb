class AddBookAndPageToBuildItems < ActiveRecord::Migration
  def change
    add_column :build_items, :book, :string
    add_column :build_items, :page, :string
  end
end
