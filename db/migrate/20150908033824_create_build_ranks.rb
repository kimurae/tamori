class CreateBuildRanks < ActiveRecord::Migration
  def change
    create_table :build_ranks do |t|
      t.integer :build_id
      t.uuid :trait_id
      t.string :category
      t.string :comment
      t.integer :cost
      t.string :cost_type
      t.decimal :value, :precision => 3, :scale => 1
      t.timestamps
    end

    add_index :build_aspects,    :build_id
    add_index :build_attributes, :build_id
    add_index :build_items,      :build_id
    add_index :build_properties, :build_id
    add_index :build_ranks,      :build_id

    add_index :build_calculations, :system_id
    add_index :traits,             :system_id
    add_index :details,            :trait_id

  end
end
