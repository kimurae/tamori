class ChangeArgumentColumnForCalculator < ActiveRecord::Migration
  def change
    change_column :build_calculations, :argument, :string
  end
end
