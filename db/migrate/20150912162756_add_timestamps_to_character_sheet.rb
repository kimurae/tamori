class AddTimestampsToCharacterSheet < ActiveRecord::Migration
  def change

    change_table :character_sheets do |t|
      t.timestamps
    end
  end
end
