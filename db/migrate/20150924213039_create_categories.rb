class CreateCategories < ActiveRecord::Migration
  def up
    create_table :categories, :id => :uuid, :default => 'uuid_generate_v1()' do |t|
      t.uuid   :system_id
      t.string :name
      t.string :label
      t.timestamps
    end

    add_column :traits, :category_id, :uuid

    conn = connection.raw_connection
    conn.prepare('insert_categories', 'INSERT INTO categories (system_id, name, label) VALUES ($1, $2, $3) returning id')
    conn.prepare('update_traits', 'UPDATE traits SET category_id = $1 WHERE category = $2 AND system_id = $3')

    conn.exec('SELECT DISTINCT system_id, category FROM traits;').each do |row|
      category  = row['category']
      system_id = row['system_id']
      if row['category']
        category_id = conn.exec_prepared('insert_categories',[ system_id, category.pluralize.parameterize('_'), category] ).first['id']
        conn.exec_prepared('update_traits', [category_id, category, system_id])
      end
    end

  end

  def down

    remove_column :traits, :category_id

    drop_table :categories
  end

end
