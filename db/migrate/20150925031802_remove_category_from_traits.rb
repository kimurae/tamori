class RemoveCategoryFromTraits < ActiveRecord::Migration
  def up
    remove_column :traits, :category
  end

  def down
    add_column :traits, :category, :string

    connection.execute <<SQL

UPDATE traits
   SET category = categories.label
  FROM categories
 WHERE categories.id = category_id
SQL
  end
end
