class AddRendererToSystems < ActiveRecord::Migration
  def change
    add_column :systems, :renderer, :integer
  end
end
