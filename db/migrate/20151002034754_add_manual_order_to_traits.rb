class AddManualOrderToTraits < ActiveRecord::Migration
  def change
    add_column :traits, :manual_order, :integer
  end
end
