class AddDescriptionToBuildItems < ActiveRecord::Migration
  def change
    add_column :build_items, :description, :text
  end
end
