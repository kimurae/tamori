class AddCategoryIdToBuildItems < ActiveRecord::Migration
  def change
    add_column :build_items, :category_id, :uuid
  end
end
