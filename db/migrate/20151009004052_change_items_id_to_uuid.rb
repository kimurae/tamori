class ChangeItemsIdToUuid < ActiveRecord::Migration
  def change
    add_column :build_items, :item_id, :uuid, :default => "uuid_generate_v1()"
    add_index  :build_items, :item_id
  end
end
