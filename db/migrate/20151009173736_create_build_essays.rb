class CreateBuildEssays < ActiveRecord::Migration
  def change
    create_table :build_essays do |t|
      t.integer :build_id
      t.string :comment
      t.text :value
      t.uuid :trait_id

      t.timestamps
    end

    add_index :build_essays, :build_id
    add_index :build_essays, :trait_id

  end
end
