class ShiftCategoriesToAspects < ActiveRecord::Migration
  def change
    Category.where(name: %w(ancestors advantages demeanors disadvantages natures school_affinities school_deficiencies skill_masteries)).update_all(name: 'aspects')
  end
end
