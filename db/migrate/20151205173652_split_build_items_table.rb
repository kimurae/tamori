class SplitBuildItemsTable < ActiveRecord::Migration
  def up
    rename_table :build_items, :user_items

    create_table :build_items do |t|
      t.integer  :build_id
      t.uuid     :user_item_id
      t.string   :comment
      t.integer  :cost
      t.string   :cost_type
      t.string   :location
      t.string   :notes, :default => [], :array => true
      t.integer  :qty
      t.timestamps
    end

    add_column :user_items, :user_id, :integer

    connection.execute <<SQL

      INSERT INTO build_items (build_id, user_item_id, comment, cost, cost_type, location, notes, qty, created_at, updated_at)
      SELECT build_id, item_id, comment, cost, cost_type, location, notes, qty, created_at, updated_at
        FROM user_items;
SQL

    connection.execute <<SQL

      UPDATE user_items SET user_id = c.user_id
       FROM user_items ui
 INNER JOIN builds b     ON b.id = ui.build_id
 INNER JOIN characters c ON c.id = b.character_id;
SQL

    remove_column :user_items, :build_id
    remove_column :user_items, :comment
    remove_column :user_items, :cost
    remove_column :user_items, :cost_type
    remove_column :user_items, :location
    remove_column :user_items, :notes
    remove_column :user_items, :qty
  end

  def down

    add_column :user_items, :build_id,  :integer
    add_column :user_items, :comment,   :string
    add_column :user_items, :cost,      :integer
    add_column :user_items, :cost_type, :string
    add_column :user_items, :location,  :string
    add_column :user_items, :notes,     :string, :default => [], :array => true
    add_column :user_items, :qty,       :integer

    remove_column :user_items, :user_id

    connection.execute <<SQL

      UPDATE  user_items
         SET  build_id  = bi.build_id,
              comment   = bi.comment,
              cost      = bi.cost,
              cost_type = bi.cost_type,
              location  = bi.location,
              notes     = bi.notes,
              qty       = bi.qty
        FROM  user_items ui
  INNER JOIN  build_items bi ON ui.item_id = bi.user_item_id;
SQL

    drop_table :build_items
    rename_table :user_items, :build_items

  end

end
