class ChangePrimaryKeyToItemId < ActiveRecord::Migration

  def up
    remove_column :user_items, :id
    rename_column :user_items, :item_id, :id
    execute 'ALTER TABLE user_items ADD PRIMARY KEY (id);'
  end

  def down
    execute 'ALTER TABLE user_items DROP CONSTRAINT user_items_pkey'
    rename_column :user_items, :id, :item_id
    add_column :user_items, :id, :primary_key
  end

end
