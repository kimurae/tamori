class AddDataToCharacters < ActiveRecord::Migration
  def change
    add_column :characters, :data, :jsonb
  end
end
