class AddKeysToBuildCalculations < ActiveRecord::Migration[5.0]
  def change
    add_column :build_calculations, :input_key,   :string, default: 'value', null: false
    add_column :build_calculations, :output_key,  :string, default: 'value', null: false
  end
end
