class CreateBuildComponent < ActiveRecord::Migration[5.0]
  def up

    create_table :build_components do |t|
      t.integer :build_id, index: true
      t.uuid    :trait_id
      t.string  :type,      index: true
      t.string  :comment
      t.integer :cost
      t.string  :cost_type
      t.string  :notes,     default: [], array: true
      t.text  :value
      t.datetime :created_at
      t.datetime :updated_at
    end

    execute <<-SQL
      INSERT INTO build_components (build_id, trait_id, type, comment, cost, cost_type, notes, value, created_at, updated_at)
      SELECT build_id, trait_id, 'BuildAspect' AS type, comment, cost, cost_type, notes, 't' AS value, created_at, updated_at FROM build_aspects
      UNION SELECT  build_id, trait_id, 'BuildAttribute' AS type, comment, cost, cost_type, emphasis AS notes, value::text, created_at, updated_at FROM build_attributes
      UNION SELECT  build_id, trait_id, 'BuildEssay' AS type, comment, NULL AS cost, NULL AS cost_type, '{}' AS notes, value, created_at, updated_at FROM build_essays
      UNION SELECT  build_id, trait_id, 'BuildProperty' AS type, comment, cost, cost_type, '{}' AS notes, value::text, created_at, updated_at FROM build_properties
      UNION SELECT  build_id, trait_id, 'BuildRank' AS type, comment, cost, cost_type, '{}' AS notes, value::text, created_at, updated_at FROM build_ranks
    SQL

    remove_index :buld_aspects,  :name => 'index_build_aspects_on_build_id'

    remove_index :build_attributes, :name => 'index_build_attributes_on_build_id'

    remove_index :build_essays, :name => 'index_build_essays_on_build_id'
    remove_index :build_essays, :name => 'index_build_essays_on_trait_id'

    remove_index :build_properties, :name => 'index_build_properties_on_build_id'

    remove_index :build_ranks, :name => 'index_build_ranks_on_build_id'

    drop_table :build_aspects
    drop_table :build_attributes
    drop_table :build_essays
    drop_table :build_properties
    drop_table :build_ranks
  end

  def down
    create_table "build_aspects", force: :cascade do |t|
      t.integer  "build_id"
      t.uuid     "trait_id"
      t.boolean  "active"
      t.string   "comment"
      t.integer  "cost"
      t.string   "cost_type"
      t.string   "notes",                 default: [], array: true
      t.datetime "created_at"
      t.datetime "updated_at"
      t.index ["build_id"], name: "index_build_aspects_on_build_id", using: :btree
    end

    create_table "build_attributes", force: :cascade do |t|
      t.integer  "build_id"
      t.string   "category"
      t.integer  "cost"
      t.string   "cost_type"
      t.uuid     "trait_id"
      t.integer  "value"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "comment"
      t.string   "emphasis",              default: [], array: true
      t.index ["build_id"], name: "index_build_attributes_on_build_id", using: :btree
    end

    create_table "build_essays", force: :cascade do |t|
      t.integer  "build_id"
      t.string   "comment"
      t.text     "value"
      t.uuid     "trait_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.index ["build_id"], name: "index_build_essays_on_build_id", using: :btree
      t.index ["trait_id"], name: "index_build_essays_on_trait_id", using: :btree
    end

    create_table "build_properties", force: :cascade do |t|
      t.integer  "build_id"
      t.string   "category"
      t.integer  "cost"
      t.string   "cost_type"
      t.uuid     "trait_id"
      t.string   "value"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "comment"
      t.index ["build_id"], name: "index_build_properties_on_build_id", using: :btree
    end

    create_table "build_ranks", force: :cascade do |t|
      t.integer  "build_id"
      t.uuid     "trait_id"
      t.string   "category"
      t.string   "comment"
      t.integer  "cost"
      t.string   "cost_type"
      t.decimal  "value",                 precision: 3, scale: 1
      t.datetime "created_at"
      t.datetime "updated_at"
      t.index ["build_id"], name: "index_build_ranks_on_build_id", using: :btree
    end

    execute <<-SQL
      INSERT INTO build_aspects
      SELECT build_id, trait_id, value::boolean AS active, comment, cost, cost_type, notes, created_at, updated_at FROM build_components WHERE type = 'BuildAspect';

      INSERT INTO build_attributes
      SELECT  build_id, trait_id, NULL AS category, cost, cost_type, value::integer, created_at, updated_at, comment, notes AS emphasis FROM build_components WHERE type = 'BuildAttribute';

      INSERT INTO build_essays
      SELECT  build_id, comment, value, trait_id, created_at, updated_at FROM build_components WHERE type = 'BuildEssay';

      INSERT INTO build_properties
      SELECT  build_id, NULL AS category, cost, cost_type, trait_id, value::character varying, created_at, updated_at, comment FROM build_components WHERE type = 'BuildProperty';

      INSERT INTO build_ranks
      UNION SELECT  build_id, trait_id, NULL AS category, comment, cost, cost_type, value::numeric(3,1), created_at, updated_at FROM build_components WHERE type = 'BuildRank';
    SQL

    drop_table :build_components
  end
end
