class UpdateCostReportVwToVersion2 < ActiveRecord::Migration[5.0]
  def change
    enable_extension 'tablefunc'
    update_view :cost_report_vw, version: 2, revert_to_version: 1
  end
end
