class CreatePortraits < ActiveRecord::Migration[5.0]
  def change
    add_attachment :characters, :portrait
  end
end
