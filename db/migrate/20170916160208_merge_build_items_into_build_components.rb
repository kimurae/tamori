class MergeBuildItemsIntoBuildComponents < ActiveRecord::Migration[5.0]
  def up
    add_column :build_components, :trait_type, :string, null: false, default: 'Trait', index: true
    add_column :build_components, :location, :string

    execute <<-SQL
      INSERT INTO build_components (build_id, comment, cost, cost_type, location, notes, trait_id, trait_type, type, value, created_at, updated_at)
      SELECT build_id, comment, cost, cost_type, location, notes, user_item_id, 'UserItem', 'BuildItem', qty, created_at, CURRENT_TIMESTAMP
       FROM build_items
    SQL

    drop_table :build_items
  end

  def down

    create_table "build_items" do |t|
      t.integer  :build_id
      t.uuid     :user_item_id
      t.string   :comment
      t.integer  :cost
      t.string   :cost_type
      t.string   :location
      t.string   :notes,        default: [], array: true
      t.integer  :qty
      t.timestamps
    end

    execute <<-SQL
      INSERT INTO build_items (build_id, comment, cost, cost_type, location, notes, qty, user_item_id, created_at, updated_at)
      SELECT build_id, comment, cost, cost_type, location, notes, value::integer, trait_id, created_at, CURRENT_TIMESTAMP
      FROM build_components WHERE type = 'BuildItem'
    SQL

    execute %q{DELETE FROM build_components WHERE type = 'BuildItem'}

    remove_column :build_components, :location
    remove_column :build_components, :trait_type
  end
end
