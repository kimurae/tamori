class AddValueTypeToBuildComponents < ActiveRecord::Migration[5.0]
  def up
    add_column :build_components, :value_type, :string, default: 'text'

    execute %q{UPDATE build_components SET value_type = 'boolean' WHERE type = 'BuildAspect'}
    execute %q{UPDATE build_components SET value_type = 'integer' WHERE type IN ('BuildAttribute', 'BuildItem')}
    execute %q{UPDATE build_components SET value_type = 'numeric(3,2)' WHERE type = 'BuildRank'}
  end

  def down
    remove_column :build_components, :value_type
  end
end
