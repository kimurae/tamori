class AddTypeToBuilds < ActiveRecord::Migration[5.0]
  def change
    add_column :builds, :type, :string
  end
end
