class AddFiltersToBuildCalculations < ActiveRecord::Migration[5.0]
  def change
    change_table(:build_calculations) do |t|
      t.string :filter_key,       null: false, default: 'active'
      t.string :filter_argument,  null: false, default: 'true'
    end
  end
end
