class AddGames < ActiveRecord::Migration[5.0]
  def change
    add_column :characters, :status, :integer, default: 0, null: false
    add_index  :characters, :status, where: 'status = 0'
  end
end
