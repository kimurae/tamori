class CreateCharactersBuilds < ActiveRecord::Migration[5.2]
  def change
    create_table :characters_builds do |t|
      t.uuid :build_id
      t.uuid :character_id
    end
  end
end
