class AddUuidsToBuilds < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        execute 'ALTER TABLE builds DROP CONSTRAINT builds_pkey'
      end

      dir.down do
        execute 'ALTER TABLE builds ADD PRIMARY KEY(id)'
      end
    end

    rename_column :builds, :id, :old_id
    add_column    :builds, :id, :uuid, default: 'uuid_generate_v1()', null: false

    rename_column :build_components, :build_id, :old_build_id
    add_column    :build_components, :build_id, :uuid, index: true

    reversible do |dir|
      dir.up do
        execute 'ALTER TABLE builds ADD PRIMARY KEY(id)'

        execute 'UPDATE build_components SET build_id = builds.id FROM builds WHERE builds.old_id = build_components.old_build_id'

        change_column :builds, :old_id, :integer, default: nil, null: true
      end

      dir.down do
        execute 'ALTER TABLE builds DROP CONSTRAINT builds_pkey'
      end
    end
  end
end
