class UpdateCostReportVwToVersion3 < ActiveRecord::Migration[5.2]
  def change
    update_view :cost_report_vw, version: 3, revert_to_version: 2
  end
end
