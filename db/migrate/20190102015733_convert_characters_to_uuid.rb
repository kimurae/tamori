class ConvertCharactersToUuid < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        execute 'ALTER TABLE characters DROP CONSTRAINT characters_pkey'
        execute 'ALTER TABLE character_sheets DROP CONSTRAINT character_sheets_pkey'
        execute 'ALTER TABLE active_storage_attachments DROP CONSTRAINT active_storage_attachments_pkey'
      end

      dir.down do
        execute 'ALTER TABLE characters ADD PRIMARY KEY(id)'
        execute 'ALTER TABLE character_sheets ADD PRIMARY KEY(id)'
        execute 'ALTER TABLE active_storage_attachments ADD PRIMARY KEY(id)'
      end
    end

    rename_column :characters, :id, :old_id
    add_column    :characters, :id, :uuid, default: 'uuid_generate_v1()', null: false

    rename_column :character_sheets, :id, :old_id
    add_column    :character_sheets, :id, :uuid, default: 'uuid_generate_v1()', null: false

    rename_column :active_storage_attachments, :id, :old_id
    add_column    :active_storage_attachments, :id, :uuid, default: 'uuid_generate_v1()', null: false

    rename_column :builds,            :character_id, :old_character_id
    rename_column :character_sheets,  :character_id, :old_character_id
    rename_column :summaries,         :character_id, :old_character_id

    add_column    :builds,            :character_id, :uuid, index: true
    add_column    :character_sheets,  :character_id, :uuid, index: true
    add_column    :summaries,         :character_id, :uuid, index: true

    rename_column :active_storage_attachments, :record_id, :old_record_id
    add_column    :active_storage_attachments, :record_id, :uuid, index: true

    reversible do |dir|
      dir.up do
        execute 'ALTER TABLE characters ADD PRIMARY KEY(id)'
        execute 'ALTER TABLE character_sheets ADD PRIMARY KEY(id)'
        execute 'ALTER TABLE active_storage_attachments ADD PRIMARY KEY(id)'

        %w(builds character_sheets summaries).each do |i|
          execute "UPDATE #{i} SET character_id = characters.id FROM characters WHERE characters.old_id = #{i}.old_character_id"
        end

        execute %q{UPDATE active_storage_attachments SET record_id = characters.id FROM characters WHERE characters.old_id = active_storage_attachments.old_record_id AND record_type = 'Character'}
        execute %q{UPDATE active_storage_attachments SET record_id = character_sheets.id FROM character_sheets WHERE character_sheets.old_id = active_storage_attachments.old_record_id AND record_type = 'CharacterSheet'}
        execute %q{UPDATE active_storage_attachments SET record_id = subquery.id   FROM (SELECT id, old_id FROM active_storage_attachments) subquery WHERE subquery.old_id = active_storage_attachments.old_record_id AND record_type = 'ActiveStorage::Blob'}

        change_column :characters, :old_id, :integer, default: nil, null: true
        change_column :character_sheets, :old_id, :integer, default: nil, null: true
        change_column :active_storage_attachments, :old_id, :integer, default: nil, null: true
        change_column :active_storage_attachments, :old_record_id, :integer, default: nil, null: true

        drop_table :logs
      end

      dir.down do
        execute 'ALTER TABLE characters DROP CONSTRAINT characters_pkey'
        execute 'ALTER TABLE character_sheets DROP CONSTRAINT character_sheets_pkey'
        execute 'ALTER TABLE active_storage_attachments DROP CONSTRAINT active_storage_attachments_pkey'
      end
    end
  end
end
