class SetupCharactersBuilds < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        execute 'INSERT INTO characters_builds (character_id, build_id) SELECT character_id, id FROM builds'
      end
    end 
  end
end
