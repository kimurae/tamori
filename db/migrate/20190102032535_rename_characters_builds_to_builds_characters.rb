class RenameCharactersBuildsToBuildsCharacters < ActiveRecord::Migration[5.2]
  def change
    rename_table :characters_builds, :builds_characters
  end
end
