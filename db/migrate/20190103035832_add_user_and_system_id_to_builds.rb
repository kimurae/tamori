class AddUserAndSystemIdToBuilds < ActiveRecord::Migration[5.2]
  def change
    add_column :builds, :system_id, :uuid, index: true
    add_column :builds, :user_id, :integer, index: true

    reversible do |dir|
      dir.up do
        execute <<~SQL
          UPDATE builds SET system_id = characters.system_id, user_id = characters.user_id
            FROM characters
           WHERE builds.character_id = characters.id
        SQL
      end
    end
  end
end
