# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_03_035832) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "hstore"
  enable_extension "plpgsql"
  enable_extension "tablefunc"
  enable_extension "uuid-ossp"

  create_table "active_storage_attachments", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.integer "old_id"
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "old_record_id"
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.uuid "record_id"
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "old_record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "build_calculations", id: :serial, force: :cascade do |t|
    t.uuid "system_id"
    t.uuid "trait_id"
    t.string "argument"
    t.integer "calculator"
    t.integer "priority"
    t.uuid "trait_ids", default: [], array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "input_key", default: "value", null: false
    t.string "output_key", default: "value", null: false
    t.string "filter_key", default: "active", null: false
    t.string "filter_argument", default: "true", null: false
    t.index ["system_id"], name: "index_build_calculations_on_system_id"
  end

  create_table "build_components", id: :serial, force: :cascade do |t|
    t.integer "old_build_id"
    t.uuid "trait_id"
    t.string "type"
    t.string "comment"
    t.integer "cost"
    t.string "cost_type"
    t.string "notes", default: [], array: true
    t.text "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "trait_type", default: "Trait", null: false
    t.string "location"
    t.string "value_type", default: "text"
    t.uuid "build_id"
    t.index ["old_build_id"], name: "index_build_components_on_old_build_id"
    t.index ["type"], name: "index_build_components_on_type"
  end

  create_table "build_ranked_attributes", id: :serial, force: :cascade do |t|
    t.integer "build_id"
    t.string "category"
    t.integer "cost"
    t.string "cost_type"
    t.string "name"
    t.uuid "trait_id"
    t.decimal "value", precision: 1
  end

  create_table "builds", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.integer "old_id"
    t.integer "old_character_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "name"
    t.date "built_on"
    t.string "type"
    t.uuid "character_id"
    t.uuid "system_id"
    t.integer "user_id"
    t.index ["old_character_id"], name: "index_builds_on_old_character_id"
  end

  create_table "builds_characters", force: :cascade do |t|
    t.uuid "build_id"
    t.uuid "character_id"
  end

  create_table "categories", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.uuid "system_id"
    t.string "name"
    t.string "label"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "character_sheets", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.integer "old_id"
    t.integer "old_character_id"
    t.jsonb "data"
    t.string "sheet_file_name"
    t.string "sheet_content_type"
    t.integer "sheet_file_size"
    t.datetime "sheet_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid "character_id"
  end

  create_table "characters", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.integer "old_id"
    t.integer "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "user_id"
    t.uuid "system_id"
    t.string "name"
    t.jsonb "data"
    t.string "portrait_file_name"
    t.string "portrait_content_type"
    t.integer "portrait_file_size"
    t.datetime "portrait_updated_at"
    t.integer "status", default: 0, null: false
    t.index ["game_id"], name: "index_characters_on_game_id"
    t.index ["status"], name: "index_characters_on_status", where: "(status = 0)"
    t.index ["user_id", "old_id"], name: "index_characters_on_user_id_and_old_id"
    t.index ["user_id"], name: "index_characters_on_user_id"
  end

  create_table "details", id: :serial, force: :cascade do |t|
    t.uuid "trait_id"
    t.string "book"
    t.string "page"
    t.text "description"
    t.string "keywords", default: [], array: true
    t.hstore "properties", default: {}
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "raises", default: [], array: true
    t.index ["trait_id"], name: "index_details_on_trait_id"
  end

  create_table "summaries", id: :serial, force: :cascade do |t|
    t.integer "old_character_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid "character_id"
    t.index ["old_character_id"], name: "index_summaries_on_old_character_id"
  end

  create_table "systems", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "renderer"
  end

  create_table "template_components", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.uuid "build_template_id"
    t.uuid "trait_id"
    t.index ["build_template_id"], name: "index_template_components_on_build_template_id"
  end

  create_table "traits", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "label"
    t.uuid "system_id"
    t.uuid "category_id"
    t.integer "manual_order"
    t.index ["system_id"], name: "index_traits_on_system_id"
  end

  create_table "user_items", id: :uuid, default: -> { "uuid_generate_v1()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "book"
    t.string "page"
    t.hstore "properties", default: {}
    t.text "description"
    t.uuid "category_id"
    t.integer "user_id"
    t.integer "old_character_id"
    t.uuid "character_id"
    t.index ["id"], name: "index_user_items_on_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "name"
    t.boolean "admin"
  end


  create_view "cost_report_vw",  sql_definition: <<-SQL
      SELECT traits.label,
      ct.build_id,
      ct.trait_id,
      ct.xp,
      ct.fp,
      ct.dots
     FROM (crosstab('
    SELECT       build_id, trait_id, cost_type, cost FROM build_components WHERE cost IS NOT NULL
    ORDER BY 2,3'::text, 'VALUES (''XP''), (''FP''), (''Dots'')'::text) ct(build_id uuid, trait_id uuid, xp integer, fp integer, dots integer)
       JOIN traits ON ((traits.id = ct.trait_id)))
    ORDER BY traits.label;
  SQL

end
