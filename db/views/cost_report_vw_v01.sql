SELECT traits.label, ct.*
FROM crosstab( '
  SELECT       build_id, trait_id, cost_type, cost FROM build_attributes WHERE cost IS NOT NULL
  UNION SELECT build_id, trait_id, cost_type, cost FROM build_aspects    WHERE cost IS NOT NULL
  UNION SELECT build_id, trait_id, cost_type, cost FROM build_properties WHERE cost IS NOT NULL
  UNION SELECT build_id, trait_id, cost_type, cost FROM build_ranks      WHERE cost IS NOT NULL
  ORDER BY 2,3',
$$VALUES ('XP'), ('FP'), ('Dots')$$) AS ct ("build_id" int, "trait_id" uuid, "xp" int, "fp" int, "dots" int)
INNER JOIN traits ON traits.id = ct.trait_id
ORDER BY traits.label
