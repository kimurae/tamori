require 'rake'

namespace :db do
  namespace :functions do

    desc 'load functions into the database'
    task :load => :environment do
      Dir.glob(File.join(Rails.root, 'db', 'functions', '*.sql')).each do |sql_file|
        begin
          ActiveRecord::Base.connection.execute(IO.read(sql_file))
        rescue ActiveRecord::StatementInvalid => err
          warn "#{sql_file} #{err.message}"
        end
      end
    end
  end
end
