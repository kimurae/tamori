Aws.config.update(
  region: 'us-east-1',
  credentials: Aws::Credentials.new(Rails.application.secrets.aws_access_key_id, Rails.application.secrets.aws_secret_access_key)
)

s3 = Aws::S3::Client.new({
  region: 'us-east-1',
  access_key_id: Rails.application.secrets.aws_access_key_id,
  secret_access_key: Rails.application.secrets.aws_secret_access_key
})

CharacterSheet.alias_method :new_sheet, :sheet
CharacterSheet.has_attached_file(:sheet, :styles => {})

CharacterSheet.includes(:character).all.select do |cs|
  cs.sheet.file?
end.each do |cs|
  cs.new_sheet.attach(
    io: s3.get_object(bucket: 'tamori', key: cs.sheet.path).body,
    filename: "#{cs.character.name}.pdf"
  )
end

Character.alias_method :new_portrait, :portrait
Character.has_attached_file(:portrait, :styles => { :medium => '300x300' }, :default_url => '/images/:style/missing.png')

Character.all.select do |c|
  c.portrait.file?
end.each do |c|
  c.new_portrait.attach(
    io: s3.get_object(bucket: 'tamori', key: c.portrait.path).body,
    filename: c.portrait.original_filename
  )
end
