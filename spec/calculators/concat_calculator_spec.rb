require 'rails_helper'

RSpec.describe ConcatCalculator do

  describe '#to_s' do
    specify { expect(ConcatCalculator.to_s).to eq 'ConcatCalculator' }

    specify { expect(ConcatCalculator.to_s(%w(trait1 trait2 trait3))).to      eq '#{trait1} #{trait2} #{trait3}' }
    specify { expect(ConcatCalculator.to_s(%w(trait1 trait2 trait3), '/')).to eq '#{trait1}/#{trait2}/#{trait3}' }
  end

  describe '#calculate' do

    specify { expect(ConcatCalculator.calculate([])).to eq '' }
    specify { expect(ConcatCalculator.calculate(%w(test1 test2))).to eq 'test1 test2' }
    specify { expect(ConcatCalculator.calculate(%w(test1 test2), '/')).to eq 'test1/test2' }
  end

end
