require 'rails_helper'

RSpec.describe L5r::FullDefenseCalculator do

  describe '#to_s' do
    specify { expect(L5r::FullDefenseCalculator.to_s).to eq 'L5r::FullDefenseCalculator' }

    specify { expect(L5r::FullDefenseCalculator.to_s(%w(trait1 trait2 trait3))).to    eq 'L5r::FullDefense(#{trait1},#{trait2},#{trait3})' }
    specify { expect(L5r::FullDefenseCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq 'L5r::FullDefense(#{trait1},#{trait2},#{trait3})' }
  end

  describe '#calculate' do

    specify { expect(L5r::FullDefenseCalculator.calculate([2,1, nil, 2])).to eq '2 + 1/2 3k2' }
    specify { expect(L5r::FullDefenseCalculator.calculate([])).to eq ' + 1/2 0k' }
    specify { expect(L5r::FullDefenseCalculator.calculate(%w(test1 test2))).to eq 'test1 + 1/2 0k' }
    specify { expect(L5r::FullDefenseCalculator.calculate(%w(test1 test2), '/')).to  eq 'test1 + 1/2 0k' }
    specify { expect(L5r::FullDefenseCalculator.calculate([3,4], 2)).to eq '3 + 1/2 4k' }
    specify { expect(L5r::FullDefenseCalculator.calculate([3,4], '/')).to eq '3 + 1/2 4k' }
  end

end
