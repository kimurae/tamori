require 'rails_helper'

RSpec.describe L5r::InsightRankCalculator do

  describe '#to_s' do
    specify { expect(L5r::InsightRankCalculator.to_s).to eq 'L5r::InsightRankCalculator' }

    specify { expect(L5r::InsightRankCalculator.to_s(%w(trait1 trait2 trait3))).to    eq 'L5r::InsightRank(#{trait1},#{trait2},#{trait3})' }
    specify { expect(L5r::InsightRankCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq 'L5r::InsightRank(#{trait1},#{trait2},#{trait3})' }
  end

  describe '#calculate' do

    specify { expect(L5r::InsightRankCalculator.calculate([])).to eq 1 }
    specify { expect(L5r::InsightRankCalculator.calculate([149])).to eq 1 }
    specify { expect(L5r::InsightRankCalculator.calculate([150])).to eq 2 }
    specify { expect(L5r::InsightRankCalculator.calculate([175])).to eq 3 }
    specify { expect(L5r::InsightRankCalculator.calculate([200])).to eq 4 }
    specify { expect(L5r::InsightRankCalculator.calculate([225])).to eq 5 }
    specify { expect(L5r::InsightRankCalculator.calculate([250])).to eq 6 }
    specify { expect(L5r::InsightRankCalculator.calculate([275])).to eq 7 }
    specify { expect(L5r::InsightRankCalculator.calculate([300])).to eq 8 }
    specify { expect(L5r::InsightRankCalculator.calculate([325])).to eq 9 }
    specify { expect{ L5r::InsightRankCalculator.calculate(%w(test1 test2)) }.to raise_error(NoMethodError) }
    specify { expect{ L5r::InsightRankCalculator.calculate(%w(test1 test2), '/') }.to raise_error(NoMethodError) }
    specify { expect{ L5r::InsightRankCalculator.calculate([3,'test'], '/') }.to raise_exception( TypeError, %q{String can't be coerced into Integer}) }
  end

end
