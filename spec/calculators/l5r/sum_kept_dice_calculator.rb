require 'rails_helper'

RSpec.describe L5r::SumDiceCalculator do

  describe '#to_s' do
    specify { expect(L5r::SumDiceCalculator.to_s).to eq 'L5r::SumDiceCalculator' }

    specify { expect(L5r::SumDiceCalculator.to_s(%w(trait1 trait2 trait3))).to    eq 'L5r::SumDice(#{trait1},#{trait2},#{trait3})' }
    specify { expect(L5r::SumDiceCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq 'L5r::SumDice(#{trait1},#{trait2},#{trait3})' }
  end

  describe '#calculate' do
    specify { expect(L5r::SumDiceCalculator.calculate([])).to eq '0k0' }

    specify { expect(L5r::SumDiceCalculator.calculate(%w(1k1 2k0 3k2 0k1))).to eq '6k4' }

    specify { expect( L5r::SumDiceCalculator.calculate(%w(test1 test2)) ).to      eq '0k0' }
    specify { expect( L5r::SumDiceCalculator.calculate(%w(test1 test2), '/') ).to eq '0k0' }
    specify { expect( L5r::SumDiceCalculator.calculate([3,'test'], '/') ).to      eq '0k0' }
  end

end
