require 'rails_helper'

RSpec.describe MinCalculator do

  describe '#to_s' do
    specify { expect(MinCalculator.to_s).to eq 'MinCalculator' }

    specify { expect(MinCalculator.to_s(%w(trait1 trait2 trait3))).to      eq 'Min(#{trait1},#{trait2},#{trait3})' }
    specify { expect(MinCalculator.to_s(%w(trait1 trait2 trait3), '/')).to eq 'Min(#{trait1},#{trait2},#{trait3})' }
  end

  describe '#calculate' do

    specify { expect(MinCalculator.calculate([])).to eq 0 }
    specify { expect(MinCalculator.calculate(%w(test1 test2))).to eq 'test1' }
    specify { expect(MinCalculator.calculate(%w(test1 test2), '/')).to eq 'test1' }
    specify { expect(MinCalculator.calculate([2,1])).to eq 1 }
    specify { expect(MinCalculator.calculate([3,4], '/')).to eq 3 }
    specify { expect{ MinCalculator.calculate([3,'test'], '/') }.to raise_exception( ArgumentError, 'comparison of String with 3 failed') }
  end

end
