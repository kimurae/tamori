require 'rails_helper'

RSpec.describe MultiplySumCalculator do

  describe '#to_s' do
    specify { expect(MultiplySumCalculator.to_s).to eq 'MultiplySumCalculator' }

    specify { expect(MultiplySumCalculator.to_s(%w(trait1 trait2 trait3))).to      eq '1( #{trait1} + #{trait2} + #{trait3} )' }
    specify { expect(MultiplySumCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq '3( #{trait1} + #{trait2} + #{trait3} )' }
  end

  describe '#calculate' do

    specify { expect(MultiplySumCalculator.calculate([])).to eq 0 }
    specify { expect(MultiplySumCalculator.calculate(%w(test1 test2))).to eq 'test1test2' }
    specify { expect(MultiplySumCalculator.calculate(%w(test1 test2), '/')).to eq '' }
    specify { expect(MultiplySumCalculator.calculate([2,1])).to eq 3 }
    specify { expect(MultiplySumCalculator.calculate([3,4], 2)).to eq 14 }
    specify { expect(MultiplySumCalculator.calculate([3,4], '/')).to eq 0 }
    specify { expect{ MultiplySumCalculator.calculate([3,'test'], '/') }.to raise_exception( TypeError, %q{String can't be coerced into Integer}) }
  end

end
