require 'rails_helper'

RSpec.describe SetBooleanValueCalculator do

  describe '#to_s' do
    specify { expect(SetBooleanValueCalculator.to_s).to eq 'false' }

    specify { expect(SetBooleanValueCalculator.to_s(%w(trait1 trait2 trait3))).to           eq 'false' }
    specify { expect(SetBooleanValueCalculator.to_s(%w(trait1 trait2 trait3), 'false')).to  eq 'false' }
    specify { expect(SetBooleanValueCalculator.to_s(%w(trait1 trait2 trait3), 'true')).to   eq 'true' }
  end

  describe '#calculate' do
    specify { expect(SetBooleanValueCalculator.calculate([])).to          eq false }
    specify { expect(SetBooleanValueCalculator.calculate([], 'true')).to  eq true }
    specify { expect(SetBooleanValueCalculator.calculate([], 'false')).to eq false }
  end

end
