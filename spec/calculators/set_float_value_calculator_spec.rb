require 'rails_helper'

RSpec.describe SetFloatValueCalculator do

  describe '#to_s' do
    specify { expect(SetFloatValueCalculator.to_s).to eq '0.0' }

    specify { expect(SetFloatValueCalculator.to_s(%w(trait1 trait2 trait3))).to    eq '0.0' }
    specify { expect(SetFloatValueCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq '3.0' }
  end

  describe '#calculate' do

    specify { expect(SetFloatValueCalculator.calculate([])).to eq 0.0 }
    specify { expect(SetFloatValueCalculator.calculate(%w(test1 test2))).to eq 0.0 }
    specify { expect(SetFloatValueCalculator.calculate(%w(test1 test2), '/')).to eq 0.0 }
    specify { expect(SetFloatValueCalculator.calculate([2,1])).to eq 0.0 }
    specify { expect(SetFloatValueCalculator.calculate([3,4], 2)).to eq 2.0 }
    specify { expect(SetFloatValueCalculator.calculate([3,4], '/')).to eq 0.0 }
    specify { expect(SetFloatValueCalculator.calculate([3,'test'], '/')).to eq 0.0 }
  end

end
