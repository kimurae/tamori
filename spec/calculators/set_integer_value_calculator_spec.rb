require 'rails_helper'

RSpec.describe SetIntegerValueCalculator do

  describe '#to_s' do
    specify { expect(SetIntegerValueCalculator.to_s).to eq '0' }

    specify { expect(SetIntegerValueCalculator.to_s(%w(trait1 trait2 trait3))).to    eq '0' }
    specify { expect(SetIntegerValueCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq '3' }
  end

  describe '#calculate' do

    specify { expect(SetIntegerValueCalculator.calculate([])).to eq 0 }
    specify { expect(SetIntegerValueCalculator.calculate(%w(test1 test2))).to eq 0 }
    specify { expect(SetIntegerValueCalculator.calculate(%w(test1 test2), '/')).to eq 0 }
    specify { expect(SetIntegerValueCalculator.calculate([2,1])).to eq 0 }
    specify { expect(SetIntegerValueCalculator.calculate([3,4], 2)).to eq 2 }
    specify { expect(SetIntegerValueCalculator.calculate([3,4], '/')).to eq 0 }
    specify { expect(SetIntegerValueCalculator.calculate([3,'test'], '/')).to eq 0 }
  end

end
