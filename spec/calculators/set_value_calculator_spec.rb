require 'rails_helper'

RSpec.describe SetValueCalculator do

  describe '#to_s' do
    specify { expect(SetValueCalculator.to_s).to eq '0' }

    specify { expect(SetValueCalculator.to_s(%w(trait1 trait2 trait3))).to    eq '0' }
    specify { expect(SetValueCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq '3' }
  end

  describe '#calculate' do

    specify { expect(SetValueCalculator.calculate([])).to eq '0' }
    specify { expect(SetValueCalculator.calculate(%w(test1 test2))).to eq '0' }
    specify { expect(SetValueCalculator.calculate(%w(test1 test2), '/')).to eq '/' }
    specify { expect(SetValueCalculator.calculate([2,1])).to eq '0' }
    specify { expect(SetValueCalculator.calculate([3,4], 2)).to eq '2' }
    specify { expect(SetValueCalculator.calculate([3,4], '/')).to eq '/' }
    specify { expect(SetValueCalculator.calculate([3,'test'], '/')).to eq '/' }
  end

end
