require 'rails_helper'

RSpec.describe SumCalculator do

  describe '#to_s' do
    specify { expect(SumCalculator.to_s).to eq 'SumCalculator' }

    specify { expect(SumCalculator.to_s(%w(trait1 trait2 trait3))).to    eq '#{trait1} + #{trait2} + #{trait3} + 0' }
    specify { expect(SumCalculator.to_s(%w(trait1 trait2 trait3), 3)).to eq '#{trait1} + #{trait2} + #{trait3} + 3' }
  end

  describe '#calculate' do

    specify { expect(SumCalculator.calculate([])).to eq 0 }
    specify { expect{ SumCalculator.calculate(%w(test1 test2)) }.to raise_exception( TypeError, %q{no implicit conversion of Integer into String}) }
    specify { expect{ SumCalculator.calculate(%w(test1 test2), '/') }.to raise_exception( TypeError, %q{no implicit conversion of Integer into String}) }
    specify { expect(SumCalculator.calculate([2,1])).to eq 3 }
    specify { expect(SumCalculator.calculate([3,4], 2)).to eq 9 }
    specify { expect(SumCalculator.calculate([3,4], '/')).to eq 7 }
    specify { expect{ SumCalculator.calculate([3,'test'], '/') }.to raise_exception( TypeError, %q{String can't be coerced into Integer}) }
  end

end
