FactoryGirl.define do
  factory(:build_calculation) do
    association(:trait)
    association(:system)
    argument 'k'
    priority 1
    trait_ids { [ create(:trait).id ] }
    calculator 'SumCalculator'

    trait :activation do
      argument    'true'
      calculator  'SetBooleanValueCalculator'
      output_key  'active'
      priority    0
      trait_ids   { Array.new }
    end
  end
end
