FactoryGirl.define do
  factory(:build_component) do
    association(:build)
    association(:trait)

    comment   'Test'
    cost      1
    cost_type 'XP'

    factory(:build_aspect, class: BuildAspect) do
      active true
      notes  { ['Something', 'Another thing'] }
    end

    factory(:build_attribute, class: BuildAttribute) do
      value 2
    end

    factory(:build_essay, class: BuildEssay) do
      value 'Very long essay'
    end

    factory(:build_item, class: BuildItem) do
      association(:trait, factory: :user_item)

      location 'H'
      notes    { ['Something', 'Another thing'] }
      value    3
    end

    factory(:build_property, class: BuildProperty) do
      value 'test'
    end

    factory(:build_rank, class: BuildRank) do
      value 2.2
    end
  end
end
