# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :build do
    association(:system)
    association(:user)

    sequence(:name) do |n|
      "Build #{n}"
    end

    built_on { Date.civil(2014,1,1) }
  end
end
