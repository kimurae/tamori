FactoryGirl.define do
  factory(:category) do
    association(:system)
    label 'Test'
    name  'tests'

    trait :attribute do
      label 'Attribute'
      name 'attributes'
    end

    trait :skill do
      label 'Skill'
      name 'skills'
    end

  end
end
