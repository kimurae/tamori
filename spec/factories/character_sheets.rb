FactoryGirl.define do
  factory(:character_sheet) do
    association(:character)

    after(:create) do |sheet|
      sheet.sheet.attach(io: StringIO.new('test'), filename: 'test.txt')
    end
  end
end
