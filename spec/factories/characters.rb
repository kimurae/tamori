# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :character do
    game_id 1
    association(:system, :with_experience)
    name 'Test'
    user

    trait :with_builds do
      transient do
        builds_count  1
      end

      after(:build) do |character, evaluator|
        character.builds    << build_list(:build,   evaluator.builds_count, system: character.system, user: character.user)
      end
    end

    trait :with_summaries do
      transient do
        summary_count 1
      end

      after(:build) do |character, evaluator|
        character.summaries << build_list(:summary, evaluator.summary_count)
      end
    end
  end
end
