FactoryGirl.define do
  factory(:detail) do
    book 'DMG'
    page '42'
    description 'And... its mate!'
    keywords { ['Stupid Thirty', 'Badger'] }
    raises   { ['+1k0 per raise'] }
    properties { { :list => 'beowulf', :level => '3' } }
  end
end
