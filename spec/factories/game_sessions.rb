# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :game_session do
    association(:system, :with_experience)
    association(:user)

    sequence(:name) do |n|
      "Build #{n}"
    end
    built_on { Date.civil(2014,1,1) }
    experience_awarded 2
  end
end
