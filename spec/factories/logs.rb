# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :log do
    description 'Test'
    session_on { Date.civil(2012,12,24) }
    notes <<HERE
### Listen
* In the begining of time there was an old man.
* The old man died.
HERE
    xp 3
    trait :with_character do
      character
    end
  end
end
