FactoryGirl.define do
  factory(:system) do
    sequence(:name) { |n| "Test #{n}" }
    renderer 'L5rRenderer'

    trait(:with_experience) do
      after(:create) do |system, evaluator|
        category = create(:category, label: 'Experience', system_id: system.id)
        create(:trait, label: 'Xp Awarded', name: 'xp_awarded', system_id: system.id, category_id: category.id)
      end
    end
  end
end
