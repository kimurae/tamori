FactoryGirl.define do
  factory(:trait) do
    association(:category)
    association(:system)
    name     'test'
    label    'Test'
    manual_order 1

    trait :with_details do
      detail
    end
  end
end
