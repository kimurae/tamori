FactoryGirl.define do
  factory(:user_item) do
    association(:category)
    association(:user)
    name 'Test'
    book 'Core'
    description 'This is a test'
    page '291'
    properties { { :something => 'nothing' } }
  end
end
