# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name 'Test User'
    sequence(:email)      { |n| "email#{n}@test.com" }
    password              'ABBA'
    password_confirmation 'ABBA'
  end
end
