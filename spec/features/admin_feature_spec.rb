require 'rails_helper'

RSpec.describe 'Admin Functionality', :js => true do

  let!(:user) { FactoryGirl.create(:user, :password => 'test', :password_confirmation => 'test', :admin => true, name: 'Joe Test') }

  specify do
    visit '/'
    fill_in 'Email',    :with => user.email
    fill_in 'Password', :with => 'test'
    click_button 'Login'

    expect(page).to have_content 'Joe Test'

    visit '/admin'

    expect(page).to have_content 'Systems'

    click_link 'Add New System'

    fill_in 'Name', :with => 'Legend of the Five Rings'
    click_button 'Create System'

    click_link 'Legend of the Five Rings'


    click_link 'Categories'
    click_on 'Create Category'

    fill_in 'Label', :with => 'Attribute'
    select 'air_attributes', :from => 'Name'
    click_on 'Create Category'

    click_link 'Traits'

    click_link 'Add New Trait'

    select 'Attribute', :from => 'Category'
    fill_in 'Name',     :with => 'agility'
    fill_in 'Label',    :with => 'Agility'

    click_link 'Details (Optional)'

    within('#trait-details-form-collapse') do

      fill_in 'Book', :with => 'Core'
      fill_in 'Page', :with => '143'

      fill_in 'Description', :with => 'Your Mom'

      # TODO: Figure out how to emulate the keyup/keydown events here.
      fill_in 'Keywords', with: 'Awesome'
      click_on 'Add Keywords'

      fill_in 'Raises (L5R Only', with: '+ 1k1'
      click_on 'Add Raises'

      fill_in 'List',       with: 'Void'
      fill_in 'Level',      with: '2'
      fill_in 'Area',       with: 'Personal'
      fill_in 'Cost',       with: '2'
      fill_in 'Damage',     with: 'None'
      fill_in 'Demands',    with: 'Variable'
      fill_in 'Difficulty', with: 'Terrible'
      fill_in 'Duration',   with: '2 minutes'
      fill_in 'Range',      with: '100 ft'
      fill_in 'Roll',       with: 'Perception + Investigation'
      fill_in 'Reduction',  with: 'None'
      fill_in 'TN bonus',   with: '+2'
    end

    click_button 'Create Trait'

    expect(page).to have_content 'Attribute'
    expect(page).to have_content 'Agility'
    click_link 'Agility'

    click_on 'Details (Optional)'
    expect(find_field('Description').value).to eq 'Your Mom'

    within('#trait-details-form-collapse') do
     uncheck 'Awesome'
    end

    fill_in 'Label', :with => 'Your Dad'
    click_button 'Update Trait'

    click_link 'Traits'

    expect(page).to have_content 'Your Dad'
  end
end
