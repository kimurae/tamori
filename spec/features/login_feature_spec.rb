require 'rails_helper'

RSpec.describe 'The login process', type: :feature, js: true do
  include SampleDataUri

  let!(:user) { FactoryGirl.create(:user, name: 'Test User') }

  before do
    FactoryGirl.create(:system, name: 'Test').tap do |system|
      FactoryGirl.create(:category, label: 'Traps', name: 'traps', system: system)
      FactoryGirl.create(:category, label: 'Advantages', name: 'advantages', system: system).tap do |advantage|
        FactoryGirl.create(:trait, category: advantage, name: 'dusk_aspect', label: 'Dusk Aspect', system: system)
      end
    end
  end

  it 'signs in a valid user' do

    visit '/'
    fill_in 'Email', :with => user.email
    fill_in 'Password', :with => 'ABBA'
    click_button 'Login'

    expect(page).to have_content 'Test User'

    click_on 'Add Character'

    fill_in 'Name', with: 'Mr. Potato Head'
    select  'Test', from: 'System'

    click_button 'Create Character'

    expect(page).to have_content 'Edit'

    visit "/characters/#{user.character_ids.first}.html"

    expect(page).to have_content 'Mr. Potato Head'

    click_link  'Test User'
    click_link  'Items'
    click_on    'Add New Item'

    select      'Test: Traps',        from: 'Category'
    fill_in     'Name',    with: 'Booby Trap'
    fill_in     'Book',         with: 'Paranoia'
    fill_in     'Page',         with: '666'
    fill_in     'Description',  with: 'Its a giant rock that rolls towards fedoras'
    fill_in     'Devotion',     with: '2'
    fill_in     'Influence',    with: '1'
    fill_in     'Mentor',       with: '3'
    fill_in     'Level',        with: '1'
    fill_in     'List',         with: 'Traps'

    click_on    'Create User item'

    expect(page).to have_content 'Traps'
    expect(page).to have_content 'Booby Trap'

    click_link 'Edit'
    fill_in    'Name',     with: 'Boulder Trap'
    click_on   'Update User item'

    visit "/characters/#{user.character_ids.first}.html"

    click_link 'Build'

    click_on 'Add Build'

    click_link 'Edit'

    fill_in   'Name',     with: 'Test Build'
    fill_in   'Built on', with: '2016/08/19'

    click_on  'Update Build'

    within('#builds') do
      expect(page).to have_content 'Aug 19, 2016'
      expect(page).to have_content 'Test Build'
    end

    click_link 'Edit'

    click_link 'Add Aspect'
    fill_in    'Comment',     with: 'Gained Dusk Aspect'
    select     'Advantages',  from: 'Category'
    select     'Dusk Aspect', from: 'Trait'
    choose     'Active'
    fill_in    'Cost',        with: '2'
    fill_in    'Cost type',   with: 'XP'
    fill_in    'Notes', with: 'Duly Noted'
    click_on   'Add Notes'
    click_on   'Create Build aspect'

    expect(page).to have_content 'Gained Dusk Aspect'
    expect(page).to have_content '2 XP'

    click_link 'Add Property'
    fill_in    'Comment',     with: 'Gained Dusk Aspect'
    select     'Advantages',  from: 'Category'
    select     'Dusk Aspect', from: 'Trait'
    fill_in    'Value',       with: 'Dawn'
    fill_in    'Cost',        with: '2'
    fill_in    'Cost type',   with: 'XP'
    click_on   'Create Build property'

    click_link 'Add Rank'
    fill_in    'Comment',     with: 'Gained Dusk Aspect'
    select     'Advantages',  from: 'Category'
    select     'Dusk Aspect', from: 'Trait'
    fill_in    'Value',       with: '2.1'
    fill_in    'Cost',        with: '2'
    fill_in    'Cost type',   with: 'XP'
    click_on   'Create Build rank'

    click_link 'Add Attribute'
    fill_in    'Comment',     with: 'Gained Dusk Aspect'
    select     'Advantages',  from: 'Category'
    select     'Dusk Aspect', from: 'Trait'
    fill_in    'Value',       with: '2'
    fill_in    'Cost',        with: '2'
    fill_in    'Cost type',   with: 'XP'
    fill_in    'Emphasis',    with: 'Sneaking'
    click_on   'Add Emphasis'
    click_on   'Create Build attribute'

    click_link 'Add Essay'
    fill_in    'Comment',     with: 'Gained Dusk Aspect'
    select     'Advantages',  from: 'Category'
    select     'Dusk Aspect', from: 'Trait'
    fill_in    'Value',       with: 'I am dark and weary'
    click_on   'Create Build essay'

    click_link 'Add Item'
    select     'Boulder Trap',   from: 'Item'
    fill_in    'Comment',     with: 'Gained Dusk Aspect'
    fill_in    'Qty',         with: '1'
    fill_in    'Location',    with: 'Deep Space Nine'
    fill_in    'Cost',        with: '2'
    fill_in    'Cost type',   with: 'XP'
    fill_in    'Notes', with: 'Duly Noted'
    click_on   'Add Note'
    click_on   'Create Build item'

    click_link 'Build'
    click_link 'Delete'

    click_link 'Update'

    expect(page).to have_content 'Test'

    fill_in     'Name', with: 'Johanes Bach'
    attach_file 'Portrait', File.join(Rails.root, %w(spec samples portrait.png))
    expect(find('.portrait-preview')['src']).to have_content sample_data_uri
    click_button 'Update Character'
  end
end
