describe('AdminCtrl', function() {

  var scope, ctrl, $httpbackend;

  beforeEach(module('tamoriApp'));

  beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/systems.json').
        respond([
          {id: '3fd152ea-39f2-4090-9622-5a52de1c85dc', name: 'Legend of the Five Rings'},
          {id: 'c17b4c3b-077b-4ac9-bdf7-55d960e3392a', name: 'D20 v5'}]);

    $httpBackend.whenGET('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/calculations.json').
      respond([]);

    //$httpBackend.expectGET('/systems/c17b4c3b-077b-4ac9-bdf7-55d960e3392a/calculations.json').
    //  respond([]);

    scope = $rootScope.$new();
    ctrl = $controller('AdminCtrl', {$scope: scope});
  }));

  it('should create a "systems" model with two systems initially', function() {
    $httpBackend.flush();
    expect(scope.systems.length).toBe(2);
  });

  it('should create "traits" model when a system is selected', function() {
    $httpBackend.flush();

    $httpBackend.expectGET('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits.json').
      respond([
        { category_id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc',
          category: 'Test',
          traits: [
            {id: '150b19ce-cab1-4304-8d80-e2c22d805626', name: 'Awesomeness' }
          ]
        }
      ]);

    $httpBackend.expectGET('/api/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/categories.json').
      respond([]);

    scope.currentSystem = scope.systems[0];
    scope.$apply();
    $httpBackend.flush();
    expect(scope.traits.length).toBe(1);
  });

  it('should create a "system" model when saving.', function() {

    $httpBackend.flush();

    $httpBackend.expectPOST('/systems.json').
      respond({ id: '904c1582-3f51-4b10-9619-1815137b24bc', name: 'A Game of Thrones' });

    $httpBackend.expectGET('/systems/904c1582-3f51-4b10-9619-1815137b24bc/traits.json').
      respond([]);

    $httpBackend.expectGET('/systems/904c1582-3f51-4b10-9619-1815137b24bc/calculations.json').
      respond([]);

    $httpBackend.expectGET('/api/systems/904c1582-3f51-4b10-9619-1815137b24bc/categories.json').
      respond([]);

    // No idea why length is 2, but it has 3 items o.0
    expect(scope.systems.length).toBe(2);

    scope.addSystem();
    $httpBackend.flush();

    expect(scope.currentSystem.id).toEqual('904c1582-3f51-4b10-9619-1815137b24bc');

  });

  it('should create a "trait" model when saving', function() {
    $httpBackend.flush();


    $httpBackend.expectGET('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits.json').
      respond([
        { category_id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc',
          category: 'Test',
          traits: [
            {id: '150b19ce-cab1-4304-8d80-e2c22d805626', name: 'Awesomeness' }
          ]
        }
      ]);

    $httpBackend.expectGET('/api/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/categories.json').
      respond([]);

    scope.currentSystem = scope.systems[0];
    scope.$apply();
    $httpBackend.flush();

    $httpBackend.expectPOST('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits.json').
      respond({ id: '6cd8f211-f314-4ec1-860c-202c1273bc75' });

    $httpBackend.expectGET('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits.json').
      respond([
        { category_id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc',
          category: 'Test',
          traits: [
            {id: '150b19ce-cab1-4304-8d80-e2c22d805626', name: 'Awesomeness' }
          ]
        },
        { category_id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc',
          category: 'Test',
          traits: [
            {id: '6cd8f211-f314-4ec1-860c-202c1273bc75', name: 'Coolness' }
          ]
        }
      ]);

    var event = jQuery.Event('click');

    scope.openTraitModal(event, '#something');
    scope.submit();
    $httpBackend.flush();

    expect(scope.traits.length).toBe(2);

  });

  it('should update a "trait" model when saving', function() {
    $httpBackend.flush();

    $httpBackend.whenGET('/api/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/categories.json').
      respond([{ id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc', label: 'Test', name: 'test' }]);

    $httpBackend.whenPUT('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits.json').respond({});

    $httpBackend.expectGET('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits.json').
      respond([
        { category_id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc',
          category: 'Test',
          traits: [
            {id: '150b19ce-cab1-4304-8d80-e2c22d805626', name: 'Awesomeness' }
          ]
        }
      ]);

    scope.currentSystem = scope.systems[0];
    scope.$apply();
    $httpBackend.flush();

    $httpBackend.expectGET('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits/150b19ce-cab1-4304-8d80-e2c22d805626.json').
      respond({ id: '150b19ce-cab1-4304-8d80-e2c22d805626',
        category: 'Test',
        label:    'Awesomeness',
        name: 'awesomeness',
        detail: {
          book: 'DMG',
          page: '69',
          keywords: [ 'Mental' ],
          properties: { lisp: 'none' }
        }
      });

    //$httpBackend.expectPUT('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits/150b19ce-cab1-4304-8d80-e2c22d805626.json').
      //respond({ id: '6cd8f211-f314-4ec1-860c-202c1273bc75' });

    $httpBackend.expectGET('/systems/3fd152ea-39f2-4090-9622-5a52de1c85dc/traits.json').
      respond([
        { category_id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc',
          category: 'Test',
          traits: [
            {id: '150b19ce-cab1-4304-8d80-e2c22d805626', name: 'Awesomeness' }
          ]
        },
        { category_id: 'test-3fd152ea-39f2-4090-9622-5a52de1c85dc',
          category: 'Test',
          traits: [
            {id: '6cd8f211-f314-4ec1-860c-202c1273bc75', name: 'Coolness' }
          ]
        }
      ]);

    var event = jQuery.Event('click');

    scope.openTraitModal(event, '#something', '150b19ce-cab1-4304-8d80-e2c22d805626');
    //$httpBackend.flush();
    console.log(scope);

    scope.submit();
    $httpBackend.flush();

    expect(scope.traits.length).toBe(2);

  });
});
