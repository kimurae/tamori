require 'rails_helper'

RSpec.describe CharacterCompileJob do

  let!(:system)       { FactoryGirl.create(:system) }

  let(:advantages)          { FactoryGirl.create(:category, :label => 'Advantage',      :name => 'advantages') }
  let(:experience_category) { FactoryGirl.create(:category, :label => 'Experience',     :name => 'experience') }
  let(:factions)            { FactoryGirl.create(:category, :label => 'Faction',        :name => 'factions') }
  let(:fire_attributes)     { FactoryGirl.create(:category, :label => 'Fire Attribute', :name => 'fire_attributes') }
  let(:rings)               { FactoryGirl.create(:category, :label => 'Ring',           :name => 'rings') }

  let!(:agility)      { FactoryGirl.create(:trait, :system_id => system.id, :name => 'agility',      :label => 'Agility',      :category => fire_attributes) }
  let!(:experience)   { FactoryGirl.create(:trait, :system_id => system.id, :name => 'experience',   :label => 'Experience Spent / Total',   :category => experience_category) }
  let!(:intelligence) { FactoryGirl.create(:trait, :system_id => system.id, :name => 'intelligence', :label => 'Intelligence', :category => fire_attributes) }
  let!(:property)     { FactoryGirl.create(:trait, :system_id => system.id, :name => 'clan',         :label => 'Clan',         :category => factions) }
  let!(:sage)         { FactoryGirl.create(:trait, :system_id => system.id, :name => 'sage',         :label => 'Sage',         :category => advantages) }
  let!(:fire_ring)    { FactoryGirl.create(:trait, :system_id => system.id, :name => 'fire_ring',    :label => 'Fire',         :category => rings ) }
  let!(:xp_awarded)   { FactoryGirl.create(:trait, :system_id => system.id, :name => 'xp_awarded',   :label => 'XP Awarded',   :category => experience_category ) }
  let!(:xp_cost)      { FactoryGirl.create(:trait, :system_id => system.id, :name => 'xp_cost', :category => experience_category, :label => 'XP Cost') }

  let(:character) { FactoryGirl.create(:character, :system_id => system.id, :name => 'Test') }

  let(:sheet) { CharacterSheet.create(:character_id => character.id) }

  before do

    FactoryGirl.create(:build_calculation,
      :activation,
      trait_id: fire_ring.id,
      system_id: system.id
    )

    FactoryGirl.create(:build_calculation,
      :calculator => 'MinCalculator',
      :system_id => system.id,
      :trait_id => fire_ring.id,
      :trait_ids => [agility.id, intelligence.id],
      :argument => nil,
      :priority => 1)
    FactoryGirl.create(:build_calculation,
      :calculator => 'ConcatCalculator',
      :system_id => system.id,
      :trait_id => experience.id,
      :trait_ids => [xp_cost.id, xp_awarded.id],
      :argument => ' / ',
      :priority => 2)

    FactoryGirl.create(:build, characters: [character], system_id: system.id, user: character.user).tap do |b|
      FactoryGirl.create(:build_attribute, :build_id => b.id, :trait_id => agility.id, :value => 2, :cost => 0)
      FactoryGirl.create(:build_attribute, :build_id => b.id, :trait_id => intelligence.id, :value => 2, :cost => 0)
      FactoryGirl.create(:build_attribute, :build_id => b.id, :trait_id => xp_awarded.id, :value => 24, :cost => 0)
      FactoryGirl.create(:build_property,  :build_id => b.id, :trait_id => property.id, :value => 'Lion', :cost => 0)
    end
    FactoryGirl.create(:build, characters: [character], system_id: system.id, user: character.user).tap do |b|
      FactoryGirl.create(:build_attribute, :build_id => b.id, :trait_id => agility.id, :value => 1, :cost => 12, :cost_type => 'XP')
    end
    FactoryGirl.create(:build, characters: [character], system_id: system.id, user: character.user).tap do |b|
      FactoryGirl.create(:build_aspect, :build_id => b.id, :trait_id => sage.id, :active => true, :cost => 4, :cost_type => 'XP')
    end

    CharacterCompileJob.perform_now(sheet.id, character.build_ids)
  end

  subject { CharacterSheet.find(sheet.id).data.with_indifferent_access }

  let(:agility_value)       { subject['fire_attributes'].detect { |a| a['label'] == 'Agility'      }['value'] }
  let(:clan_value)          { subject['factions'].detect        { |a| a['label'] == 'Clan'         }['value'] }
  let(:intelligence_value)  { subject['fire_attributes'].detect { |a| a['label'] == 'Intelligence' }['value'] }
  let(:fire_ring_value)     { subject['rings'].detect           { |a| a['label'] == 'Fire'         }['value'] }

  let(:name_value)          { subject['name']['value'] }
  let(:xp_spent)            { subject['experience']['value'] }

  let(:agility_active)       { subject['fire_attributes'].detect { |a| a['label'] == 'Agility'      }['active'] }
  let(:clan_active)          { subject['factions'].detect        { |a| a['label'] == 'Clan'         }['active'] }
  let(:intelligence_active)  { subject['fire_attributes'].detect { |a| a['label'] == 'Intelligence' }['active'] }
  let(:fire_ring_active)     { subject['rings'].detect           { |a| a['label'] == 'Fire'         }['active'] }

  let(:name_active)          { subject['name']['active'] }
  let(:xp_active)            { subject['experience']['active'] }


  specify do
    expect(agility_value).to      eq 3
    expect(clan_value).to         eq 'Lion'
    expect(intelligence_value).to eq 2
    expect(fire_ring_value).to    eq 2
    expect(name_value).to         eq 'Test'
    expect(xp_spent).to           eq '16 / 24'
    expect(subject['advantages'].first['name']).to eq 'sage'

    expect(agility_active).to       eq true
    expect(clan_active).to          eq true
    expect(intelligence_active).to  eq true
    expect(fire_ring_active).to     eq true
    expect(name_active).to          eq true
    expect(xp_active).to            eq true

    expect(subject['advantages'].map { |a| a['active'] }.all? ).to eq true
  end
end
