require 'rails_helper'

RSpec.describe BuildAspect, :type => :model do

  subject { BuildAspect.new(*valid_attributes) }

  let(:valid_attributes) do
    {
      :build_id   => 42,
      :trait_id   => 'b812b5ab-43ee-4a08-871a-83535f723271',
      :active     => true,
      :comment    => 'This is a test.',
      :cost       => 2,
      :cost_type  => 'XP',
      :notes      => ['One', 'Two']
    }
  end

  describe '#to_component' do

    let(:expected_results) do
      {
        :id       => 'b812b5ab-43ee-4a08-871a-83535f723271',
        :active   => true,
        :xp_cost  => 2,
        :summary  => '',
        :notes    => ['One', 'Two'],
        :value    => true
      }
    end

    subject do
      BuildAspect.create(valid_attributes)
      BuildComponent.compiled([42]).first.to_component
    end

    specify { expect(subject).to eq expected_results }
  end

  specify { expect(BuildAspect.new.value_type).to eq 'boolean' }
end
