require 'rails_helper'

RSpec.describe BuildAttribute, :type => :model do

  let(:valid_attributes) do
    {
      :build_id   => 42,
      :trait_id   => 'b812b5ab-43ee-4a08-871a-83535f723271',
      :comment    => 'This is a test.',
      :cost       => 2,
      :cost_type  => 'XP',
      :emphasis   => ['free'],
      :value      => 1
    }
  end

  describe '#to_component' do

    let(:expected_results) do
      {
        :id       => 'b812b5ab-43ee-4a08-871a-83535f723271',
        :active   => true,
        :emphasis => ['free'],
        :notes    => [],
        :summary  => ' 1',
        :xp_cost  => 2,
        :value    => 1
      }
    end

    subject do
      BuildAttribute.create(valid_attributes)
      BuildAttribute.compiled([42]).first.to_component
    end

    specify { expect(subject).to eq expected_results }
  end

  specify { expect(BuildAttribute.new.value_type).to eq 'integer' }
end
