require 'rails_helper'

RSpec.describe BuildEssay, :type => :model do

  let(:valid_attributes) do
    {
      :build_id   => 42,
      :trait_id   => 'b812b5ab-43ee-4a08-871a-83535f723271',
      :comment    => 'This is a test.',
      :cost       => 0,
      :cost_type  => 'XP',
      :value      => 'Its the song that never ends...'
    }
  end

  describe '#to_component' do

    let(:expected_results) do
      {
        :id       => 'b812b5ab-43ee-4a08-871a-83535f723271',
        :active   => true,
        :notes    => [],
        :summary  => ' Its the song that never ends...',
        :value      => 'Its the song that never ends...',
        :xp_cost  => 0
      }
    end

    subject do
      BuildEssay.create(valid_attributes)
      BuildEssay.compiled([42]).first.to_component
    end

    specify { expect(subject).to eq expected_results }
  end

  specify { expect(BuildEssay.new.value_type).to eq 'text' }
end
