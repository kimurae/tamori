require 'rails_helper'

RSpec.describe BuildItem, :type => :model do

  let(:valid_attributes) do
    {
      :build_id   => 42,
      :trait_id   => 'b812b5ab-43ee-4a08-871a-83535f723271',
      :comment    => 'This is a test.',
      :cost       => 2,
      :cost_type  => 'GP',
      :location   => 'T',
      :notes      => ['One', 'Two'],
      :qty        => 3
    }
  end

  describe '#to_component' do

    let(:expected_results) do
      {
        id:       'b812b5ab-43ee-4a08-871a-83535f723271',
        gp_cost:  2,
        active:   true,
        location: 'T',
        notes:    ['One', 'Two'],
        qty:      3
      }
    end

    before do
      allow_any_instance_of(BuildItem).to receive(:trait_to_component) { Hash.new }
      BuildItem.create(valid_attributes)
    end

    subject do
      BuildItem.compiled([42]).first.to_component
    end

    specify { expect(subject).to eq expected_results }
  end
end
