require 'rails_helper'

RSpec.describe BuildProperty, :type => :model do

  let(:valid_attributes) do
    {
      :build_id   => 42,
      :trait_id   => 'b812b5ab-43ee-4a08-871a-83535f723271',
      :comment    => 'This is a test.',
      :cost       => 2,
      :cost_type  => 'XP',
      :value      => 'real'
    }
  end

  describe '#to_component' do

    let(:expected_results) do
      {
        :id       => 'b812b5ab-43ee-4a08-871a-83535f723271',
        :active   => true,
        :notes    => [],
        :summary  => ' real',
        :xp_cost  => 2,
        :value    => 'real'
      }
    end

    subject do
      BuildProperty.create(valid_attributes)
      BuildProperty.compiled([42]).first.to_component
    end

    specify { expect(subject).to eq expected_results }
  end

  specify { expect(BuildProperty.new.value_type).to eq 'text' }
end
