require 'rails_helper'

RSpec.describe BuildRank, :type => :model do

  let(:valid_attributes) do
    {
      :build_id   => 42,
      :trait_id   => 'b812b5ab-43ee-4a08-871a-83535f723271',
      :comment    => 'This is a test.',
      :cost       => 2,
      :cost_type  => 'XP',
      :value      => 1.2
    }
  end

  describe '#to_component' do

    let(:expected_results) do
      {
        :id       => 'b812b5ab-43ee-4a08-871a-83535f723271',
        :active   => true,
        :notes    => [],
        :summary  => ' 1.2',
        :xp_cost  => 2,
        :value    => 1.2
      }
    end

    subject do
      BuildRank.create(valid_attributes)
      BuildRank.compiled([42]).first.to_component
    end

    specify { expect(subject).to eq expected_results }
  end

  specify { expect(BuildRank.new.value_type).to eq 'numeric(3,2)' }
end
