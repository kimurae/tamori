require 'rails_helper'

# Right now character does absolutely nothing,
# Will add tests here once I add functionality to character.
RSpec.describe Character, :type => :model do

  describe '#description' do

    subject do
      Character.new(data: { 'description' => [ { 'value' => 'Amazing Description' } ] })
    end

    specify { expect(subject.description).to eq 'Amazing Description' }
  end
end
