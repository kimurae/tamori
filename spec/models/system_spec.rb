require 'rails_helper'

RSpec.describe System do

  let(:valid_attributes) do
    { :name => 'name' }
  end

  subject { System.new(valid_attributes) }

  specify do
    expect(subject).to be_valid
    expect { subject.save }.to change(System, :count).by(1)
  end

  describe 'with invalid attributes' do
    subject { System.new.tap(&:valid?) }

    specify { expect(subject).not_to be_valid }
    specify { expect { subject.save }.to change(System, :count).by(0) }
    specify { expect(subject.errors['name'].first).to       eq "can't be blank" }

  end

  describe 'with duplicate names' do
    before do
      System.create(valid_attributes)
      subject.valid?
    end

    specify do
      expect(subject).not_to be_valid
      expect(subject.errors['name'].first).to eq "has already been taken"
    end

    specify { expect { subject.save }.to change(System, :count).by(0) }
  end

end
