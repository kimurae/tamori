require 'rails_helper'

RSpec.describe Trait do

  let(:valid_attributes) do
    {
      :system_id    => SecureRandom.uuid,
      :name         => 'name',
      :category_id  => SecureRandom.uuid,
      :label        => 'Name'
    }
  end

  subject { Trait.new(valid_attributes) }

  specify do
    expect(subject).to be_valid
    expect { subject.save }.to change(Trait, :count).by(1)
  end

  describe 'with invalid attributes' do
    subject { Trait.new.tap(&:valid?) }

    specify { expect(subject).not_to be_valid }
    specify { expect { subject.save }.to change(Trait, :count).by(0) }
    specify { expect(subject.errors['category_id'].first).to   eq "can't be blank" }
    specify { expect(subject.errors['label'].first).to      eq "can't be blank" }
    specify { expect(subject.errors['name'].first).to       eq "can't be blank" }
    specify { expect(subject.errors['system_id'].first).to  eq "can't be blank" }

  end

  describe 'with attribute dependency' do
    before do
      subject.save
      BuildAttribute.create(:build_id => 42, :trait_id => subject.id)
    end

    specify { expect { subject.destroy }.to raise_exception(ActiveRecord::DeleteRestrictionError) }
  end

  describe 'with property dependency' do
    before do
      subject.save
      BuildProperty.create(:build_id => 42, :trait_id => subject.id)
    end

    specify { expect { subject.destroy }.to raise_exception(ActiveRecord::DeleteRestrictionError) }
  end

  describe '#to_component' do

    let(:valid_properties) do
      {
        'damage' => '3k3',
        'range' => '100 ft',
      }
    end

    let(:expected_results) do
      {
        :id         => subject.id,
        :active     => false,
        :category   => 'Test',
        :category_name => 'test',
        :book       => 'Core',
        :description => 'Really this is a test',
        :label      => 'Name',
        :name       => 'name',
        :keywords   => ['One', 'Two'],
        :page       => '320',
        :damage     => '3k3',
        :range      => '100 ft'
      }
    end

    before do
      allow_any_instance_of(Trait).to receive(:category_label) { 'Test' }
      allow_any_instance_of(Trait).to receive(:category_name)  { 'test' }
      allow_any_instance_of(Trait).to receive(:detail_book)    { 'Core' }
      allow_any_instance_of(Trait).to receive(:detail_description) { 'Really this is a test' }
      allow_any_instance_of(Trait).to receive(:detail_keywords)    { ['One', 'Two'] }
      allow_any_instance_of(Trait).to receive(:detail_page)        { '320' }
      allow_any_instance_of(Trait).to receive(:detail_properties)  { valid_properties }
      Trait.create(valid_attributes)
    end

    subject { Trait.first }
    specify { expect(subject.to_component).to eq expected_results }
  end

  describe '#for_system' do

    let(:attribute_category) { FactoryGirl.create(:category, :attribute,  :system => system) }
    let(:skill_category)     { FactoryGirl.create(:category, :skill,      :system => system) }
    let(:system)             { FactoryGirl.create(:system) }

    before do
      Trait.create(:system_id => system.id, :category_id => attribute_category.id, :name => 'trait_4', :label => 'Stamina', :manual_order => 3)
      Trait.create(:system_id => system.id, :category_id => attribute_category.id, :name => 'trait_1', :label => 'Strength', :manual_order => 1)
      Trait.create(:system_id => system.id, :category_id => attribute_category.id, :name => 'trait_2', :label => 'Dexterity', :manual_order => 2)
      Trait.create(:system_id => system.id, :category_id => skill_category.id,     :name => 'trait_3', :label => 'Subterfuge')
      Trait.create(:system_id => system.id, :category_id => skill_category.id,     :name => 'trait_3', :label => 'Awareness')
      Trait.create(:system_id => system.id, :category_id => skill_category.id,     :name => 'trait_3', :label => 'Athletics')
      Trait.create(:system_id => system.id, :category_id => skill_category.id,     :name => 'trait_3', :label => 'Brawl')
    end

    subject { Trait.for_system(system.id).to_a.map(&:label) }

    specify { is_expected.to eq ['Strength', 'Dexterity', 'Stamina', 'Athletics', 'Awareness', 'Brawl', 'Subterfuge'] }

  end
end
