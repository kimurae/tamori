require 'rails_helper'

RSpec.describe CompiledComponentsQuery do

  before { FactoryGirl.create_list(:build_attribute, 2) }

  let(:correct_sql) do
    [
      %q{SELECT "build_components"."type",},
      %q{"build_components"."trait_id",},
      %q{"build_components"."trait_type",},
      %q{LAST("build_components"."location") AS location,},
      %q{COMPILE("build_components"."value", "build_components"."value_type") AS value,},
      %q{array_agg_mult("build_components"."notes") AS notes,},
      %q{SUM(CASE WHEN "build_components"."cost_type" = 'XP' THEN "build_components"."cost" ELSE 0 END) AS xp_cost},
      %Q{FROM "build_components" WHERE "build_components"."build_id" IN (#{Build.pluck(:id).map { |i| "'#{i}'" }.join(', ')})},
      %q{GROUP BY "build_components"."trait_id", "build_components"."trait_type", "build_components"."type"},
      %q{HAVING (type NOT IN ('BuildAspect', 'BuildItem') OR (type = 'BuildAspect' AND LAST(value) = 't') OR (type = 'BuildItem' AND SUM(CASE WHEN type = 'BuildItem' THEN value::integer ELSE 0 END) > 0))},
      %q{ORDER BY "build_components"."trait_id" ASC}
    ].join(' ')
  end

  specify do
    CompiledComponentsQuery.call(Build.pluck(:id)).to_sql.split(' ').zip(correct_sql.split(' ')).each do |(a,b)|
      expect(a).to eq(b)
    end
  end
end
