require 'rails_helper'

RSpec.describe L5rRenderer do

  let(:data) do
    JSON.parse(File.read(File.join(Rails.root, 'spec', 'samples', 'seventh_sea_character_data.json'))).with_indifferent_access
  end

  specify do
    GenericRenderer.new(data).save_as('test.pdf')
    `open test.pdf` 
  end
end
