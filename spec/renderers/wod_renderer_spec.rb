require 'rails_helper'

RSpec.describe WodRenderer do

  let(:data) do
    JSON.parse(File.read(File.join(Rails.root, 'spec', 'samples', 'wod_character_data.json'))).with_indifferent_access
  end

  specify do
    WodRenderer.new(data).save_as('test_wod.pdf')
    `open test_wod.pdf`
  end
end
