require 'rails_helper'

RSpec.describe 'Traits', type: :request do

  let(:current_user)  { FactoryGirl.create(:user, :admin => true) }
  let(:system)        { FactoryGirl.create(:system) }

  subject { JSON.parse(response.body) }

  describe 'GET /api/system/:system_id/traits.json' do

    let!(:traits) { FactoryGirl.create_list(:trait, 2, :system_id => system.id) }

    before do
      get api_system_traits_path(system, :format => :json)
    end

    specify do
      expect(response.status).to be(200)
      expect(response).to match_response_schema('api/systems/traits/index')
    end
  end
end
