require 'rails_helper'

RSpec.describe 'builds', type: :request do
  let(:character)    { FactoryGirl.create(:character) }
  let(:current_user) { character.user }

  describe 'GET /characters/:character_id/builds' do
    let!(:build)        { FactoryGirl.create(:build, name: 'Dummy', built_on: '2016/08/25', characters: [character]) }

    before  { get character_builds_path(character) }
    specify { expect(response).to have_http_status(:ok)     }
  end

  describe 'POST /characters/:character_id/builds' do
    before  { post character_builds_path(character.id)  }
    specify { expect(response).to redirect_to character_builds_path(character.id) }
  end

  describe 'GET /characters/:character_id/builds/:id/edit' do
    let(:build)        { FactoryGirl.create(:build, name: 'Dummy', built_on: '2016/08/25', characters: [character]) }

    before  { get edit_character_build_path(character, build) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'PATCH /characters/:character_id/builds/:id' do
    let(:build)        { FactoryGirl.create(:build, name: 'Dummy', built_on: '2016/08/25', characters: [character]) }

    describe 'with valid parameters' do
      let(:valid_attributes) do
        {
          build: { name: 'Meh', built_on: '2015/08/24' }
        }
      end

      before do
        patch(character_build_path(character, build), params: valid_attributes)
      end

      specify { expect(response).to redirect_to character_builds_path(character) }
    end

    describe 'with invalid parameters' do
      let(:invalid_attributes) do
        {
          build: { name: '' }
        }
      end

      before do
        patch(character_build_path(character, build), params: invalid_attributes)
      end

      it 'should not redirect, but instead render the existing page with the validation errors' do
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'DELETE /characters/:character_id/builds/:id' do
    let(:build)         { FactoryGirl.create(:build, characters: [character]) }

    before { delete character_build_path(character, build) }
    specify { expect(response).to redirect_to character_builds_path(character) }
  end
end
