require 'rails_helper'

RSpec.describe '/characters/:character_id/builds/:build_id/build_aspects', type: :request do
  let(:build)        { character.builds.first }
  let(:character)    { FactoryGirl.create(:character, :with_builds) }
  let(:current_user) { build.user }
  let(:trait)        { FactoryGirl.create(:trait) }

  describe 'GET /characters/:character_id/builds/:build_id/build_aspects/:id/new' do
    before  { get new_character_build_build_aspect_path(character, build) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'POST /characters/:character_id/builds/:build_id/build_aspects' do
    let(:valid_attributes) do
      {
        build_aspect: {
          comment: 'Test',
          trait_id: trait.id,
          active: true,
          cost: '2',
          cost_type: 'XP'
        }
      }
    end

    it 'should create the record then redirect to index when posting valid attributes' do
      post(character_build_build_aspects_path(character, build), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildAspect.count).to eq(1)
    end
  end

  describe 'GET /characters/:character_id/builds/:build_id/build_aspects/:id/edit' do
    let(:build_aspect) { FactoryGirl.create(:build_aspect, build: build) }

    before  { get edit_character_build_build_aspect_path(character, build, build_aspect) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /characters/:character_id/builds/:build_id/build_aspects/:id' do
    let(:build_aspect) { FactoryGirl.create(:build_aspect, build: build) }

    let(:valid_attributes) do
      {
        build_aspect: { active: false }
      }
    end

    it 'should update the record, then redirect to index when posting valid attributes' do
      patch(character_build_build_aspect_path(character, build, build_aspect), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildAspect.where(active: false).count).to eq(1)
    end
  end

  describe 'DELETE /characters/:character_id/builds/:build_id/build_aspects/:id' do
    let(:build_aspect)  { FactoryGirl.create(:build_aspect, build: build) }

    before { delete character_build_build_aspect_path(character, build, build_aspect) }

    specify { expect(response).to redirect_to edit_character_build_path(character, build) }
  end
end
