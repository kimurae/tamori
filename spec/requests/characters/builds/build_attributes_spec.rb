require 'rails_helper'

RSpec.describe '/characters/:character_id/builds/:build_id/build_attributes', type: :request do
  let(:build)        { character.builds.first }
  let(:character)    { FactoryGirl.create(:character, :with_builds) }
  let(:current_user) { build.user }
  let(:trait)        { FactoryGirl.create(:trait) }

  describe 'GET /characters/:character_id/builds/:build_id/build_attributes/:id/new' do
    before  { get new_character_build_build_attribute_path(character, build) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'POST /characters/:character_id/builds/:build_id/build_attributes' do
    let(:valid_attributes) do
      {
        build_attribute: {
          comment: 'Test',
          trait_id: trait.id,
          value: 21,
          cost: '2',
          cost_type: 'XP',
          emphasis: ['Speed']
        }
      }
    end

    it 'should create the record then redirect to index when posting valid attributes' do
      post(character_build_build_attributes_path(character, build), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildAttribute.count).to eq(1)
    end
  end

  describe 'GET /characters/:character_id/builds/:build_id/build_attributes/:id/edit' do
    let(:build_attribute) { FactoryGirl.create(:build_attribute, build: build) }

    before  { get edit_character_build_build_attribute_path(character, build, build_attribute) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /characters/:character_id/builds/:build_id/build_attributes/:id' do
    let(:build_attribute) { FactoryGirl.create(:build_attribute, build: build) }

    let(:valid_attributes) do
      {
        build_attribute: { value: 32 }
      }
    end

    it 'should update the record, then redirect to index when posting valid attributes' do
      patch(character_build_build_attribute_path(character, build, build_attribute), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildAttribute.where(value: 32).count).to eq(1)
    end
  end

  describe 'DELETE /characters/:character_id/builds/:build_id/build_attributes/:id' do
    let(:build_attribute)  { FactoryGirl.create(:build_attribute, build: build) }

    before { delete character_build_build_attribute_path(character, build, build_attribute) }

    specify { expect(response).to redirect_to edit_character_build_path(character, build) }
  end
end
