require 'rails_helper'

RSpec.describe '/characters/:character_id/builds/:build_id/build_essays', type: :request do
  let(:build)        { character.builds.first }
  let(:character)    { FactoryGirl.create(:character, :with_builds) }
  let(:current_user) { build.user }
  let(:trait)        { FactoryGirl.create(:trait) }

  describe 'GET /characters/:character_id/builds/:build_id/build_essays/:id/new' do
    before  { get new_character_build_build_essay_path(character, build) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'POST /characters/:character_id/builds/:build_id/build_essays' do
    let(:valid_attributes) do
      {
        build_essay: {
          comment: 'Test',
          trait_id: trait.id,
          value: 'It was the best of times...',
          cost: '2',
          cost_type: 'XP'
        }
      }
    end

    it 'should create the record then redirect to index when posting valid attributes' do
      post(character_build_build_essays_path(character, build), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildEssay.count).to eq(1)
    end
  end

  describe 'GET /characters/:character_id/builds/:build_id/build_essays/:id/edit' do
    let(:build_essay) { FactoryGirl.create(:build_essay, build: build) }

    before  { get edit_character_build_build_essay_path(character, build, build_essay) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /characters/:character_id/builds/:build_id/build_essays/:id' do
    let(:build_essay) { FactoryGirl.create(:build_essay, build: build) }

    let(:valid_attributes) do
      {
        build_essay: { value: 'It was the best of times...' }
      }
    end

    it 'should update the record, then redirect to index when posting valid attributes' do
      patch(character_build_build_essay_path(character, build, build_essay), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildEssay.where(value: 'It was the best of times...').count).to eq(1)
    end
  end

  describe 'DELETE /characters/:character_id/builds/:build_id/build_essays/:id' do
    let(:build_essay) { FactoryGirl.create(:build_essay, build: build) }

    before { delete character_build_build_essay_path(character, build, build_essay) }
    specify { expect(response).to redirect_to edit_character_build_path(character, build) }
  end
end
