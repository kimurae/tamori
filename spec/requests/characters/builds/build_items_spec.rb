require 'rails_helper'

RSpec.describe '/characters/:character/builds/:build_id/build_items', type: :request do
  let(:build)        { character.builds.first }
  let(:character)    { FactoryGirl.create(:character, :with_builds) }
  let(:current_user) { build.user }
  let(:user_item) { FactoryGirl.create(:trait) }

  describe 'GET /characters/:character/builds/:build_id/build_items/:id/new' do
    before  { get new_character_build_build_item_path(character, build) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'POST /characters/:character/builds/:build_id/build_items' do
    let(:valid_attributes) do
      {
        build_item: {
          comment: 'Test',
          trait_id: user_item.id,
          qty: 4,
          location: 'Home',
          cost: '2',
          cost_type: 'GP',
          notes: ['It was awesome']
        }
      }
    end

    it 'should create the record then redirect to index when posting valid attributes' do
      post(character_build_build_items_path(character, build), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildItem.count).to eq(1)
    end
  end

  describe 'GET /characters/:character/builds/:build_id/build_items/:id/edit' do
    let(:build_item) { FactoryGirl.create(:build_item, build: build) }

    before  { get edit_character_build_build_item_path(character, build, build_item) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /characters/:character/builds/:build_id/build_items/:id' do
    let(:build_item) { FactoryGirl.create(:build_item, build: build) }

    let(:valid_attributes) do
      {
        build_item: { qty: 22 }
      }
    end

    it 'should update the record, then redirect to index when posting valid attributes' do
      patch(character_build_build_item_path(character, build, build_item), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildItem.where(qty: 22).count).to eq(1)
    end
  end

  describe 'DELETE /characters/:character/builds/:build_id/build_items/:id' do
    let(:build_item)  { FactoryGirl.create(:build_item, build: build) }

    before { delete character_build_build_item_path(character, build, build_item) }

    specify { expect(response).to redirect_to edit_character_build_path(character, build) }
  end
end
