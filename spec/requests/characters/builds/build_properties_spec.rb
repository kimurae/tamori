require 'rails_helper'

RSpec.describe '/characters/:character_id/builds/:build_id/build_properties', type: :request do
  let(:build)        { character.builds.first }
  let(:character)    { FactoryGirl.create(:character, :with_builds) }
  let(:current_user) { build.user }
  let(:trait)        { FactoryGirl.create(:trait) }

  describe 'GET /characters/:character_id/builds/:build_id/build_properties/:id/new' do
    before  { get new_character_build_build_property_path(character, build) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'POST /characters/:character_id/builds/:build_id/build_properties' do
    let(:valid_attributes) do
      {
        build_property: {
          comment: 'Test',
          trait_id: trait.id,
          value: 'magnetic',
          cost: '2',
          cost_type: 'XP'
        }
      }
    end

    it 'should create the record then redirect to index when posting valid attributes' do
      post(character_build_build_properties_path(character, build), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildProperty.count).to eq(1)
    end
  end

  describe 'GET /characters/:character_id/builds/:build_id/build_properties/:id/edit' do
    let(:build_property) { FactoryGirl.create(:build_property, build: build) }

    before  { get edit_character_build_build_property_path(character, build, build_property) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /characters/:character_id/builds/:build_id/build_properties/:id' do
    let(:build_property) { FactoryGirl.create(:build_property, build: build) }

    let(:valid_attributes) do
      {
        build_property: { value: 'none' }
      }
    end

    it 'should update the record, then redirect to index when posting valid attributes' do
      patch(character_build_build_property_path(character, build, build_property), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildProperty.where(value: 'none').count).to eq(1)
    end
  end

  describe 'DELETE /characters/:character_id/builds/:build_id/build_properties/:id' do
    let(:build_property) { FactoryGirl.create(:build_property, build: build) }

    before { delete character_build_build_property_path(character, build, build_property) }

    specify { expect(response).to redirect_to edit_character_build_path(character, build) }
  end
end
