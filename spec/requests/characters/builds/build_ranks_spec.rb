require 'rails_helper'

RSpec.describe '/characters/:character_id/builds/:build_id/build_ranks', type: :request do
  let(:build)        { character.builds.first }
  let(:character)    { FactoryGirl.create(:character, :with_builds) }
  let(:current_user) { build.user }
  let(:trait)        { FactoryGirl.create(:trait) }

  describe 'GET /characters/:character_id/builds/:build_id/build_ranks/:id/new' do
    before  { get new_character_build_build_rank_path(character, build) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'POST /characters/:character_id/builds/:build_id/build_ranks' do

    let(:valid_attributes) do
      {
        build_rank: {
          comment: 'Test',
          trait_id: trait.id,
          value: 42.1,
          cost: '2',
          cost_type: 'XP'
        }
      }
    end

    it 'should create the record then redirect to index when posting valid attributes' do
      post(character_build_build_ranks_path(character, build), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildRank.count).to eq(1)
    end
  end

  describe 'GET /characters/:character_id/builds/:build_id/build_ranks/:id/edit' do
    let(:build_rank) { FactoryGirl.create(:build_rank, build: build) }

    before  { get edit_character_build_build_rank_path(character, build, build_rank) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /characters/:character_id/builds/:build_id/build_ranks/:id' do
    let(:build_rank) { FactoryGirl.create(:build_rank, build: build) }

    let(:valid_attributes) do
      {
        build_rank: { value: 3.5 }
      }
    end

    it 'should update the record, then redirect to index when posting valid attributes' do
      patch(character_build_build_rank_path(character, build, build_rank), params: valid_attributes)
      expect(response).to redirect_to edit_character_build_path(character, build)
      expect(BuildRank.where(value: '3.5').count).to eq(1)
    end
  end

  describe 'DELETE /characters/:character_id/builds/:build_id/build_ranks/:id' do
    let(:build_rank)  { FactoryGirl.create(:build_rank, build: build) }

    before { delete character_build_build_rank_path(character, build, build_rank) }

    specify { expect(response).to redirect_to edit_character_build_path(character, build) }
  end
end
