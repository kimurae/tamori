require 'rails_helper'

RSpec.describe 'game_sessions', type: :request do
  let(:character)    { FactoryGirl.create(:character) }

  describe 'GET /characters/:character_id/game_sessions' do
    let!(:game_session) { FactoryGirl.create(:game_session, characters: [character], name: 'Dummy', built_on: '2016/08/25', user: character.user) }
    let(:current_user)  { character.user }

    before  { get character_game_sessions_path(character) }
    specify { expect(response).to have_http_status(:ok)     }
  end

  describe 'POST /characters/:character_id/game_sessions' do
    let(:current_user) { character.user }

    before  { post character_game_sessions_path(character.id)  }

    specify { expect(response).to redirect_to character_game_sessions_path(character.id) }
  end

  describe 'GET /characters/:character_id/game_sessions/:id/edit' do
    let(:game_session) { FactoryGirl.create(:game_session, characters: [character], name: 'Dummy', built_on: '2016/08/25') }
    let(:current_user) { character.user }

    before  { get edit_character_game_session_path(character, game_session) }
    specify { expect(response).to have_http_status(:ok)                }
  end

  describe 'PATCH /characters/:character_id/game_sessions/:id' do
    let(:game_session) { FactoryGirl.create(:game_session, characters: [character], name: 'Dummy', built_on: '2016/08/25', experience_awarded: 0) }
    let(:current_user) { character.user }

    describe 'with valid parameters' do
      let(:valid_attributes) do
        {
          game_session: { name: 'Meh', built_on: '2015/08/24', experience_awarded: 4 }
        }
      end

      before do
        patch(character_game_session_path(character, game_session), params: valid_attributes)
      end

      specify { expect(response).to redirect_to character_game_sessions_path(character) }
    end

    describe 'with invalid parameters' do
      let(:invalid_attributes) do
        {
          game_session: { name: '' }
        }
      end

      before do
        patch(character_game_session_path(character, game_session), params: invalid_attributes)
      end

      it 'should not redirect, but instead render the existing page with the validation errors' do
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'DELETE /characters/:character_id/game_sessions/:id' do
    let(:game_session)  { FactoryGirl.create(:game_session, characters: [character] ) }
    let(:current_user)  { character.user }

    before { delete character_game_session_path(character, game_session) }

    specify { expect(response).to redirect_to character_game_sessions_path(character) }
  end
end
