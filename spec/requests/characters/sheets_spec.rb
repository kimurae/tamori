require 'rails_helper'

RSpec.describe 'Sheets', type: :request do

  # TODO: Move these to some function in the helper.
  let(:character)    { FactoryGirl.create(:character) }
  let(:current_user) { character.user }

  describe 'GET /character/:id/sheets' do
    before do
      FactoryGirl.create(:character_sheet, character: character)
      get character_sheets_path(character)
    end

    it 'returns a successful response with a character sheet' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET /character/:character_id/sheet/:id' do
    let(:character_sheet) { FactoryGirl.create(:character_sheet, character: character) }

    before do
      get character_sheet_path(character, character_sheet)
    end

    specify { expect(response).to redirect_to Rails.application.routes.url_helpers.rails_blob_path(character_sheet.sheet, disposition: 'attachment', only_path: true) }
  end

  describe 'POST /character/:character_id/sheets' do
    let(:builds) { FactoryGirl.create_list(:build, 3, characters: [character]) }

    it 'creates a character sheet with valid attributes and redirect to the index page' do
      post character_sheets_path(character), params: { build_ids: builds.map(&:id) }
      expect(response).to redirect_to character_sheets_path(character)
      expect(CharacterSheet.count).to eq(1)
    end

    it 'should return a unsuccessful response' do
      post character_sheets_path(0), params: { build_ids: builds.map(&:id) }
      expect(response).to have_http_status(:unprocessable_entity)
      expect(CharacterSheet.count).to eq(0)
    end
  end

  describe 'PUT /characters/:character_id/sheets/:id' do
    let(:character_sheet) { FactoryGirl.create(:character_sheet, character: character) }

    before do
      patch character_sheet_path(character, character_sheet)
    end

    it 'should return a character sheet o.0' do
      expect(response).to redirect_to character_sheets_path(character)
    end
  end

  describe 'DELETE /charachters/:character_id/sheets/:id' do
    let(:character_sheet) { FactoryGirl.create(:character_sheet, character: character) }

    before do
      delete character_sheet_path(character, character_sheet)
    end

    context 'with a valid character' do
      it 'returns successfully and removes the sheet' do
        expect(response).to redirect_to character_sheets_path(character)
        expect(CharacterSheet.count).to eq(0)
      end
    end

    context 'without a valid character' do
      let(:current_user) { FactoryGirl.create(:user) }
      specify { expect(response).to have_http_status(:unprocessable_entity) }
    end
  end
end
