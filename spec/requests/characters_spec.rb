require 'rails_helper'

RSpec.describe 'builds', type: :request do

  describe 'GET /characters' do
    let!(:character)    { FactoryGirl.create(:character) }
    let(:current_user)  { character.user }

    before  { get characters_path }
    specify { expect(response).to have_http_status(:ok)     }
  end

  describe 'GET /characters/new' do
    let(:current_user) { FactoryGirl.create(:user) }

    before  { get new_character_path }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'GET /characters/:id' do
    let(:character)    { FactoryGirl.create(:character) }
    let(:current_user) { character.user }

    before  { get character_path(character) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'POST /character' do
    let(:system)       { FactoryGirl.create(:system) }
    let(:current_user) { FactoryGirl.create(:user) }

    describe 'with valid parameters' do
      let(:valid_attributes) do
        {
          character: { name: 'Meh', system_id: system.id }
        }
      end

      before do
        post(characters_path, params: valid_attributes)
      end

      specify { expect(response).to redirect_to characters_path }
    end

    describe 'with invalid parameters' do
      let(:invalid_attributes) do
        {
          character: { name: '' }
        }
      end

      before do
        post(characters_path, params: invalid_attributes)
      end

      skip 'should not redirect, but instead render the existing page with the validation errors' do
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET /characters/:id/edit' do
    let(:character)    { FactoryGirl.create(:character) }
    let(:current_user) { character.user }

    before  { get edit_character_path(character) }
    specify { expect(response).to have_http_status(:ok) }
  end
end
