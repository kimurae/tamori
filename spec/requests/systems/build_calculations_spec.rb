require 'rails_helper'

RSpec.describe 'Calculations', type: :request do

  # These may need to be moved to a helper, so I can authenticate
  # in other request specs.
  let(:current_user)  { FactoryGirl.create(:user, admin: true) }
  let(:system)        { FactoryGirl.create(:system) }

  describe 'GET /system/:system_id/build_calculations' do

    before do
      FactoryGirl.create(:build_calculation, system: system)
      get system_build_calculations_path(system)
    end

    specify do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET /system/build_calculations/new' do
    before  { get new_system_build_calculation_path(system) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'POST /system/build_calculations' do
    let(:traits) do
      FactoryGirl.create_list(:trait, 2, system: system).map(&:id)
    end

    let(:valid_attributes) do
      {
        build_calculation: {
          argument:   '1',
          calculator: 'ConcatCalculator',
          input_key:  'value',
          output_key: 'value',
          priority:   10,
          trait_id:   traits.first,
          trait_ids:  traits
        }
      }
    end

    it 'should post valid attributes, create a new record, and redirect to index' do
      post(system_build_calculations_path(system), params: valid_attributes)

      expect(response).to redirect_to system_build_calculations_path(system)
      expect(BuildCalculation.count).to eq(1)
    end

    it 'should post invalid attributes and only re-render the form' do
      post(system_build_calculations_path(system), params: { build_calculation: { argument: 'meh' } })

      expect(response).to have_http_status(:ok)
      expect(BuildCalculation.count).to eq(0)
    end
  end

  describe 'GET /build_calculation/:id/edit' do
    let(:build_calculation) { FactoryGirl.create(:build_calculation, system: system) }

    before  { get edit_build_calculation_path(build_calculation.id) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /build_calculations/:id' do
    let(:build_calculation) do
      FactoryGirl.create(:build_calculation, system: system)
    end

    let(:valid_attributes) do
      {
        build_calculation: { argument:   '4' }
      }
    end

    it 'should post valid attributes, update the record, and redirect to index' do
      patch(build_calculation_path(build_calculation), params: valid_attributes)

      expect(response).to redirect_to system_build_calculations_path(system)
      expect(BuildCalculation.where(argument: '4').count).to eq(1)
    end

    it 'should post invalid attributes and only re-render the form' do
      patch(build_calculation_path(build_calculation), params: { build_calculation: {input_key: ''}})

      expect(response).to have_http_status(:ok)
      expect(BuildCalculation.where(argument: '4').count).to eq(0)
    end
  end
end
