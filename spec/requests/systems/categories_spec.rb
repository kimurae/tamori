require 'rails_helper'

RSpec.describe 'Categories', type: :request do

  # These may need to be moved to a helper, so I can authenticate
  # in other request specs.
  let(:current_user)  { FactoryGirl.create(:user, :admin => true) }
  let(:system)        { FactoryGirl.create(:system) }

  describe 'GET /system/categories' do
    before do
      FactoryGirl.create(:category, system: system)
      get system_categories_path(system)
    end

    specify do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET /categories/:id/edit' do
    before  { get new_system_category_path(system) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'POST /system/:system_id/categories' do
    context 'with valid attributes' do
      let(:valid_attributes) do
        { label: 'Test', name: 'tests' }
      end

      before do
        post system_categories_path(system), params: { category: valid_attributes }
      end

      it 'should create a system category and return it with a successful response' do
        expect(response).to redirect_to system_categories_path(system)
        expect(Category.count).to eq(1)
      end
    end
  end

  describe 'GET /categories/:id/edit' do
    let(:category) { FactoryGirl.create(:category, name: '1', system: system) }

    before  { get edit_category_path(category) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PUT/PATCH /categories/:id' do
    context 'with valid attributes' do
      let(:category)         { FactoryGirl.create(:category, name: '1', system: system) }
      let(:valid_attributes) { { name: '3' } }

      before do
        patch category_path(category), params: { :category => valid_attributes }
      end

      it 'should update a system category and return a successful response' do
        expect(response).to redirect_to system_categories_path(system)
        expect(Category.where(:name => '3').count).to eq(1)
      end
    end
  end
end
