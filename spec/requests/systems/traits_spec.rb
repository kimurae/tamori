require 'rails_helper'

RSpec.describe 'Traits', type: :request do

  # These may need to be moved to a helper, so I can authenticate
  # in other request specs.
  let(:current_user)  { FactoryGirl.create(:user, admin: true) }
  let(:system)        { FactoryGirl.create(:system) }

  describe 'GET /system/traits' do

    before do
      FactoryGirl.create(:trait, system_id: system.id)
      get system_traits_path(system)
    end

    specify do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET /system/traits/new' do
    before  { get new_system_trait_path(system) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'POST /system/traits' do
    let(:category) do
      FactoryGirl.create(:category, system: system)
    end

    let(:valid_attributes) do
      {
        trait: {
          category_id:  category.id,
          label:        'Agility',
          manual_order: 1,
          name:         'agility',
          detail_attributes: {
            area:         '5 ft cone',
            book:         'Core',
            cost:         '2 willpower',
            damage:       'None',
            description:  'Its a small world...',
            demands:      'A shrubbery',
            difficulty:   'None',
            duration:     'One Hour',
            level:        '3',
            list:         'Guest',
            page:         '102',
            range:        '5 ft',
            reduction:    'None',
            roll:         'None',
            tn_bonus:     '2',
            keywords:     %w(Jade Thunder),
            raises:       ['+1k1']
          }
        }
      }
    end

    it 'should post valid attributes, create a new record, and redirect to index' do
      post(system_traits_path(system), params: valid_attributes)

      expect(response).to redirect_to system_traits_path(system)

      expect(Trait.where(name: 'agility').exists?).to eq(true)
      expect(Detail.where(description: 'Its a small world...').exists?).to eq(true)
    end

    it 'should post invalid attributes and only re-render the form' do
      post(system_traits_path(system), params: { trait: { name: ' ' } })

      expect(response).to have_http_status(:ok)
      expect(Trait.count).to eq(0)
      expect(Detail.count).to eq(0)
    end
  end

  describe 'GET /system/:system_id/trait/:id/edit' do
    let(:trait) { FactoryGirl.create(:trait, system: system) }

    before  { get edit_system_trait_path(system.id,trait.id) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PATCH /system/:system_id/traits/:id' do
    let(:trait) do
      FactoryGirl.create(:trait, system: system)
    end

    let(:valid_attributes) do
      {
        trait: { name:   'four' }
      }
    end

    it 'should post valid attributes, update the record, and redirect to index' do
      patch(system_trait_path(system, trait), params: valid_attributes)

      expect(response).to redirect_to system_traits_path(system)
      expect(Trait.where(name: 'four').count).to eq(1)
    end

    it 'should post invalid attributes and only re-render the form' do
      patch(system_trait_path(system, trait), params: { trait: {name: ''}})

      expect(response).to have_http_status(:ok)
    end
  end
end
