require 'rails_helper'

RSpec.describe "Systems", :type => :request do

  # These may need to be moved to a helper, so I can authenticate
  # in other request specs.
  let(:current_user)  { FactoryGirl.create(:user, :admin => true) }

  describe 'GET /systems' do
    before do
      FactoryGirl.create(:system)
      get systems_path
    end

    specify do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET /system/:id' do
    let(:system) { FactoryGirl.create(:system) }
    before       { get system_path(system) }
    specify      { expect(response).to have_http_status(:ok) }
  end

  describe 'GET /systems/new' do
    before       { get new_system_path }
    specify      { expect(response).to have_http_status(:ok) }
  end

  describe 'POST /systems.json' do
    context 'with valid attributes' do
      before do
        post systems_path, params: { system: { name: 'cat' } }
      end

      specify do
        expect(response).to redirect_to systems_path
        expect(System.count).to eq(1)
      end
    end

    context 'with invalid attributes' do
      before do
        post systems_path, params: { system: { name: '' } }
      end

      it 'should render the new template with the errors' do
        expect(response).to have_http_status(:ok)
        expect(System.count).to eq(0)
      end
    end
  end
end
