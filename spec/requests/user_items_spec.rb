require 'rails_helper'

RSpec.describe 'UserItems', :type => :request do

  let(:current_user)  { FactoryGirl.create(:user) }

  describe 'GET /user_items' do
    before  { get user_items_path(params: { character_id: FactoryGirl.create(:character, user: current_user).id }) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'GET /users_items/new' do
    before  { get new_user_item_path }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'POST /user_items' do

    context 'with valid attributes' do
      let(:valid_attributes) do
        {
          book:          'Core',
          category_id:  '93cbae14-1324-4f8b-bbd7-a921c8bec18f',
          description:  'Awesome',
          name:         'Legend',
          page:         '201',
          devotion:     '1',
          influence:    '1',
          mentor:       '1',
          level:        '1',
          list:         'Meh'
        }
      end

      before do
        post user_items_path, :params => { user_item: valid_attributes }
      end

      it 'creates a user item and returns successfully' do
        expect(response).to redirect_to user_items_url
        expect(UserItem.count).to eq(1)
      end
    end
  end

  describe 'GET /users_item/:id/edit' do
    let(:user_item) { FactoryGirl.create(:user_item, user_id: current_user.id) }

    before  { get edit_user_item_path(user_item) }
    specify { expect(response).to have_http_status(:ok) }
  end

  describe 'PUT/PATCH /user_items/:id' do
    let(:user_item)        { FactoryGirl.create(:user_item, user_id: current_user.id) }
    let(:valid_attributes) { { name: 'Awesome Build' } }

    before do
      patch user_item_path(user_item), params: { user_item: valid_attributes }
    end

    it 'updates the user item and returns successfully' do
      expect(response).to redirect_to user_items_url
      expect(UserItem.where(name: 'Awesome Build').count).to eq(1)
    end
  end

  describe 'DELETE /user_items/:id' do
    let(:user_item) { FactoryGirl.create(:user_item, user_id: current_user.id) }

    before { delete user_item_path(user_item) }

    it 'deletes the user item and returns to the index page' do
      expect(response).to redirect_to user_items_url
      expect(UserItem.count).to eq(0)
    end
  end
end
