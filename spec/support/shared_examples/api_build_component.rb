require 'rails_helper'

RSpec.shared_examples 'an api build component' do

  def self.api_endpoint
    described_class.name.underscore.split('_').last
  end

  def api_endpoint
    self.class.api_endpoint
  end

  def factory
    described_class.name.singularize.underscore.to_sym
  end

  def singular_api_path
    "api_character_#{described_class.name.singularize.underscore}_path".to_sym
  end

  def plural_api_path
    "api_character_#{described_class.name.pluralize.underscore}_path".to_sym
  end

  let(:character)    { FactoryGirl.create(:character) }

  let(:character_build) {
    FactoryGirl.create(:build, :character => character, :name => 'Test Build')
  }

  let(:current_user)  { character.user }

  describe "GET /api/character/:id/build/:build_id/#{api_endpoint.pluralize}.json" do
    before do
      FactoryGirl.create(factory, :build_id => character_build.id)
      get send(plural_api_path, character, character_build, :format => :json)
    end

    it "returns a successful response with a list of #{api_endpoint.pluralize}" do
      expect(response).to have_http_status(:ok)
      expect(response).to match_response_schema("api/characters/builds/#{api_endpoint.pluralize}/index")
    end
  end

  describe "POST /api/character/:character_id/build/:build_id/#{api_endpoint.pluralize}.json" do
    let(:valid_attributes) do
      { :comment => 'Awesome Build' }
    end

    context 'with valid attributes' do
      before do
        post send(plural_api_path, character, character_build, :format => :json), :params => { api_endpoint.singularize.to_sym => valid_attributes }
      end

      it "creates a new #{api_endpoint.singularize} for the build and returns successfully" do
        expect(response).to have_http_status(:created)
        expect(described_class.where(build_id: character_build.id).count).to eq(1)
      end
    end

    context 'without a valid character' do

      before do
        post send(plural_api_path, 0, character_build, :format => :json), :params => { api_endpoint.singularize.to_sym => valid_attributes }
      end

      it 'returns a 422' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PUT/PATCH /api/character/:character_id/build/:build_id/#{api_endpoint.singularize}/:id.json" do
    let(:build_component)  { FactoryGirl.create(factory, :build_id => character_build.id) }
    let(:valid_attributes) { { :comment => 'Awesome Build' } }

    context 'with valid attributes' do
      before do
        patch send(singular_api_path, character,character_build,build_component, :format => 'json'), :params => { api_endpoint.singularize.to_sym => valid_attributes }
      end

      it "updates the #{api_endpoint.singularize} and returns successfully" do
        expect(response).to have_http_status(:no_content)
        expect(described_class.where(:comment => 'Awesome Build').count).to eq(1)
      end
    end

    context 'without a valid character' do
      before do
        patch send(singular_api_path,0,character_build,build_component, :format => 'json'), :params => { api_endpoint.singularize.to_sym => valid_attributes }
      end

      it 'returns a 422' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE /character/:character_id/build/:build_id/#{api_endpoint.pluralize}/:id.json" do
    let(:build_component)  { FactoryGirl.create(factory, :build_id => character_build.id) }

    before do
      delete send(singular_api_path,character,character_build,build_component, :format => 'json')
    end

    it "removes the #{api_endpoint.singularize} and returns successfully" do
      expect(response).to have_http_status(:no_content)
      expect(described_class.count).to eq(0)
    end

    context 'without a valid character' do
      let(:current_user) { FactoryGirl.create(:user) }
      specify { expect(response).to have_http_status(:unprocessable_entity) }
    end
  end
end
